require 'date'
require 'global_config'
require 'typhoeus'
require 'broker_apis/bin_blob'

class TDACurlApi
  def initialize
    @headers = {
      'User-Agent' => "com.optionscraft TDAPI #{GlobalConfig.options_craft_version}"
    }
    @logged_in = false
  end

  @@instance = TDACurlApi.new

  def self.instance
    @@instance
  end

  private_class_method :new

  def login
    @tries ||= GlobalConfig.tda_login_retries
    req = make_login_request
    req.on_complete do |response|
      if response.success?
        h = Hash.from_xml(response.body)
        @headers['Cookie'] = "JSESSIONID=#{h['amtd']['xml_log_in']['session_id']}"
        return h['amtd']['result']
      elsif response.timed_out?
        handle_login_timeout(req)
      elsif response.code != 200
        handle_login_bad_response_code(req)
      else
        Rails.logger.warn { "#{Time.zone.now} Unknown error occurred during TDA login." }
      end
      return 'FAIL'
    end
    req.run
  end

  def lazy_login
    unless @logged_in
      return 'FAIL' if login == 'FAIL'
      @logged_in = true
      at_exit do
        logout
      end
    end
  end

  # abstracting out the login -> do stuff -> logout pattern
  def while_logged_in
    return if login == 'FAIL'
    yield
    logout
  end

  def handle_login_timeout(req)
    if @tries.nonzero?
      Rails.logger.warn do
        "#{Time.zone.now} Retrying login request, response timed out."
      end
      @tries -= 1
      req.run
    end
    Rails.logger.warn { "#{Time.zone.now} TDA login response timed out." }
  end

  # interval vs period. guess: interval is the granularity of the volatility sampling
  # period is the amount of data requested
  # "2 months of data with volatility sampled daily" =>
  # intervaltype = "DAILY", periodtype = "MONTH", period="2"
  # def verbose_volatility_info(symbol,
  #                             volatility_history_type, # I || H
  #                             interval_type, # DAILY, WEEKLY, MONTHLY
  #                             period_type, #DAY, WEEK, MONTH, YTD
  #                             period, # Int with constraints
  #                             startdate, #YYYYMMDD
  #                             enddate, #YYYYMMDD
  #                             days_to_expiration, # 30, 60, 90, 120, 150, 180
  #                             surface_type_identifier, # DELTA, DELTA_WITH_COMPOSITE, SKEW
  #                             surface_type_value)
  # error handling needs to be done by inspecting the http headers
  # "&startdate=#{args[:startdate]}&enddate=#{args[:enddate]}" \
  def verbose_volatility_info(args)
    return if lazy_login == 'FAIL'
    uri = URI.parse(
      "https://apis.tdameritrade.com/apps/100/VolatilityHistory?source=#{TDA_SOURCE_ID}&" \
      "requestidentifiertype=SYMBOL&requestvalue=#{args[:symbol]}&" \
      "volatilityhistorytype=#{args[:volatility_history_type]}&" \
      "intervaltype=#{args[:interval_type]}&intervalduration=1&" \
      "periodtype=#{args[:period_type]}&period=#{args[:period]}" \
      "&daystoexpiration=#{args[:days_to_expiration]}&surfacetypeidentifier=" \
      "#{args[:surface_type_identifier]}&surfacetypevalue=#{args[:surface_type_value]}"
    )
    result = Typhoeus.get(uri, headers: @headers)
    return result
  end

  def implied_volatility(symbol)
    args = {
      symbol: symbol,
      volatility_history_type: 'I',
      interval_type: 'DAILY',
      period_type: 'DAY',
      period: '1',
      daystoexpiration: '30',
      surface_type_identifier: 'DELTA_WITH_COMPOSITE',
      surface_type_value: '50,-50'
    }
    formatted_volatility_data(args)
  end

  def formatted_volatility_data(args)
    # {'SYMB' => [{vol: 0.123123, timestamp: 00000123232}]}
    data = verbose_volatility_info(args).response_body
    b = BinBlob.new(data)
    result = {}
    symbol_count = b.read_int32
    symbol_count.times do
      symbol_length = b.read_int16
      symbol = b.read_string(symbol_length)
      result[symbol] = []
      error_code = b.read_char
      # error_length = b.read_int16
      puts "error code: #{error_code}, error_length: #{}"
      # error
      value_count = b.read_int32
      value_count.times do
        value = b.read_float32
        timestamp = b.read_int64
        result[symbol].push({vol: value, timestamp: timestamp})
      end
    end
    b.read_string(2)
    b.close
    result
  end


  def historical_data(symbs)
    return if lazy_login == 'FAIL'
    today = Time.zone.today - 1.day
    startdate = (today - GlobalConfig.yahoo_history_days_back.days).strftime("%Y%m%d")
    enddate = today.strftime("%Y%m%d")
    uri = URI.parse(
      "https://apis.tdameritrade.com/apps/100/PriceHistory?source=#{TDA_SOURCE_ID}&" \
      "requestidentifiertype=SYMBOL&requestvalue=#{symbs.join(', ')}&intervaltype=DAILY&" \
      "intervalduration=1&startdate=#{startdate}&enddate=#{enddate}"
    )
    Typhoeus.get(uri, headers: @headers)
  end

  def formatted_historical_data(symbols)
    result = {}
    all_symbs = symbols.each_slice(100)
    all_symbs.each do |symbs|
      data = historical_data(symbs).response_body
      read_binary_history(data, result)
    end
    result
  end

  def read_binary_history_2(data, result)
    b = BinBlob.new(data)
    symbol_count = b.read_int32
    symbol_count.times do
      symbol_length = b.read_int16
      symbol = b.read_string(symbol_length)
      result[symbol] = []
      error_code = b.read_string(1) # error not handled
      bar_count = b.read_int32
      bar_count.times do
        close = b.read_float32.round(2)
        high = b.read_string(4)
        low = b.read_string(4)
        open = b.read_string(4)
        volume = b.read_string(4)
        timestamp = (b.read_int64) / 1000
        date = Date.strptime(timestamp.to_s, "%s")
        result[symbol].push([close, date])
      end
      terminator = b.read_string(2)
    end
    b.close

  end


  def read_binary_history(data, result)
    File.write('/tmp/response_data', data, {mode: 'wb'})
    f = File.open('/tmp/response_data')
    symbol_count = f.read(4).unpack('N')[0]
    symbol_count.times do
      symbol_length = f.read(2).unpack('n')[0]
      symbol = f.read(symbol_length)
      result[symbol] = []
      error_code = f.read(1).unpack('C')[0]
      bar_count = f.read(4).unpack('N')[0]
      bar_count.times do
        close = f.read(4).unpack('g')[0].round(2)
        high = f.read(4)
        low = f.read(4)
        open = f.read(4)
        volume = f.read(4)
        timestamp = (f.read(8).unpack('Q>')[0]) / 1000
        date = Date.strptime(timestamp.to_s, "%s")
        result[symbol].push([close, date])
      end
      terminator = f.read(2)
    end
    f.close
  end

  def closing_quote_for(symb, expire_date)
    result = {}
    data = api_closing_quote_for(symb, expire_date).response_body
    read_binary_history(data, result)
    result[symb][0][0]
  end

  def api_closing_quote_for(symb, expire_date)
    return if lazy_login == 'FAIL'
    startdate = expire_date.strftime("%Y%m%d")
    enddate = expire_date.strftime("%Y%m%d")
    uri = URI.parse(
      "https://apis.tdameritrade.com/apps/100/PriceHistory?source=#{TDA_SOURCE_ID}&" \
      "requestidentifiertype=SYMBOL&requestvalue=#{symb}&intervaltype=DAILY&" \
      "intervalduration=1&startdate=#{startdate}&enddate=#{enddate}"
    )
    Typhoeus.get(uri, headers: @headers)
  end


  def handle_login_bad_response_code(req)
    if @tries.nonzero?
      Rails.logger.warn do
        "#{Time.zone.now} Retrying login request, response code != 200"
      end
      @tries -= 1
      req.run
    end
    Rails.logger.warn do
      "#{Time.zone.now} Unexpected response code on TDA login = #{response.code}"
    end
  end

  def logout
    uri = URI.parse(
      "https://apis.tdameritrade.com/apps/100/LogOut?source=#{TDA_SOURCE_ID}"
    )
    Typhoeus.get(uri, headers: @headers)
  end

  def make_login_request
    body = {
      'userid' => TDA_USER_ID,
      'password' => TDA_PASSWORD,
      'source' => TDA_SOURCE_ID,
      'version' => GlobalConfig.options_craft_version
    }
    Typhoeus::Request.new(login_uri, method: :post, body: body, headers: @headers)
  end

  def login_uri
    URI.parse(
      "https://apis.tdameritrade.com/apps/100/LogIn?source=#{TDA_SOURCE_ID}&version=#{GlobalConfig.options_craft_version}"
    )
  end

  def make_chain_request(symbol, series)
    return if lazy_login == 'FAIL'
    uri = URI.parse('https://apis.tdameritrade.com/apps/200/OptionChain?' \
    "source=#{TDA_SOURCE_ID}&" \
    "symbol=#{symbol}&" \
    'quotes=true&' \
    "expire=#{series}")
    Typhoeus::Request.new(uri, headers: @headers)
  end

  def make_symbol_lookup_request(search_text)
    return if lazy_login == 'FAIL'
    uri = URI.parse('https://apis.tdameritrade.com/apps/100/SymbolLookup?' \
    "source=#{TDA_SOURCE_ID}&" \
    "matchstring=#{search_text}")
    Typhoeus::Request.new(uri, headers: @headers)
  end

  def make_symbol_quote_request(symbol)
    return if lazy_login == 'FAIL'
    uri = URI.parse('https://apis.tdameritrade.com/apps/100/Quote?' \
    "source=#{TDA_SOURCE_ID}&" \
    "symbol=#{symbol}")
    Typhoeus::Request.new(uri, headers: @headers)
  end

  def batch_quote_request(symbols)
    return if lazy_login == 'FAIL'
    symbols_string = symbols.join(',')
    uri = URI.parse('https://apis.tdameritrade.com/apps/100/Quote?' \
    "source=#{TDA_SOURCE_ID}&" \
    "symbol=#{symbols_string}")
    Typhoeus.get(uri, headers: @headers)
  end
end
