require 'date'
require 'csv'
require 'stats_guy'
require 'global_config'

class YHApi

  def now
    Time.zone.today
  end

  def past
    now - GlobalConfig.yahoo_history_days_back
  end

  @@instance = YHApi.new

  def self.instance
    @@instance
  end

  private_class_method :new

  # Aug 03, 2016
  # 03-Aug-16
  def nasdaq_earnings_date(symb)
    uri = URI.parse("http://www.nasdaq.com/earnings/report/#{symb.downcase}")
    resp = YAHOO_HTTPCLIENT.get(uri)
    text = resp.body.to_s
    result = text.match(/Earnings announcement.+:\s*(.*)<?/)[1]
    if result.include? '</h2>'
      result = 'None'
    else
      month = result[0..2]
      day = result[4..5]
      year = result[10..11]
      result = "#{day}-#{month}-#{year}" # legacy format
    end
    result
  end

  # starting format: 2016-07-15
  # ending format: 15-Jul-16
  def yahoo_earnings_date(symb)
    resp = fetch_earnings_date_json(symb)
    return 'None' unless resp
    begin
      ed = JSON.parse(resp)
    rescue JSON::ParserError
      return 'None'
    end
    return 'None' unless ed

    # result could either not exist or be equal to nil
    date_array = (ed.fetch('quoteSummary', {})
                   .fetch('result', [{}]) || [{}])[0]
                   .fetch('calendarEvents', {})
                   .fetch('earnings', {})
                   .fetch('earningsDate', [])
    return 'None' if date_array.empty?
    Date.parse(date_array[0]['fmt']).strftime('%d-%b-%y')
  end

  def fetch_earnings_date_json(symb)
    uri_str = 'https://query2.finance.yahoo.com/v10/finance/quoteSummary/' \
              "#{symb}?modules=calendarEvents"
    uri = URI.parse(uri_str)
    have_retried = false
    loop do
      resp_str = YAHOO_HTTPCLIENT.get(uri).body.to_s
      return resp_str if resp_str.length >= 2
      return nil if resp_str.length < 2 && have_retried
      puts 'retrying'
      have_retried = true
    end
  end
end
