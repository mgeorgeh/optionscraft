# require 'active_support/core_ext/hash/conversions'
require 'global_config'

class OHApi
  def initialize
    @om_user_agent = "com.optionscraft OMAPI #{GlobalConfig.options_craft_version}"
    @logged_in = false
  end

  @@instance = OHApi.new

  def self.instance
    @@instance
  end

  def login
    resp = OH_HTTPCLIENT.post(login_uri, login_body, 'User-Agent' => @om_user_agent)
    resp_json = JSON.parse(resp.body)
    @om_token = resp_json['token']
    @om_user_id = resp_json['userId']
    if !@om_token || !@om_user_id
      Rails.logger.error do
        "#{Time.zone.now} #{resp_json['errorCode']} #{resp_json['errorMessage']}" \
        ' ERROR: during OH login.'
      end
      return 'FAIL'
    end
  rescue => e
    Rails.logger.error do
      "#{Time.zone.now} #{e.class} #{e.message} EXCEPTION: during OH login."
    end
    return 'FAIL'
  end

  def login_uri
    URI.parse('https://www.trademonster.com/j_acegi_security_check')
  end

  def login_body
    { 'j_username' => OH_USER_ID, 'j_password' => OH_PASSWORD }
  end

  def logout
    uri = URI.parse('https://www.trademonster.com/j_acegi_logout')
    OH_HTTPCLIENT.post(uri,
                       nil,
                       'User-Agent' => @om_user_agent,
                       'token' => @om_token)
  end

  def lazy_login
    unless @logged_in
      return 'FAIL' if login == 'FAIL'
      @logged_in = true
      at_exit do
        logout
      end
    end
  end

  def while_logged_in
    return if login == 'FAIL'
    yield
    logout
  end

  def quote_request(attrib, symbol)
    # quote/quotes
    return if lazy_login == 'FAIL'
    uri = URI.parse('https://www.trademonster.com/services/quotesService')
    body = "<getQuotes><item><symbol>#{symbol}</symbol>" \
           '<instrumentType>Equity</instrumentType></item></getQuotes>'
    resp = OH_HTTPCLIENT.post(uri, body,
                              'Content-Type' => 'text/xml',
                              'User-Agent' => @om_user_agent,
                              'token' => @om_token)

    if resp.body.start_with?('<ns1:XMLFault')
      puts resp.body
      Rails.logger.error do
        "#{Time.zone.now} #{e.class} #{e.message}" \
        " ERROR: during OH quote request symbol: #{symbol}."
      end
      return nil
    end

    quote_hash = Hash.from_xml(resp.body)['getQuotesResponse']['item']
    mark = quote_hash[attrib]['amount']
    # mark = quote_hash["closingMark"]["amount"]
    mark
  end

  def options_quote_request(symbol)
    # quote/quotes AAPLL0916C115000
    return if lazy_login == 'FAIL'
    uri = URI.parse('https://www.trademonster.com/services/quotesService')
    body = "<getQuotes><item><symbol>#{symbol}</symbol>" \
           '<instrumentType>Option</instrumentType></item></getQuotes>'
    resp = OH_HTTPCLIENT.post(uri, body,
                              'Content-Type' => 'text/xml',
                              'User-Agent' => @om_user_agent,
                              'token' => @om_token)

    if resp.body.start_with?('<ns1:XMLFault')
      puts resp.body
      Rails.logger.error do
        "#{Time.zone.now} #{e.class} #{e.message}" \
        " ERROR: during OH quote request symbol: #{symbol}."
      end
      return nil
    end

    puts resp.body
  end

  def underlying_volatility(symbol)
    return if lazy_login == 'FAIL'
    uri = URI.parse('https://www.trademonster.com/services/usersSymbolHistoryService')
    body = "<getVolatilityDetails><symbolName>#{symbol}</symbolName>" \
           '<instrumentType>Equity</instrumentType></getVolatilityDetails>'
    resp = OH_HTTPCLIENT.post(uri, body,
                              'Content-Type' => 'text/xml',
                              'User-Agent' => @om_user_agent,
                              'token' => @om_token)
    # error handling
    if resp.body.start_with?('<ns1:XMLFault')
      puts resp.body
      Rails.logger.error do
        "#{Time.zone.now} #{e.class} #{e.message} " \
        "ERROR: during OH request symbol: #{symbol}."
      end
      return nil
    end
    vol_hash = Hash.from_xml(resp.body)['getVolatilityDetailsResponse']
    return calc_iv_stats(vol_hash)
  rescue => e
    Rails.logger.error do
      "#{Time.zone.now} #{e.class} #{e.message}" \
      " EXCEPTION: during OH request symbol: #{symbol}."
    end
    return nil
  end

  def calc_iv_stats(vol_hash)
    iv = vol_hash['impliedVolatility'].to_f
    iv_low = vol_hash['ivLow52Weeks'].to_f
    iv_high = vol_hash['ivHigh52Weeks'].to_f
    hv = vol_hash['historicalVolatility'].to_f
    iv_rank = 100 * (iv - iv_low) / (iv_high - iv_low)

    { iv: format('%.2f', (iv * 100)),
      hv: format('%.2f', (hv * 100)),
      iv_rank: format('%.2f', iv_rank) }
  end

  private_class_method :new
end
