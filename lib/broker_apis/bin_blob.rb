class BinBlob
  def initialize(data)
    File.write('/tmp/response_data', data, {mode: 'wb'})
    @file = File.open('/tmp/response_data')
  end

  def read_int64
    @file.read(8).unpack('Q>')[0]
  end

  def read_int32
    @file.read(4).unpack('N')[0]
  end

  def read_int16
    @file.read(2).unpack('n')[0]
  end

  def read_char
    @file.read(1).unpack('C')[0]
  end

  def read_float32
    @file.read(4).unpack('g')[0]
  end

  def read_string(n)
    @file.read(n)
  end

  def close
    @file.close
  end
end
