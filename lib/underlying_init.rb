# Should be a one time run of this code during inital set up
# bin/rails runner lib/underlying_init.rb

require 'csv'

CSV.foreach('notes/InitialIndexes.csv') do |row|
  Underlying.create(
    tda_symbol: row[0],
    om_symbol: row[3],
    yh_symbol: row[4],
    name: row[2],
    asset_type: row[1]
  )
end

CSV.foreach('notes/InitialETFs.csv') do |row|
  Underlying.create(
    tda_symbol: row[0],
    om_symbol: row[0],
    yh_symbol: row[0],
    name: row[2],
    asset_type: row[1]
  )
end
