class GlobalConfig
  @@holidays = [
    Date.new(2016, 1, 1),
    Date.new(2016, 1, 18),
    Date.new(2016, 2, 15),
    Date.new(2016, 3, 25),
    Date.new(2016, 5, 30),
    Date.new(2016, 7, 4),
    Date.new(2016, 9, 5),
    Date.new(2016, 12, 26),
    Date.new(2017, 1, 2),
    Date.new(2017, 1, 16),
    Date.new(2017, 2, 20),
    Date.new(2017, 4, 14),
    Date.new(2017, 5, 29),
    Date.new(2017, 7, 4),
    Date.new(2017, 9, 4),
    Date.new(2017, 11, 23),
    Date.new(2017, 12, 25)
  ]

  @@one_pm_closes = [
    Date.new(2016, 11, 24),
    Date.new(2017, 7, 3),
    Date.new(2017, 11, 24)
  ]

  def self.banned_stocks
    # report same store monthly retail sales
    %w(BKE CATO COST GPS LB SMRT WAG ZUMZ)
  end

  def self.concurrent_chain_requests
    6
  end

  def self.daily_options_volume_cutoff
    1500
  end

  def self.daily_options_volume_removal
    500
  end

  def self.data_table_page_size
    50
  end

  # keyed by CBOE symbol. val is OM symbol
  def self.deviant_symbols
    { 'BRKB' => 'BRK.B' }
  end

  def self.expiration_dates
    expmonths = expiration_months
    startdate =
      Date.new(expmonths.first[0..3].to_i, expmonths.first[4..6].to_i, 1)
    enddate = Date.new(expmonths.last[0..3].to_i, expmonths.last[4..6].to_i, -1)
    (startdate..enddate).select(&:friday?).map do |d|
      @@holidays.include?(d) ? d - 1 : d
    end
  end

  def self.expiration_months
    today = Time.zone.today
    months = months_beyond_front_month

    last_day_of_month = Date.new(today.year, today.month, -1)
    last_friday_of_this_month =
      last_day_of_month - ((last_day_of_month.wday - 5) % 7)

    # range = if today <= last_friday_of_this_month
    #           (0..months)
    #         else
    #           (1..months + 1)
    #         end

    range = today <= last_friday_of_this_month ? (0..months) : (1..months + 1)

    expmonths = range.map do |n|
      (today >> n).year.to_s + format('%02d', (today >> n).month.to_s)
    end

    expmonths
  end

  def self.hours_to_expiration(now, days)
    # TODO: account for expiration on half day
    # now = now.getutc
    exp_time = (now + (days.to_i * 60 * 60 * 24)).change(hour: 16, min: 0)
    ((exp_time - now) / 3600).round(2).to_s
  end

  # not sure how to handle these symbols, exclude them
  def self.index_symbol_skip(om_symbol)
    %w(SPXPM XSP).include? om_symbol
  end

  def self.linear_regression_period
    20
  end

  def self.market_open?(current_time)
    yr = current_time.year
    mo = current_time.month
    da = current_time.day

    today = Date.new(yr, mo, da)
    return false if today.saturday? || today.sunday?
    return false if @@holidays.include? today
    market_open = Time.zone.parse("#{yr}-#{mo}-#{da} 09:30:00")
    close_time = @@one_pm_closes.include?(today) ? '13:00:00' : '16:00:00'
    market_close = Time.zone.parse("#{yr}-#{mo}-#{da} #{close_time}")
    current_time.between?(market_open, market_close)
  end

  def self.months_beyond_front_month
    2
  end

  def self.monthly_expiration_date?(date)
    startdate = Date.new(date.year, date.month, 1)
    enddate = Date.new(date.year, date.month, -1)
    third_friday = (startdate..enddate).select(&:friday?).map do |d|
      @@holidays.include?(d) ? d - 1 : d
    end[2]
    date == third_friday
  end

  def self.options_craft_version
    'v.00.00.01'
  end

  def self.position_fields
    %w(description
       bid
       ask
       open_interest
       underlying_symbol
       delta
       volume
       implied_volatility)
  end

  def self.rsi_data_points
    45
  end

  def self.rsi_period
    14
  end

  def self.slider_profile
    {
      historic_volatility: { min: 0, max: 50 },
      implied_volatility: { min: 10, max: 200 },
      iv_rank: { min: 30, max: 150 },
      rsi: { min: 20, max: 70 },
      trend: { min: -0.5, max: 0.5 },
      recent_price: { min: 50, max: 150 }
    }
  end

  def self.special_index_fees
    {
      'SPX' => 0.53,
      'SPXW' => 0.53,
      'SPXPM' => 0.40,
      'RUT' => 0.18,
      'RUTQ' => 0.18,
      'VIX' => 0.36,
      'OEX' => 0.40,
      'XEO' => 0.40,
      'DJX' => 0.18,
      'NDX' => 0.14
    }
  end

  def self.tda_login_retries
    2
  end

  # number of times to retry the options symbols, random fails
  def self.tda_option_quote_hack
    5
  end

  def self.valid_trade_types
    ['credit-spread', 'iron-condor', 'debit-spread']
  end

  def self.williams_r_period
    14
  end

  def self.yahoo_earnings_date_exception
    { 'GOOG' => 'GOOGL' }
  end

  def self.yahoo_history_days_back
    80
  end

  def self.yahoo_symbol_exception
    { '^DJX' => { symbol: '^DJI', divisor: 100 } }
  end
end
