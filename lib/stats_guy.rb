class StatsGuy
  # TEST THINGS
  def self.mean(xs)
    xs.inject(0, :+) / xs.size.to_f
  end

  def self.variance(xs)
    xmean = mean(xs)
    (xs.reduce(0) { |acc, elem| (elem - xmean)**2 + acc }) / xs.size.to_f
  end

  # length xs = length ys
  def self.covariance(xs, ys)
    xmean = mean(xs)
    ymean = mean(ys)
    (xs.zip(ys).reduce(0) do |acc, obj|
      (obj[0] - xmean) * (obj[1] - ymean) + acc
    end) / xs.size.to_f
  end

  # assumes that values occur from earliest to latest
  def self.linear_regression_beta(ys)
    n = ys.size
    xs = (1..ys.size).to_a
    covariance(xs, ys) / ((1.0 / 12) * (n * n - 1))
  end

  def self.calc_trend_data(symbol_data)
    result = {}
    symbol_data.each do |key, val|
      result[key] = trend_hash(val)
    end
    result
  end

  def self.trend_hash(close_history)
    recent_price = close_history[0][0]
    rsi = nil
    if close_history.size >= GlobalConfig.rsi_period
      rsi = format '%.2f', relative_strength_index(close_history)
    end

    ret_hash = {
      trend: linear_regression_slope(recent_price, close_history),
      price: recent_price,
      rsi: rsi
    }
    ret_hash
  end

  def self.linear_regression_slope(recent_price, close_history)
    slope = nil
    lr_period = GlobalConfig.linear_regression_period
    if close_history.size >= lr_period
      lr_list = close_history.take(lr_period).reverse.map { |x| x[0] }
      slope =
        format '%.2f',
               ((100 * StatsGuy.linear_regression_beta(lr_list)) / recent_price)
    end
    slope
  end


  # splits or other events cause data to be inconsistent, the adj_close
  # will be changed in the history but the Open Low High Close will not

  def self.adjust_for_close(history)
    history.each do |row|
      adjustment = row[6] / row[4]
      next unless format('%.2f', adjustment) != '1.00'
      [1, 2, 3, 4].each do |n|
        row[n] *= adjustment
      end
    end
  end

  def self.williams_r(history)
    adjust_for_close(history)
    high = history.map { |x| x[2] }.max
    low = history.map { |x| x[3] }.min
    last_close = history[0][4]
    ((high - last_close) / (high - low)) * -100
  end

  def self.relative_strength_index(history)
    period = GlobalConfig.rsi_period
    data_points = GlobalConfig.rsi_data_points
    data_points = history.length if history.length < data_points
    closes = history.take(data_points).reverse.map { |arr| arr[0].round(2) }

    # difference from previous day and day
    diffs = closes.drop(1).map.with_index { |prev, i| (prev - closes[i]).round(2) }

    # split into gains and loss arrays
    gains =  diffs.map { |x| x <= 0 ? 0 : x }
    losses = diffs.map { |x| x.positive? ? 0 : (-1.0 * x) }

    # wilder starts with simple average of 14 initial days
    avg_gain = gains.take(period).inject(0, :+) / period
    avg_loss = losses.take(period).inject(0, :+) / period

    # Wilder smooth moving average, process rest of the data (minus 2??? but it's working)
    (period...gains.length).each do |idx|
      avg_gain = ((period - 1) * avg_gain.round(2) + gains[idx]) / period
      avg_loss = ((period - 1) * avg_loss.round(2) + losses[idx]) / period
    end
    rs = avg_gain.round(2) / avg_loss.round(2)
    rsi = (100 - (100 / (1 + rs.round(2))))
    rsi.round(2)
  end

  # HERE LIES PROBABILITY #
  def self.normal_cdf(x)
    t = 1.0 / (1.0 + 0.2316419 * x.abs)
    d = 0.3989423 * Math.exp(-x * x / 2)
    prob =
      d * t *
      (0.3193815 + t * (-0.3565638 + t * (1.781478 + t * (-1.821256 + t * 1.330274))))
    x.positive? ? (1 - prob) : prob
  end

  def self.prob_below(stock_price, strike_price, implied_volatility, hours_to_exp)
    hours_to_exp = 0 if hours_to_exp.to_f.negative?
    v = implied_volatility.to_f / 100
    t = hours_to_exp.to_f / (24 * 365)
    normal_cdf(Math.log(strike_price.to_f / stock_price.to_f) / (v * Math.sqrt(t)))
  end

  def self.prob_between(price1, price2, current_price, implied_volatility, hours_to_exp)
    prob1 = prob_below(current_price, price1, implied_volatility, hours_to_exp)
    prob2 = prob_below(current_price, price2, implied_volatility, hours_to_exp)
    price1 > price2 ? (prob1 - prob2) : (prob2 - prob1)
  end

  def self.error_function(x)
    a1 = 0.278393
    a2 = 0.230389
    a3 = 0.000972
    a4 = 0.078108
    1.0 - (1.0 / (1.0 + a1 * x + a2 * x * x + a3 * x ** 3 + a4 * x ** 4) ** 4)
  end

  def self.cc_prob_test(y, u, s, hours_to_exp)
    t = hours_to_exp.to_f / (24 * 365)
    s = (s.to_f / 100) * Math.sqrt(t)
    u = u.to_f
    y = y.to_f
    puts s, u, y
    # puts s, u, y
    # y = (y.to_f - u) / s
    # c1 = Math.exp((s ** 4 + 2 * s * u)/(2 * s * s))
    c1 = Math.exp(((s * s) / 2) + u)
    # puts ((s * s) / 2) + u
    # puts c1
    c2 = (s * s + u - Math.log(y)) / (Math.sqrt(2) * s)
    puts error_function(c2)
    0.5 * c1 * (1 - error_function(c2))
  end
end
