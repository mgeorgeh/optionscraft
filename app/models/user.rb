class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable, :timeoutable, :confirmable
  has_many :trade_param_sets
  has_many :positions
  before_create :set_default_trade_params

  private

  def set_default_trade_params
    self.trade_param_sets = [TradeParamSet.default_set]
    # self.save
  end
end
