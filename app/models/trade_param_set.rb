class TradeParamSet < ActiveRecord::Base
  belongs_to :user
  def self.default_set
    TradeParamSet.new(
      name: 'MySet',
      near_strike_max_delta: 0.15,
      min_open_interest: 200,
      min_mark_premium: 0.15,
      max_margin_per_trade: 1000,
      min_percent_rom: 0.01,
      avoid_earnings: true,
      total_margin: 5000,
      commission_base: 4.95,
      commission_per_contract: 0.50,
      commission_min: 0.0,
      recommended_flag: true,
      ds_near_strike_min_delta: 0.35
      # avoid_dividends: nil,
      # min_options_volume: nil,
    )
  end
end
