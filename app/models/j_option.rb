require 'global_config'
class JOption < ActiveRecord::Base
  @@fields = GlobalConfig.position_fields
  @@compressed_cols = %w(strike_price bid ask open_interest
                         delta volume implied_volatility)

  def self.init(om_symbol, tda_hash, last, date, days_to_expiration, should_avoid)
    return nil unless sensible_option(last, tda_hash)
    strike_price = tda_hash['strike_price']
    opt_type = tda_hash['put']['in_the_money'] == 'true' ? 'call' : 'put'
    opt = tda_hash[opt_type]
    vals = opt.slice(*@@fields).values
    return nil unless vals.compact.length == vals.length

    data_hash = {
      'avoid_flag' => should_avoid,
      'days_to_expiration' => days_to_expiration.to_f,
      'exp_date' => date,
      'strike_price' => strike_price.to_f,
      'opt_type' => opt_type,
      'description' => opt['description'],
      'bid' => opt['bid'].to_f,
      'ask' => opt['ask'].to_f,
      'open_interest' => opt['open_interest'].to_i,
      'underlying_symbol' => om_symbol,
      'delta' => opt['delta'].to_f,
      'volume' => opt['volume'].to_i,
      'implied_volatility' => opt['implied_volatility'].to_f
    }
    data_hash['compressed_form'] = compressed_cols.map { |f| data_hash[f] }.join(',')
    JOption.new do |o|
      o.data = data_hash
    end
  end

  def self.delete_rows(om_symbol, exp_month, reason)
    JOption.transaction do
      sql = "DELETE FROM j_options WHERE data->>'underlying_symbol'='#{om_symbol}' AND data->>'exp_date' LIKE '#{exp_month}%'"
      Option.connection.execute sql
    end
    puts reason
  end

  def self.update_database(optlist, om_symbol, exp_month)
    del_sql = "DELETE FROM j_options WHERE data->>'underlying_symbol'='#{om_symbol}' AND data->>'exp_date' LIKE '#{exp_month}%'"
    JOption.transaction do
      JOption.connection.execute del_sql
      vals = optlist.map { |opt| "('#{opt.data.to_json}')" }.join(', ')
      JOption.connection.execute "INSERT INTO j_options (data) values #{vals}"
    end
  end

  def self.sensible_option(last, obj)
    [
      obj['standard_option'] == 'true',
      obj.key?('put'),
      obj.key?('call'),
      obj['strike_price'] != last # both Strings
    ].all?
  end
end
