require 'global_config'

class Underlying < ActiveRecord::Base
  before_destroy { |u| Option.where(underlying_symbol: u.om_symbol).destroy_all }

  def self.create_from_csv_row(row, params)
    under = Underlying.find_by(om_symbol: row[0])
    opt_vol = row[params[:vol_index]].to_i
    if under
      under.update(avg_daily_opt_volume: opt_vol)
      return nil
    else
      Underlying.create(
        tda_symbol: row[0],
        om_symbol: row[0],
        yh_symbol: row[0],
        name: row[1],
        asset_type: params[:asset_type],
        avg_daily_opt_volume: opt_vol
      )
    end
  end
end
