require 'global_config'
class Option < ActiveRecord::Base
  @@fields = %w(description open_interest underlying_symbol
                delta volume)
  @@compressed_cols = %w(bid ask open_interest
                         delta volume implied_volatility)

  def self.init(should_avoid, days_to_exp, date, opt_data, om_symbol)
    Option.new do |o|
      opt = opt_data[:data]
      o.avoid_flag = should_avoid
      o.days_to_expiration = days_to_exp.to_i
      o.exp_date = date
      o.strike_price = opt_data[:strike_price].to_f
      o.opt_type = opt_data[:opt_type]
      o.description = opt['description']
      o.bid = opt['bid'].to_f
      o.ask = opt['ask'].to_f
      o.open_interest = opt['open_interest'].to_i
      o.underlying_symbol = om_symbol
      o.delta = opt['delta'].to_f
      o.volume = opt['volume'].to_i
      o.implied_volatility = opt['implied_volatility'].to_f
      o.compressed_form = "#{o.strike_price},#{o.bid},#{o.ask},#{o.open_interest}," \
                          "#{o.delta},#{o.volume},#{o.implied_volatility}"
    end
  end

  def self.contains_all_fields(opt)
    !opt.slice(*@@fields).values.any?(&:nil?)
  end

  def self.update_database(optlist, om_symbol, exp_month)
    col_names = Option.column_names.drop(1)
    cols = col_names.join(', ')
    del_sql = "DELETE FROM options WHERE underlying_symbol='#{om_symbol}'" \
              " AND exp_date LIKE '#{exp_month}%'"
    Option.transaction do
      # delete
      Option.connection.exec_query del_sql
      # insert
      tostring = lambda do |opt, name|
        field = opt.send(name)
        field.is_a?(String) ? "'#{field}'" : field
      end
      vals = optlist.map do |opt|
        '(' + col_names.map { |n| tostring.call(opt, n) }.join(', ') + ')'
      end.join(', ')
      Option.connection.exec_query "INSERT INTO options (#{cols}) values #{vals}"
    end
  end

  def self.delete_rows(om_symbol, exp_month, reason)
    Option.transaction do
      sql = "DELETE FROM options WHERE underlying_symbol='#{om_symbol}'" \
            " AND exp_date LIKE '#{exp_month}%'"
      Option.connection.exec_query sql
    end
    puts reason
  end

  # underlying_symbol, opt_type, etc are in scope because of ActiveRecord weirdness
  def same_option?(opt)
    [
      underlying_symbol == opt.underlying_symbol,
      exp_date == opt.exp_date,
      opt_type == opt.opt_type,
      strike_price == opt.strike_price
    ].all?
  end

  def fill(tda_strikes, days_to_exp, should_avoid)
    strike = tda_strikes.find do |s|
      s['strike_price'].to_f == strike_price
    end[opt_type]
    self.avoid_flag = should_avoid
    self.days_to_expiration = days_to_exp
    self.description = strike['description']
    self.bid = strike['bid'].to_f
    self.ask = strike['ask'].to_f
    self.open_interest = strike['open_interest'].to_i
    self.delta = strike['delta'].to_f
    self.volume = strike['volume'].to_i
    self.implied_volatility = strike['implied_volatility'].to_f
    self.compressed_form = "#{strike_price},#{bid},#{ask},#{open_interest}," \
                           "#{delta},#{volume},#{implied_volatility}"
  end

  def self.sensible_option(last, obj)
    [
      obj['standard_option'] == 'true',
      obj.key?('put'),
      obj.key?('call'),
      obj['strike_price'] != last # both Strings
    ].all?
  end
end
