class Position < ActiveRecord::Base
  belongs_to :user
  validates :user_id, uniqueness: { scope: [:symbol,
                                            :expiration,
                                            :strikes,
                                            :opt_type] }

  def self.add_positions_for_user(arr, user_id)
    arr.each do |cs|
      cs_obj = Position.new(symbol: cs['Symbol'],
                            opt_type: cs['Type'],
                            expiration: cs['expiration'],
                            strikes: cs['Strikes'],
                            num_contracts: cs['#'].to_i,
                            margin: cs['Margin'].to_i,
                            mark_premium: cs['Mark Prem'].to_f,
                            market_premium: cs['Market Prem'].to_f,
                            user_id: user_id)
      cs_obj.save if cs_obj.valid?
    end
  end

  def self.from_option_list(list, date, days_to_expiration)
    # each with index?!
    # monkeying with indexes is probably much faster
    # [options] -> [puts,calls] -> [putspreads, callspreads]
    intermediate = list.partition { |opt| opt.type == 'put' }
    intermediate[1].reverse!
    intermediate.flat_map do |optlist|
      optlist.flat_map.with_index do |near_leg, i|
        optlist.drop(i + 1).map do |far_leg|
          Position.new(near_leg, far_leg, date, days_to_expiration)
        end
      end
    end
  end

  def self.handle_updates(spread_list)
    spread_list.each(&:save)
  end

  def constituent_options
    strikes.split('/').map do |s|
      Option.new(
        underlying_symbol: symbol,
        exp_date: expiration,
        opt_type: opt_type,
        strike_price: s.to_f
      )
    end
  end
end
