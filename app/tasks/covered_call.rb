require 'stats_guy'
require 'cache_method'
require 'broker_apis/oh_api'
require 'broker_apis/tda_curl_api'
# underlying_symbol, strie_price, kind, closing price
# profit, Eprof, pop, success?, actual profit, should_exit, eloss, exit cost
class CoveredCall

  attr_accessor :quote_data

  def initialize(opt, user)
    @option = opt
    @user = user
    @underlying = Underlying.find_by om_symbol: @option.underlying_symbol
    @quote_data = nil
  end

  def delta
    @option.delta
  end

  def to_hash
    {
      underlying_symbol: @underlying.om_symbol,
      strike_price: @option.strike_price,
      type: @option.opt_type,
      closing_price: closing_price,
      profit: profit,
      eprof: eprof,
      pop: pop,
      actual_profit: actual_profit,
      should_exit: should_exit?,
      eloss: expected_value,
      exit_cost: exit_cost,
      exp_date: exp_date
    }
  end

  def should_exit?
    false
  end

  def exit_cost
    1
  end

  def actual_profit
    1
  end

  def pop
    1
  end

  def today
    now = Time.zone.now
    Date.new(now.year, now.month, now.day)
  end

  cache_method :today

  def closing_price
    if today > exp_date
      TDACurlApi.instance.closing_quote_for(tda_symbol, exp_date)
    else
      OHApi.instance.quote_request('mark', symbol).to_f
    end
  end

  cache_method :closing_price

  def exp_date
    Date.parse(@option.exp_date)
  end

  cache_method :exp_date

  def tda_symbol
    @underlying.tda_symbol
  end

  def symbol
    @underlying.om_symbol
  end

  def ivol
    @underlying.implied_volatility
  end

  def profit
    mark_premium * 100 - fees # 1 contract
  end

  def margin
    stock_price * 100
  end

  # ignoring stock purchase fees
  def fees
    trade_params.commission_base + trade_params.commission_per_contract
  end

  def trade_params
    @user.trade_param_sets.first
  end

  cache_method :trade_params

  def eprof
    profit + expected_value
  end

  cache_method :eprof

  def eprom
    eprof / margin
  end

  # should always be negative
  # def expected_value
  #   l = low_bound
  #   u = low_bound + rectangle_width
  #   sum = 0
  #   while u < strike_price
  #     sum += prob_between(l, u) * ((l + u) / 2)
  #     l = u
  #     u += rectangle_width
  #     num_rects += 1
  #   end
  #   (val_beyond_strike + sum) * 100
  # end

  def num_rectangles
    50
  end

  def expected_value
    low_bound = 0.0
    up_bound = strike_price
    interval = (up_bound - low_bound) / num_rectangles
    sum = 0
    l = low_bound
    u = low_bound + interval
    num_rectangles.to_i.times do
      price = (l + u) / 2
      sum += prob_between(l, u) * price
      l = u
      u += interval
    end
    (val_beyond_strike + sum) * 100
  end

  cache_method :expected_value

  def prob_between(price1, price2)
    StatsGuy.prob_between(price1, price2, closing_price, ivol, hours_to_exp)
  end

  def val_beyond_strike
    prob = StatsGuy.prob_below(stock_price, strike_price, underlying_ivol, hours_to_exp)
    (1 - prob) * strike_price
  end

  def rectangle_width
    0.5
  end

  # def low_bound
  #   0
  # end

  def stock_price
    @underlying.recent_price
  end

  def underlying_ivol
    @underlying.implied_volatility
  end

  def hours_to_exp
    GlobalConfig.hours_to_expiration(Time.zone.now, @option.days_to_expiration)
  end

  def strike_price
    @option.strike_price
  end

  def mark_premium
    (@option.bid + @option.ask) / 2
  end

  def description
    "#{@underlying.om_symbol} #{strike_price}"
  end
end

class Time
  def to_ms
    (self.to_f * 1000.0).to_i
  end
end
