require 'broker_apis/yh_api'

# for stastical purposes. not used in final product
class YahooFetcher
  def self.run
    date = Time.zone.today

    Underlying.all.each do |u|
      path = Rails.root.to_s + "/csv/#{u.om_symbol}.csv"
      File.write(path, YHApi.instance.fetch_historical_data(u.yh_symbol, date))
    end
  end
end
