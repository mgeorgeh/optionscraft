require 'broker_apis/yh_api'

# shcedule once a day, can be after hours
class UpdateNextEarningsDate
  def run
    Underlying.where(asset_type: 'Equity').find_each do |row|
      print "symbol = #{row.yh_symbol}, "
      proper_yh_symbol =
        GlobalConfig.yahoo_earnings_date_exception[row.yh_symbol] || row.yh_symbol
      result = YHApi.instance.yahoo_earnings_date(proper_yh_symbol)
      puts "date = #{result}"
      row.next_earnings_date = result
      row.save
    end
  end
end

UpdateNextEarningsDate.new.run
