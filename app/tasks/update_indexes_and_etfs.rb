require 'global_config'

# http://www.cboe.com/publish/MonthlyVolume/SEPTEMBER%202015%20News%20Release%20attachment.xls
# http://www.cboe.com/publish/MonthlyVolume/SEPTEMBER 2015 News Release attachment.xls
# http://www.cboe.com/publish/MonthlyVolume/OCTOBER%20%202016%20News%20Release%20attachment.xls
# http://www.cboe.com/publish/MonthlyVolume/MARCH%202017%20News%20Release%20attachment.xls
# schedule daily, only runs once a month
# NEVER UPDATE AGAIN?!

# OM    TDA     YH
# SPX, $SPX.X, ^GSPC
# NDX, $NDX.X, ^NDX
# RUT, $RUT.X, ^RUT
# VIX, $VIX.X, ^VIX
# DJX, $DJX.X, ^DJX
# OEX, $OEX.X, ^OEX
class UpdateIndexesAndETFs < CBOEUpdate
  # --fail stops curl from downloading a 404 page
  def download_spreadsheet
    `#{@curl} --fail #{spreadsheet_url} -o #{xls_filepath}`
    `#{@curl} --fail #{alt_spreadsheet_url} -o #{xls_filepath}` if download_failed?
  end

  def prev_month
    Date::MONTHNAMES[(@today - 1.month).month].upcase
  end

  def prev_year
    (@today - 1.month).year.to_s
  end

  def find_underlyings
    "asset_type='Index' or asset_type='ETF'"
  end

  def symbols_to_skip
    proc do |row|
      GlobalConfig.index_symbol_skip(row[0])
    end
  end

  def create_params
    {
      asset_type: 'ETF',
      vol_index: vol_index
    }
  end

  def vol_index
    3
  end

  def csv_blank_rows
    5
  end

  def old_file_glob
    Dir.glob("#{Rails.root}/spreadsheets/*index_vol_sheet*")
  end

  def xls_filepath
    "#{Rails.root}/spreadsheets/#{prev_date_str}index_vol_sheet.xls"
  end

  def csv_filepath
    "#{Rails.root}/spreadsheets/#{prev_date_str}index_vol_sheet.csv"
  end

  # def spreadsheet_url
  #   "http://www.cboe.com/publish/MonthlyVolume/#{prev_month}%20" \
  #   "#{prev_year}%20News%20Release%20attachment.xls"
  # end
  def spreadsheet_url
    "http://www.cboe.com/publish/MonthlyVolume/MARCH%202017%20News%20Release%20attachment.xls"
  end

  def alt_spreadsheet_url
    "http://www.cboe.com/publish/MonthlyVolume/#{prev_month}%20%20" \
    "#{prev_year}%20News%20Release%20attachment.xls"
  end
end

UpdateIndexesAndETFs.new.run
