require 'broker_apis/oh_api'
require 'broker_apis/yh_api'
require 'global_config'
require 'json_fetcher'
require 'c_spread'
require 'quote_data'

# cleaner FT implementation
class ForwardTest
  class << self
    attr_reader :spreadsheets_dir
  end

  @spreadsheets_dir = "#{Rails.root}/spreadsheets"

  def initialize(user, should_fetch_json, force_no_fetch)
    @now = Time.zone.now
    @user = user
    init_dates(should_fetch_json, force_no_fetch)
    @path = self.class.spreadsheets_dir + '/credit_spread_data*.json'
    file_list = Dir.glob(@path)
    @timestamp = file_list[-1][-19..-6]
    file = File.read(file_list[-1])
    @cs_json = JSON.parse(file)
    raise 'Exiting: the json file is empty.' if @cs_json.empty?
    init_underlying_list
  end

  def run
    select_spreads
    display_spreads
    save_spread_file
  end

  def quote_data_source
    QuoteData
  end

  def select_spreads
    json_spreads =
      @cs_json.select { |s| right_expiration(s) }[0]['trades']
              .select { |s| desirable(s) }
              .group_by { |s| s['underlying_symbol'] }
              .flat_map(&highest_eprom_spreads)

    exp_date_str = @cs_json[0]['date']
    @quote_data = quote_data_source.new(json_spreads, exp_date_str).fetch
    @selected_spreads = json_spreads.map(&mk_spread)
  end

  def highest_eprom_spreads
    lambda do |_, spreads|
      put_spreads = spreads.select { |s| s['type'] == 'put' }
      call_spreads = spreads.select { |s| s['type'] == 'call' }
      [
        put_spreads.max_by { |s| s['eprom'].to_f },
        call_spreads.max_by { |s| s['eprom'].to_f }
      ].compact
    end
  end

  def desirable(json_spread)
    json_spread['eprof'].to_f >= 0 &&
      json_spread['profit'].to_f >= 10 &&
      json_spread['near']['delta'].to_f.abs <= 0.15
  end

  def right_expiration(json_spread)
    json_spread['date'] == @expiration
  end

  def fetch_today
    Time.zone.today
  end

  def fetch_next_exp_date
    GlobalConfig.expiration_dates.select { |d| d >= @today }[0]
  end

  def init_dates(should_fetch_json, force_no_fetch)
    @today = fetch_today
    @date = fetch_next_exp_date
    @day_of_week = @today.wday
    fetch_json if (@day_of_week == 1 || should_fetch_json) && !force_no_fetch
    @date = @today - 1 if @day_of_week == 6
    @date = @today - 2 if @day_of_week.zero?
    @expiration = @date.strftime('%Y%m%d')
  end

  def init_underlying_list
    @underlying_path = ForwardTest.spreadsheets_dir +
                       "/underlying_data#{@timestamp}.json"
    @underlyings = JSON.parse(File.read(@underlying_path))
  end

  def mk_spread
    proc do |json_spread|
      current_db_underlying = Underlying.find_by om_symbol: json_spread['underlying_symbol']
      json_underlying = @underlyings.find do |u|
        u['om_symbol'] == json_spread['underlying_symbol']
      end
      u = current_db_underlying.as_json || json_underlying
      if json_spread['type'] == 'put'
        Put.new(json_spread, u, @user, @now, @date, @quote_data)
      else
        Call.new(json_spread, u, @user, @now, @date, @quote_data)
      end
    end
    # FTCreditSpread.init(json_spread, @underlyings)
  end

  def fetch_json
    puts 'fetching json'
    YAHOO_HTTPCLIENT.get 'http://localhost:3000'
  rescue Errno::ECONNREFUSED
    prid = spawn "#{Rails.root}/bin/rails server", [:out, :err] => '/dev/null'
    sleep 10
    JsonFetcher.new(@user).forward_test
    Process.kill('KILL', prid)
  else
    JsonFetcher.new(@user).forward_test
  end

  def two_dec(num)
    format '%.2f', num
  end

  def text_header
    total = @selected_spreads.length
    t = Time.zone.now
    tstr = t.strftime("%m/%d/%Y %H:%M #{t.zone}")

    "#{tstr}, Exp: #{@expiration}, Expected wins: " \
    "#{two_dec @selected_spreads.sum(&:prob_of_prof)}/#{total}," \
    "Actual wins: #{two_dec @selected_spreads.count(&:success)}/#{total}," \
    "Expected Profit: #{two_dec @selected_spreads.sum(&:eprof)}," \
    "Max Profit: #{two_dec @selected_spreads.sum(&:profit)}," \
    "Total Real Profit: #{two_dec @selected_spreads.sum(&:actual_profit)}," \
    "Current Eloss: #{two_dec @selected_spreads.sum(&:expected_loss)}\n"
  end

  def csv_header
    "Symbol,Strikes,Type,Contracts,Price,Profit,Eprof,pop,success?," \
    "Actual Profit,Should Exit,Eloss,Exit Cost\n"
  end

  def display_spreads
    @selected_spreads = @selected_spreads.sort_by(&:symbol)
    @text = text_header + csv_header + @selected_spreads.map(&:format_text).join("\n")
    puts @text
  end

  def save_spread_file
    output_path = ForwardTest.spreadsheets_dir + '/forward_test/FT_' +
                  @today.strftime('%Y%m%d') + '.csv'
    File.write(output_path, @text)
  end
end
