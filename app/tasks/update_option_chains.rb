require 'broker_apis/tda_curl_api'
require 'global_config'

class UpdateOptionChains
  TD_SYMBOL_ERROR = 'Please enter a valid symbol.'.freeze
  XML_PARSE_ERROR = 'from_xml failed to parse'.freeze
  XML_CHAIN_RESULT_ERROR = 'option_chain_result field nil'.freeze

  def initialize
    @om_symbols_to_skip = []
    @current_day = Date.current
    @cs_options = Position.all.flat_map(&:constituent_options).uniq do |o|
      [o.underlying_symbol, o.exp_date, o.opt_type, o.strike_price]
    end
    @avoidable_series = series_under_earnings_pressure
  end

  def run
    unless GlobalConfig.market_open?(Time.zone.now)
      puts 'MARKET CLOSED'
      return
    end
    hydra = Typhoeus::Hydra.new(
      max_concurrency: GlobalConfig.concurrent_chain_requests
    )

    remove_expired_options

    Underlying.find_each do |u|
      GlobalConfig.expiration_months.each do |exp|
        request = TDACurlApi.instance.make_chain_request(u.tda_symbol, exp)
        request.on_complete do |response|
          write_options_to_db(
            response.body,
            u.om_symbol,
            exp,
            @avoidable_series.fetch(u.om_symbol) { [] }
          )
        end
        hydra.queue request
      end
    end

    hydra.run
  end

  def remove_expired_options
    current_date_str = @current_day.strftime('%Y%m%d')
    Option.where('exp_date < ? ', current_date_str).delete_all
  end

  def exps_to_avoid(u, exp_dates)
    d = u.next_earnings_date
    return [] if ['None', 'N/A'].include? d
    d = attach_year(d.split(' - ')[0]) if d.include? 'Est.'
    earnings_date = Date.parse d
    exp_dates.select { |date| date >= earnings_date }
             .map { |date| [u.om_symbol, date.strftime('%Y%m%d')] }
  end

  def attach_year(date_str)
    d = Date.parse(date_str)
    if d.month <= @current_day.month
      date_str + " #{@current_day.year + 1}"
    else
      date_str + " #{@current_day.year}"
    end
  end

  def series_under_earnings_pressure
    exp_dates = GlobalConfig.expiration_dates
    tmp1 = Underlying.all
                     .select('om_symbol, next_earnings_date')
                     .flat_map { |u| exps_to_avoid(u, exp_dates) }
                     .group_by { |e| e[0] }
    tmp1.each do |k, _v|
      tmp1[k] = tmp1[k].map { |arr| arr[1] }
    end
    tmp1
  end

  # modify is a shorter way, I think
  def update_underlying_recent_price(symbol, last)
    u = Underlying.find_by om_symbol: symbol
    u.recent_price = last.to_f.round(2)
    u.save
  end

  def log_bad_hash_error(err_str, om_symbol, exp_month, xmlbody)
    puts err_str
    puts om_symbol + '!'
    puts exp_month
    puts xmlbody
  end

  def validate_option_chain_xml(xmlbody, om_symbol, exp_month)
    if @om_symbols_to_skip.include? om_symbol
      puts "Skipping #{om_symbol}"
      return nil
    end
    # JOption run_from_xml time: 1m30.492s
    # Option run_from_xml time: 1m1.920s
    puts "Processing symbol: #{om_symbol}, expiration: #{exp_month}"

    chain_hash = Hash.from_xml(xmlbody)
    log_bad_hash_error(XML_PARSE_ERROR, om_symbol, exp_month, xmlbody) unless chain_hash

    chain_hash = chain_hash['amtd']
    if chain_hash['result'] == 'FAIL' && chain_hash['error'] == TD_SYMBOL_ERROR
      @om_symbols_to_skip.push(om_symbol)
      delete_underlying_from_table(om_symbol, 'invalid symbol')
      return nil
    end

    chain_hash = chain_hash.fetch('option_chain_results') do
      log_bad_hash_error(XML_CHAIN_RESULT_ERROR, om_symbol, exp_month, xmlbody)
      return nil
    end

    last = chain_hash['last']
    update_underlying_recent_price(om_symbol, last)

    option_date = chain_hash.fetch('option_date') do |_el|
      Option.delete_rows(om_symbol, exp_month, 'no option date')
      return nil
    end

    option_date = [option_date] if option_date.is_a?(Hash)

    { last: last, option_date: option_date }
  end

  def choose_put_or_call(strike)
    strike_price = strike['strike_price']
    opt_type = strike['put']['in_the_money'] == 'true' ? 'call' : 'put'
    data = strike[opt_type]
    { data: data, opt_type: opt_type, strike_price: strike_price }
  end

  def write_options_to_db(xmlbody, om_symbol, exp_month, avoidable_series)
    valid_chain = validate_option_chain_xml(xmlbody, om_symbol, exp_month)
    return unless valid_chain

    option_list = valid_chain[:option_date].flat_map do |chain|
      date = chain['date']
      should_avoid = avoidable_series.include? date
      days_to_expiration = chain['days_to_expiration']

      if chain['option_strike'].is_a?(Hash)
        chain['option_strike'] = [chain['option_strike']]
      end

      normal_options =
        chain['option_strike']
        .select { |strike| Option.sensible_option(valid_chain[:last], strike) }
        .map { |strike| choose_put_or_call(strike) }
        .select { |opt| Option.contains_all_fields(opt[:data]) }
        .map { |opt| Option.init(should_avoid, days_to_expiration, date, opt, om_symbol) }

      stray_options = @cs_options.select do |o|
        o.underlying_symbol == om_symbol &&
          o.exp_date == date &&
          !normal_options.index { |opt| o.same_option?(opt) }
      end

      stray_options.each do |opt|
        opt.fill(chain['option_strike'], days_to_expiration, should_avoid)
      end

      normal_options + stray_options
    end

    if option_list.empty?
      return Option.delete_rows(om_symbol, exp_month, 'no valid options')
      # return JOption.delete_rows(om_symbol, exp_month, "no valid options")
    end
    Option.update_database(option_list, om_symbol, exp_month)
    # JOption.update_database(option_list, om_symbol, exp_month)
  end

  def j_delete_underlying_from_table(om_symbol, _reason)
    # delete from underlying
    u = Underlying.find_by om_symbol: om_symbol
    if u
      u.destroy
      puts "Deleted #{om_symbol} from Underlying table"
    end
    # delete from option
    JOption.transaction do
      JOption.connection.exec_query 'DELETE FROM j_options WHERE data->>' \
      "'underlying_symbol'='#{om_symbol}'"
    end
  end

  def delete_underlying_from_table(om_symbol, _reason)
    # delete from underlying
    u = Underlying.find_by om_symbol: om_symbol
    if u
      u.destroy
      puts "Deleted #{om_symbol} from Underlying table"
    end
    # delete from option
    Option.transaction do
      Option.where(underlying_symbol: om_symbol).destroy_all
    end
  end

  def add_label(h)
    label = h['option_strike'][0]['description'].split(' ')[1..-3]
    label[-1].slice!(-4, 2)
    label = label.join(' ').sub('(Weekly)', 'W').sub('(Quarterly)', 'Q')
    h.merge('expiration_label' => label)
  end

  def make_cs_options
  end
end
