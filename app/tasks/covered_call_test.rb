require 'covered_call'
class CoveredCallTest
  def initialize
    @today = Time.zone.today
    @user = User.find_by email: 'forwardtest@ghweb.com'
    @trade_params = nil
  end

  def run
    generate_trades if should_generate_trades?
    update_trades
  end

  def update_trades
  end

  def generate_trades
    File.write(output_path, trades_list.to_json)
  end

  def should_generate_trades?
    (day_of_week == 1 || force_fetch?) && !force_no_fetch?
  end

  def day_of_week
    @today.wday
  end

  def force_fetch?
    ARGV.include?('force-fetch')
  end

  def force_no_fetch?
    ARGV.include?('no-fetch')
  end

  def output_path
    "#{Rails.root}/spreadsheets/covered_call_data#{timestamp}.json"
  end

  def timestamp
    Time.zone.now.to_s.delete('-: ')[0...-4]
  end

  def trades_list
    opts = Option.where(option_query_string)
    opts.map(&to_covered_call)
        .group_by(&:symbol)
        .map(&max_eprom)
        .map(&:to_hash)
  end

  def option_query_string
    max_delta = trade_params.near_strike_max_delta
    min_oi = trade_params.min_open_interest
    avoid_earnings = trade_params.avoid_earnings
    min_vol = trade_params.min_options_volume
    "underlying_symbol in ('AAPL', 'TSLA', 'NFLX') and " \
    "opt_type = 'call' and exp_date = '#{next_exp_date}' and " \
    "@ delta <= #{max_delta} and open_interest >= #{min_oi} " \
    "and volume >= #{min_vol}" + (avoid_earnings ? ' and avoid_flag=FALSE' : '')
  end

  def trade_params
    fetch_trade_params if @trade_params.nil?
    @trade_params
  end

  def fetch_trade_params
    @trade_params = @user.trade_param_sets.first
  end

  def next_exp_date
    GlobalConfig.expiration_dates.select { |d| d >= @today }[0].strftime('%Y%m%d')
  end

  def max_eprom
    proc do |_, call_list|
      call_list.max_by(&:eprom)
    end
  end

  # def desirable
  #   proc do |cc|

  #   end
  # end

  def to_covered_call
    proc do |opt|
      CoveredCall.new(opt, @user)
    end
  end
end

CoveredCallTest.new.run
