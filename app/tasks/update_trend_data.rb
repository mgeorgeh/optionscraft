require 'broker_apis/yh_api'
require 'broker_apis/tda_curl_api'

# shcedule once a day, can be after hours
class UpdateTrendData
  def run
    symbols = Underlying.all.map(&:tda_symbol)
    symbol_data = TDACurlApi.instance.formatted_historical_data(symbols)
    trend_data = StatsGuy.calc_trend_data(symbol_data)
    Underlying.find_each do |row|
      trend_hash = trend_data[row.tda_symbol]
      next unless trend_hash
      puts "#{row.tda_symbol}, #{trend_hash}"
      row.trend = trend_hash[:trend]
      row.rsi = trend_hash[:rsi]
      row.save
    end
  end
end

UpdateTrendData.new.run
