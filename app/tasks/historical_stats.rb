require 'stats_guy'
class HistoricalStats
  # for every underlying get its csv
  # [Double]
  # get longest streak
  # for each length streak up to longest
  #   get list of closing prices following a streak of that length
  #   check the proportion that follow the streak
  #   print
  def run
    # Underlying.take(5).each do |u|
    @stats = Underlying.all.map { |u| TrendStats.new(u) }
    display_trend_stats
    display_hypothesis_stats
  end

  def display_trend_stats
    puts "max up trend following: #{@stats.max_by(&:up_proportion)}"
    puts "min up trend following: #{@stats.min_by(&:up_proportion)}"
    puts "max down trend following: #{@stats.max_by(&:down_proportion)}"
    puts "min down trend following: #{@stats.min_by(&:down_proportion)}"
  end

  def display_hypothesis_stats
    # up_trends = @stats.map { |item| item[:up_proportion] }
    up_trends = @stats.map(&:up_proportion)
    up_stats = test_hypothesis(up_trends)
    # down_trends = @stats.map { |item| item[:down_proportion] }
    down_trends = @stats.map(&:down_proportion)
    down_stats = test_hypothesis(down_trends)
    puts up_stats
    puts down_stats
  end

  def uptrend?(arr)
    arr[1] > arr[0]
  end

  def downtrend?(arr)
    arr[1] < arr[0]
  end

  def test_hypothesis(trends)
    # assuming mean of 0.5
    trend_mean = StatsGuy.mean(trends)
    # population st dev ~ sample st dev / sqrt(pop size)
    st_dev = Math.sqrt(StatsGuy.variance(trends) / trends.size.to_f)
    z_value = ((trend_mean - 0.5) / st_dev).abs
    p_value = (1 - StatsGuy.normal_cdf(z_value)) + StatsGuy.normal_cdf(-z_value)
    { trend_mean: trend_mean, st_dev: st_dev, z_value: z_value, p_value: p_value }
  end
end

class TrendStats
  def initialize(u)
    calc_grouped_prices
    @underlying = u
    display
  end

  def symbol
    @underlying.om_symbol
  end

  def up_proportion
    follows_up_trend.length.to_f / ups.length.to_f
  end

  def down_proportion
    @down_proportion = follows_down_trend.length.to_f / downs.length.to_f
  end

  def calc_grouped_prices
    prices = CSV.read(csv_path_for(@underlying)).drop(1).map { |row| row[6].to_f }
    @grouped =
      (0..prices.length - 3).map { |n| [prices[n], prices[n + 1], prices[n + 2]] }
  end

  def ups
    @grouped.select { |arr| uptrend?(arr) }
  end

  def follows_up_trend
    ups.select { |arr| arr[2] > arr[1] }
  end

  def downs
    @grouped.select { |arr| downtrend?(arr) }
  end

  def follows_down_trend
    downs.select { |arr| arr[2] < arr[1] }
  end

  def csv_path_for(underlying)
    Rails.root.to_s + '/csv/' + underlying.om_symbol + '.csv'
  end

  def display
    puts "#{symbol}: down:#{follows_down_trend.length.to_f / downs.length.to_f}, " \
         "up: #{follows_up_trend.length.to_f / ups.length.to_f}"
  end
end
HistoricalStats.new.run
