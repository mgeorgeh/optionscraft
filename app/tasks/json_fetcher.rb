# require 'capybara'
# require 'capybara/dsl'
require 'stats_guy'
require 'global_config'

class JsonFetcher
  def initialize(user)
    @user = user
    @trade_params = @user.trade_param_sets.first
    @exp_date = GlobalConfig.expiration_dates
                            .select { |d| d >= Time.zone.today }[0]
                            .strftime('%Y%m%d')
  end

  def forward_test
    spreads = Option.where(option_query_string)
                    .group_by(&:underlying_symbol)
                    .flat_map { |symbol, opt_list| gather_spreads(symbol, opt_list) }
    json_bourne = [{
      'expiration_label' => '',
      'days_to_expiration' => 0,
      'date' => @exp_date,
      'trades' => spreads.map(&:to_hash)
    }].to_json

    write_data_files(json_bourne)
  end

  def gather_spreads(symbol, opt_list)
    make_spreads(symbol, opt_list.select { |o| o.opt_type == 'put' }) +
      make_spreads(symbol, opt_list.select { |o| o.opt_type == 'call' })
  end

  def option_query_string
    max_delta = @trade_params.near_strike_max_delta
    min_oi = @trade_params.min_open_interest
    avoid_earnings = @trade_params.avoid_earnings
    min_vol = @trade_params.min_options_volume
    "exp_date = '#{@exp_date}' and " \
    "@ delta <= #{max_delta} and open_interest >= #{min_oi} " \
    "and volume >= #{min_vol}" + (avoid_earnings ? ' and avoid_flag=FALSE' : '')
  end

  def write_data_files(json_bourne)
    root_path = Rails.root.to_s + '/spreadsheets'
    ts = generate_timestamp
    cs_path = root_path + '/credit_spread_data' + ts + '.json'
    un_path = root_path + '/underlying_data' + ts + '.json'
    File.write(cs_path, json_bourne)
    File.write(un_path, Underlying.all.to_json)
  end

  def generate_timestamp
    Time.zone.now.to_s.delete('-: ')[0...-4]
  end

  def make_spreads(symbol, opt_list)
    opt_compare = lambda do |o1, o2|
      if o1.opt_type == 'put'
        o2.strike_price <=> o1.strike_price
      else
        o1.strike_price <=> o2.strike_price
      end
    end
    sorted = opt_list.sort { |o1, o2| opt_compare.call(o1, o2) }
    t1 = Time.zone.now
    # u = Underlying.find_by_om_symbol(symbol)
    u = Underlying.find_by om_symbol: symbol
    credit_spreads = []

    sorted.each.with_index do |near, index|
      sorted.drop(index + 1).each do |far|
        ccs = ConversionCreditSpread.new(near, far, @trade_params)
        next if ccs.invalid?
        ccs.set_vars(u, t1)
        credit_spreads.push ccs
      end
    end

    filtered_spreads = credit_spreads.select do |cs|
      (cs.mark_premium >= @trade_params.min_mark_premium) &&
        (cs.net_prom >= @trade_params.min_percent_rom)
    end

    filtered_spreads
  end
end

class ConversionCreditSpread
  attr_reader :underlying_symbol, :type, :last, :near, :far,
              :contracts, :hours_to_exp, :margin, :mark_premium,
              :reg_prom, :fees, :profit, :net_prom, :market_premium,
              :market_profit, :market_prom, :underlying_ivol,
              :prob_of_prof, :intermediate_losses, :eprof, :eprom

  def initialize(near, far, trade_params)
    @near = near
    @far = far
    @trade_params = trade_params
    @spread_width = (@near.strike_price - @far.strike_price).abs
    @contracts = ((@trade_params.max_margin_per_trade.to_f / 100) / @spread_width).floor
  end

  def invalid?
    @contracts <= 0
  end

  def set_vars(u, t)
    @underlying_symbol = u.om_symbol
    @type = @near.opt_type
    @last = u.recent_price.to_f
    @hours_to_exp = GlobalConfig.hours_to_expiration(t, @near.days_to_expiration).to_f
    @margin = (@contracts * @spread_width * 100).floor
    near_mark = 0.5 * (@near.bid + @near.ask)
    far_mark = 0.5 * (@far.bid + @far.ask)
    @mark_premium = near_mark - far_mark
    @reg_prom = 100 * (@mark_premium / @spread_width)
    @fees = ConversionCreditSpread.calc_commissions(
      @underlying_symbol, @contracts, @trade_params
    )
    @profit = @mark_premium * 100 * @contracts - @fees
    @net_prom = 100.0 * (@profit / @margin)
    @market_premium = @near.bid - @far.ask
    @market_profit = @market_premium * 100 * @contracts - 2 * @fees
    @market_prom = 100 * (@market_profit / @margin)
    @underlying_ivol = u.implied_volatility.to_f
    calc_probabilities
  end

  def num_rectangles
    10
  end

  def calc_probabilities
    prob_beyond_far_strike = StatsGuy.prob_below(
      @last, @far.strike_price, @underlying_ivol, @hours_to_exp
    )

    low_bound = @far.strike_price
    up_bound = @near.strike_price
    if @type == 'call'
      prob_beyond_far_strike = 1 - prob_beyond_far_strike
      low_bound = @near.strike_price
      up_bound = @far.strike_price
    end

    d = (up_bound - low_bound) / num_rectangles

    @offset = @type == 'call' ? 1 : -1
    break_even_point = (@profit / (@contracts * 100 * @offset)) + @near.strike_price
    relevant_prob = StatsGuy.prob_below(
      @last, break_even_point, @underlying_ivol, @hours_to_exp
    )

    @prob_of_prof = @type == 'call' ? relevant_prob : (1 - relevant_prob)

    @intermediate_losses = 0
    l = low_bound
    u = l + d

    num_rectangles.times do
      @intermediate_losses += prob_b(l, u) * loss_at((l + u) / 2)
      l += d
      u += d
    end

    @eprof = @profit - (@intermediate_losses + prob_beyond_far_strike * @margin)
    @eprom = 100 * (@eprof / @margin)
  end

  def loss_at(stock_price)
    @contracts * 100 * @offset * (stock_price - @near.strike_price)
  end

  def prob_b(price1, price2)
    StatsGuy.prob_between(price1, price2, @last, @underlying_ivol, @hours_to_exp)
  end

  def self.calc_commissions(symbol, contracts, trade_params)
    per_contract_fees =
      trade_params.commission_per_contract +
      (GlobalConfig.special_index_fees[symbol] ||
       0)
    commission = trade_params.commission_base +
                 2 * per_contract_fees * contracts
    too_low = commission < trade_params.commission_min
    (too_low ? trade_params.comission_min : commission).to_f
  end

  def opt_to_hash(opt)
    {
      'strike_price': opt.strike_price,
      'bid': opt.bid.round(2),
      'ask': opt.ask.round(2),
      'open_interest': opt.open_interest,
      'delta': opt.delta,
      'volume': opt.volume,
      'implied_volatility': opt.implied_volatility.round(2)
    }
  end

  def to_hash
    {
      'underlying_symbol': @underlying_symbol,
      'type': @type,
      'last': @last,
      'near': opt_to_hash(@near),
      'far': opt_to_hash(@far),
      'contracts': @contracts,
      'hours_to_exp': @hours_to_exp,
      'margin': @margin,
      'mark_premium': @mark_premium.round(2),
      'reg_prom': @reg_prom.round(2),
      'fees': @fees.round(2),
      'profit': @profit.round(2),
      'net_prom': @net_prom.round(2),
      'market_premium': @market_premium.round(2),
      'market_profit': @market_profit.round(2),
      'market_prom': @market_prom.round(2),
      'underlying_ivol': @underlying_ivol.round(2),
      'prob_of_prof': @prob_of_prof.round(2),
      'intermediate_losses': @intermediate_losses.round(2),
      'eprof': @eprof.round(2),
      'eprom': @eprom.round(2)
    }
  end
end
