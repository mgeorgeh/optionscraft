require 'csv'
require 'global_config'

class CBOEUpdate
  def initialize
    @new_symbols = []
    @today = Time.zone.today
    @curl = '/usr/bin/curl'
    @in2csv = '/usr/local/bin/in2csv'
  end

  def run
    return if already_ran_this_month?
    download_spreadsheet
    return if download_failed?
    convert_spreadsheet_to_csv
    @cboe_underlyings = calc_cboe_underlyings
    update_from_csv
    notify_of_new_symbols
    sync_underlying_list_with_cboe
    remove_old_files
  end

  def convert_spreadsheet_to_csv
    `#{@in2csv} #{xls_filepath} > #{csv_filepath}`
  end

  def prev_date_str
    "#{prev_year}#{prev_month}"
  end

  def download_failed?
    !File.exist?(xls_filepath)
  end

  def already_ran_this_month?
    File.exist?(xls_filepath)
  end

  def enough_volume?(params)
    proc do |row|
      row[params[:vol_index]].to_i >= GlobalConfig.daily_options_volume_cutoff
    end
  end

  def calc_cboe_underlyings
    CSV.parse(File.read(csv_filepath))
       .drop(csv_blank_rows)
       .select(&enough_volume?(vol_index: vol_index))
       .reject(&symbols_to_skip)
       .map(&to_proper_om_symbol)
  end

  def update_from_csv
    @cboe_underlyings.each do |row|
      next unless Underlying.create_from_csv_row(row, create_params)
      @new_symbols.push(row[0])
    end
  end

  def to_proper_om_symbol
    proc do |row|
      row.drop(1).unshift(GlobalConfig.deviant_symbols[row[0]] || row[0])
    end
  end

  def notify_of_new_symbols
    OCMailer.new_symbols_alert(@new_symbols).deliver_now unless @new_symbols.empty?
  end

  def remove_old_files
    old_file_glob.reject { |p| p.include? prev_date_str }.each do |path|
      File.delete(path)
    end
  end

  def not_in_cboe
    proc do |row|
      !@cboe_underlyings.map(&:first).include?(row.om_symbol)
    end
  end

  def sync_underlying_list_with_cboe
    Underlying.where(find_underlyings).select(&not_in_cboe).each(&:destroy)
  end
end
