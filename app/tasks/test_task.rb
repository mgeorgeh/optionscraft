require 'broker_apis/tda_curl_api'
# require 'broker_apis/yh_api'
# require 'broker_apis/oh_api'
# require 'global_config'
# require 'cache_method'
# require 'covered_call'
# require 'stats_guy'


# result = {
#   "AAPL"=>[
#     {:vol=>0.18669550120830536, :timestamp=>1510786800000},
#     {:vol=>0.18462850153446198, :timestamp=>1510873200000},
#     {:vol=>0.17989300191402435, :timestamp=>1511132400000},
#     {:vol=>0.1719284951686859, :timestamp=>1511218800000},
#     {:vol=>0.1658799946308136, :timestamp=>1511305200000},
#     {:vol=>0.164280503988266, :timestamp=>1511478000000},
#     {:vol=>0.16940949857234955, :timestamp=>1511737200000},
#     {:vol=>0.17728650569915771, :timestamp=>1511823600000},
#     {:vol=>0.20456300675868988, :timestamp=>1511910000000},
#     {:vol=>0.18898999691009521, :timestamp=>1511996400000}
#   ]
# }

result = {
  "OLED"=>[
    {:vol=>0.3771175146102905, :timestamp=>1510786800000},
    {:vol=>0.37415099143981934, :timestamp=>1510873200000},
    {:vol=>0.37744200229644775, :timestamp=>1511132400000},
    {:vol=>0.3650915026664734, :timestamp=>1511218800000},
    {:vol=>0.3792015016078949, :timestamp=>1511305200000},
    {:vol=>0.38359150290489197, :timestamp=>1511478000000},
    {:vol=>0.4126724898815155, :timestamp=>1511737200000},
    {:vol=>0.4079524874687195, :timestamp=>1511823600000},
    {:vol=>0.43470749258995056, :timestamp=>1511910000000},
    {:vol=>0.4312084913253784, :timestamp=>1511996400000}
  ]
}

# puts result["AAPL"].map { |h| DateTime.strptime((h[:timestamp] / 1000).to_s, "%s")}

args = {
  symbol: 'OLED',
  volatility_history_type: 'I',
  interval_type: 'DAILY',
  period_type: 'DAY',
  period: '1',
  startdate: '20171129',
  enddate: '20171130',
  daystoexpiration: '30',
  surface_type_identifier: 'DELTA_WITH_COMPOSITE',
  surface_type_value: '50,-50'
}
result = TDACurlApi.instance.formatted_volatility_data(args)
p result








































# OM    TDA     YH
# SPX, $SPX.X, ^GSPC
# NDX, $NDX.X, ^NDX
# RUT, $RUT.X, ^RUT
# VIX, $VIX.X, ^VIX
# DJX, $DJX.X, ^DJX
# OEX, $OEX.X, ^OEX
# symbs = ["NDX", "RUT", "VIX", "DJX", "OEX"]


# Underlying.select { |u| symbs.include? u.om_symbol}.each do |row|
#   row.tda_symbol = "$#{row.om_symbol}.X"
#   row.yh_symbol = "^#{row.om_symbol}"
#   row.save
# end
# spx = Underlying.find_by om_symbol: "SPX"
# spx.tda_symbol = "$SPX.X"
# spx.yh_symbol = "^GSPC"
# spx.save
# symbs = Underlying.all.take(100).map(&:tda_symbol)
# result = {}
# all_symbs = Underlying.all.map(&:tda_symbol).each_slice(100)
# all_symbs.each do |symbs|
#   data = TDACurlApi.instance.historical_data(symbs).response_body
#   File.write('/tmp/response_data', data, {mode: 'wb'})
#   f = File.open('/tmp/response_data')
#
#   symbol_count = f.read(4).unpack('N')[0]
#   symbol_count.times do
#     symbol_length = f.read(2).unpack('n')[0]
#     symbol = f.read(symbol_length)
#     result[symbol] = []
#     error_code = f.read(1).unpack('C')[0]
#     bar_count = f.read(4).unpack('N')[0]
#     bar_count.times do
#       close = f.read(4).unpack('g')[0].round(2)
#       high = f.read(4)
#       low = f.read(4)
#       open = f.read(4)
#       volume = f.read(4)
#       timestamp = (f.read(8).unpack('Q>')[0]) / 1000
#       date = Date.strptime(timestamp.to_s, "%s")
#       result[symbol].push([close, date])
#     end
#     terminator = f.read(2)
#   end
#   puts result.count
# end
# puts (all_symbs.to_a.flatten - result.keys)

# result = {}
# expire_date = Date.new(2017, 6, 23)
# result = TDACurlApi.instance.closing_quote_for('AAPL', expire_date)


# data = TDACurlApi.instance.api_closing_quote_for('AAPL', expire_date).response_body
# File.write('/tmp/response_data', data, {mode: 'wb'})
# f = File.open('/tmp/response_data')
# symbol_count = f.read(4).unpack('N')[0]
# symbol_count.times do
#   symbol_length = f.read(2).unpack('n')[0]
#   symbol = f.read(symbol_length)
#   result[symbol] = []
#   error_code = f.read(1).unpack('C')[0]
#   bar_count = f.read(4).unpack('N')[0]
#   bar_count.times do
#     close = f.read(4).unpack('g')[0].round(2)
#     high = f.read(4)
#     low = f.read(4)
#     open = f.read(4)
#     volume = f.read(4)
#     timestamp = (f.read(8).unpack('Q>')[0]) / 1000
#     date = Date.strptime(timestamp.to_s, "%s")
#     result[symbol].push([close, date])
#   end
#   terminator = f.read(2)
# end

# puts "result count: #{result.count}"
# puts "Close should be 146.28 for June 23rd 2017"
# puts "AAPL close: #{result["AAPL"][0][0]} date: #{result["AAPL"][0][1]}"
