require 'global_config'

class UpdateUnderlyingsFromCBOE < CBOEUpdate
  def download_spreadsheet
    `#{@curl} --fail #{url} -o #{xls_filepath}`
  end

  def prev_date
    @today - 1.month
  end

  def prev_month
    format '%02d', prev_date.month
  end

  def prev_year
    prev_date.year.to_s[-2..-1]
  end

  def find_underlyings
    { asset_type: 'Equity' }
  end

  def symbols_to_skip
    proc do |row|
      GlobalConfig.banned_stocks.include?(row[0])
    end
  end

  def create_params
    {
      asset_type: 'Equity',
      vol_index: vol_index
    }
  end

  def vol_index
    6
  end

  def csv_blank_rows
    1
  end

  def old_file_glob
    Dir.glob("#{Rails.root}/spreadsheets/*_rank_wosym.*")
  end

  def filename
    "#{prev_date_str}_rank_wosym.xlsx"
  end

  def xls_filepath
    "#{Rails.root}/spreadsheets/#{filename}"
  end

  def url
    "http://www.cboe.com/Publish/TTMDAvgDailyVol/#{filename}"
  end

  def csv_filepath
    "#{Rails.root}/spreadsheets/#{prev_date_str}_rank_wosym.csv"
  end
end

UpdateUnderlyingsFromCBOE.new.run
