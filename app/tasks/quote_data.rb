require 'global_config'
require 'broker_apis/tda_curl_api'

class QuoteData
  def initialize(json_spreads, exp_date_str)
    @json_spreads = json_spreads
    @exp_date_str = exp_date_str
  end

  # RUTW_081916P1200
  # RUTW_081916P1205
  def fetch
    return [] unless GlobalConfig.market_open?(Time.zone.now)
    symbols = initial_symbols
    l = symbols.length.positive? ? symbols.length : 0
    result = []
    GlobalConfig.tda_option_quote_hack.times do
      break if symbols.empty?
      puts "Fetching #{symbols.length} option symbols."
      quotes = TDACurlApi.instance.batch_quote_request(symbols)
      quote_array = Hash.from_xml(quotes.body)['amtd']['quote_list']['quote']
      result += quote_array.select { |q| q['error'].nil? }
      retry_quotes = quote_array.select { |q| !q['error'].nil? }
      symbols = retry_quotes.map { |q| q['symbol'] }
      next if symbols.length >= l || symbols.empty?
      puts "#{symbols.length} unresolved symbols: #{symbols}"
      puts 'Sleeping for a bit before retry'
      sleep 0.5
    end
    leftover = symbols.length.positive? ? symbols.length : 0
    puts "#{leftover} unresolved symbols: #{symbols}" unless symbols.empty?
    result.select { |q| q['error'].nil? }
          .map { |q| q.slice('strike_price', 'bid', 'ask', 'underlying_symbol') }
          .group_by { |strike| strike['underlying_symbol'] }
  end

  def initial_symbols
    @json_spreads.flat_map(&option_symbols)
  end

  def exp_date
    Date.parse(@exp_date_str)
  end

  def symbol_date_str
    exp_date.strftime('%m%d%y')
  end

  def option_symbols
    lambda do |spread|
      symbol = spread['underlying_symbol']
      puts spread if symbol.nil?
      if ['SPX', 'RUT'].include?(symbol)
        is_monthly = GlobalConfig.monthly_expiration_date?(exp_date)
        symbol += (is_monthly ? '' : 'W')
        return [] if Time.zone.today == exp_date # index options not tradeable on exp date
      end
      prefix = symbol + '_' + symbol_date_str + spread['type'][0].upcase
      [prefix + format_price(near_strike_price(spread)),
       prefix + format_price(far_strike_price(spread))]
    end
  end

  def format_price(price)
    if price.to_s.split('.')[1].chars.all? { |c| c == '0' }
      price.to_i.to_s
    else
      price.to_s
    end
  end

  def near_strike_price(spread)
    spread['near']['strike_price'].to_f
  end

  def far_strike_price(spread)
    spread['far']['strike_price'].to_f
  end
end
