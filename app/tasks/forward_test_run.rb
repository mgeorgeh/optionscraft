# require_relative 'forward_test'
require 'forward_test'
puts 'running'
# bin/rails runner app/tasks/forward_test_run.rb 16

class ForwardTestRunner
  def self.run
    if ARGV.include?('force-fetch') && ARGV.include?('no-fetch')
      puts 'Contradictory command line arguments. Exiting.'
      return
    end
    should_fetch_json = ARGV.include?('force-fetch')
    force_no_fetch = ARGV.include?('no-fetch')

    # default the email address
    user_email = 'forwardtest@ghweb.com'
    fetching_args = ['force-fetch', 'no-fetch']
    user_email = ARGV[0] if ARGV[0] && !fetching_args.include?(ARGV[0])
    user = User.find_by email: user_email

    # ForwardTest.run(user, should_fetch_json)
    t = ForwardTest.new(user, should_fetch_json, force_no_fetch)
    t.run
  end
end

ForwardTestRunner.run
