require 'global_config'
require 'broker_apis/oh_api'
require 'broker_apis/tda_curl_api'
require 'cache_method'

# second credit spread class that may or may not be merged
# with the proper one in the future
# combined class needs to:
# init from json or two options
# output json, update itself with new stock price and new bid/ask
# (output data about self for any given stock price/bid/ask)
# display self as csv row
class FTCreditSpread
  attr_reader :prob_of_prof, :success, :eprof,
              :profit, :actual_profit, :symbol

  def initialize(json, underlying, user, now, date, quote_data)
    @underlying = underlying
    @json = json
    @user = user
    @now = now
    @exp_date = date
    @quote_data = quote_data
  end

  def today
    Date.new(@now.year, @now.month, @now.day)
  end

  def should_exit
    return nil if no_quote_data?
    expected_loss.abs >= exit_cost.abs
  end

  def hours_to_exp
    days = (@exp_date - today).to_i
    GlobalConfig.hours_to_expiration(@now, days)
  end

  # def closing_price
  #   fetch_closing_price if @closing_price.nil?
  #   @closing_price
  # end

  def closing_price
    if today > @exp_date
      TDACurlApi.instance.closing_quote_for(tda_symbol, @exp_date)
    else
      OHApi.instance.quote_request('mark', symbol).to_f
    end
  end

  cache_method :closing_price

  def success
    actual_profit.positive?
  end

  def actual_profit
    profit - loss
  end

  def loss
    if beyond_near_strike
      x = margin * (near_strike_price - closing_price)
      [x.abs / spread_width, margin].min
    else
      0
    end
  end

  def spread_width
    (near_strike_price - far_strike_price).abs
  end

  def no_quote_data?
    today > @exp_date || !@quote_data[tda_symbol]
  end

  def exit_cost
    return nil if no_quote_data?
    new_mark = ((near_ask + near_bid) / 2) - ((far_ask + far_bid) / 2)
    new_mark = 0 if new_mark.negative?
    new_mark * contracts * 100 + calc_commissions
  end

  def near
    @quote_data[tda_symbol].find do |strike|
      strike['strike_price'].to_f == near_strike_price
    end
  end

  def far
    @quote_data[tda_symbol].find do |strike|
      strike['strike_price'].to_f == far_strike_price
    end
  end

  def near_bid
    (near['bid'] || 0).to_f
  end

  def near_ask
    (near['ask'] || 0).to_f
  end

  def far_bid
    (far['bid'] || 0).to_f
  end

  def far_ask
    (far['ask'] || 0).to_f
  end

  def per_contract_fees
    user_trade_params.commission_per_contract +
      (GlobalConfig.special_index_fees[symbol] || 0)
  end

  def calc_commissions
    commission = user_trade_params.commission_base +
                 2 * per_contract_fees * contracts
    [user_trade_params.commission_min, commission].max
  end

  def prob_beyond_far_strike_base
    StatsGuy.prob_below(closing_price,
                        far_strike_price,
                        ivol,
                        hours_to_exp)
  end

  def prob_step
    (up_bound - low_bound) / num_rectangles.to_f
  end

  def expected_loss
    d = (up_bound - low_bound) / num_rectangles.to_f

    intermediate_losses = 0
    l = low_bound
    u = l + d

    num_rectangles.times do
      intermediate_losses += prob_between(l, u) * loss_at((l + u) / 2.0)
      l += d
      u += d
    end

    (intermediate_losses + prob_beyond_far_strike * margin) || 0
  end

  def marginal_loss_at(n)
    u = n + prob_step
    prob_between(n, u) * loss_at((n + u) / 2.0)
  end

  def loss_at(stock_price)
    contracts * 100 * offset * (stock_price - near_strike_price)
  end

  def prob_between(price1, price2)
    StatsGuy.prob_between(price1, price2, closing_price, ivol, hours_to_exp)
  end

  def break_even_point
    (profit / (contracts * 100 * offset)) + near_strike_price
  end

  def two_dec(num)
    format '%.2f', num
  end

  def format_text
    "#{symbol},#{near_strike_price}/#{far_strike_price}," \
    "#{option_type},#{contracts.to_i}," \
    "#{two_dec closing_price}," \
    "#{two_dec profit},#{two_dec eprof}," \
    "#{two_dec prob_of_prof},#{success}," \
    "#{two_dec actual_profit},#{should_exit}," \
    "#{two_dec(expected_loss || 0.0)}," \
    "#{two_dec(exit_cost || 0.0)}"
  end

  def symbol
    @underlying['om_symbol']
  end

  def tda_symbol
    @underlying['tda_symbol']
  end

  def eprof
    @json['eprof'].to_f
  end

  def profit
    @json['profit'].to_f
  end

  def near_strike_price
    @json['near']['strike_price'].to_f
  end

  def far_strike_price
    @json['far']['strike_price'].to_f
  end

  def margin
    @json['margin'].to_f
  end

  def option_type
    @json['type']
  end

  def contracts
    @json['contracts'].to_f
  end

  def user_trade_params
    @user.trade_param_sets.first
  end

  def num_rectangles
    10
  end

  def exp_date_str
    @exp_date.strftime('%m%d%y')
  end

  def ivol
    @underlying['implied_volatility'].to_f
  end
end

# polymorphism over...something
class Put < FTCreditSpread
  def prob_beyond_far_strike
    prob_beyond_far_strike_base
  end

  def prob_of_prof
    1 - StatsGuy.prob_below(closing_price, break_even_point, ivol, hours_to_exp)
  end

  def low_bound
    far_strike_price
  end

  def up_bound
    near_strike_price
  end

  def offset
    -1
  end

  def beyond_near_strike
    closing_price < near_strike_price
  end
end

# polymorphism over...something
class Call < FTCreditSpread
  def prob_beyond_far_strike
    1 - prob_beyond_far_strike_base
  end

  def prob_of_prof
    StatsGuy.prob_below(closing_price, break_even_point, ivol, hours_to_exp)
  end

  def low_bound
    near_strike_price
  end

  def up_bound
    far_strike_price
  end

  def offset
    1
  end

  def beyond_near_strike
    closing_price > near_strike_price
  end
end
