require 'broker_apis/oh_api'

# shcedule once a day, can be after hours?
class UpdateVolatility
  def run
    return unless GlobalConfig.market_open?(Time.zone.now)
    Underlying.find_each do |row|
      print "symbol: #{row.om_symbol}, "
      vol_hash = OHApi.instance.underlying_volatility(row.om_symbol)
      next unless vol_hash
      puts "HV: #{vol_hash[:hv]}, IV: #{vol_hash[:iv]}, IVRank: #{vol_hash[:iv_rank]}"
      row.historic_volatility = vol_hash[:hv]
      row.implied_volatility = vol_hash[:iv]
      row.iv_rank = vol_hash[:iv_rank]
      row.save
    end
  end
end

UpdateVolatility.new.run
