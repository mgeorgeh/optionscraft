$(document).ready ->

  curr_table = null
  go_button = $('#generateButton')

  KeyedUnderlyings.build()

  make_tabs = (tab_class, tab_names, additional_actions = () ->) ->
    $('.' + tab_class).each (i, tab) ->
      $(tab).click (event) ->
        for n in tab_names
          $('#' + n).hide()
          $('#' + n + '-tab').parent().removeClass('active')
        clicked_tab_id = event.currentTarget.id
        clicked_tab_content_id = clicked_tab_id.slice(0,clicked_tab_id.length - 4)
        $('#' + clicked_tab_content_id).show()
        $('#' + clicked_tab_id).parent().addClass('active')
        additional_actions()

  # credit_spread_button_handler = (event) ->
  #   $('#wait-for-credit-spreads').show()
  #   curr_table = new CreditSpreadTable()

  $(".ghw-checkbox").click (event) ->
    console.log("!")
    $(@).toggleClass("ghw-unchecked-box")
    if !event.target.type
      box = @.firstElementChild
      $(box).prop("checked", !box.checked)
      $(box).change()

  $('#generateButton').click (event) ->
    eh = new ErrorHandler()
    if eh.error_check()
      return

    $('#wait-for-credit-spreads').show()
    tab = $('#strategy-tab-container li.active')[0].firstElementChild.id
    if tab == "credit-spread-tab"
      curr_table = new CreditSpreadTable()
    else if tab == "iron-condor-tab"
      curr_table = new IronCondorTable()
    else if tab == "debit-spread-tab"
      curr_table = new DebitSpreadTable()
    else
      console.log("Nonexistent tab selected: ",tab)
    curr_table.fetch_trades()

  $("#saveSpreadsButton").click (event) ->
    spread_strs = []
    date = $('#credit-spread-tab-bar li.active div').text()
    column_names = []
    curr_table.trade_table.columns().every () ->
      column_names.push(($(@.header()).text()))
    curr_table.trade_table.rows(".selected").every (index, tableLoop, rowLoop) ->
      cs = $.fn.dataTable
        .tables({visible: true, api: true})
        .table(curr_table.dom_table_id)
        .row(index)
        .data()
      cs_obj = Utils.object_from_arrays(column_names, cs)
      cs_obj.expiration = date
      spread_strs.push(cs_obj)

    if spread_strs.length == 0
      console.log("Attempt to send empty selected spread array.")
      return
    json_body = {
      "positions": spread_strs
    }
    $.ajax {
      type: "POST"
      url: "/positions/batch_update"
      data: JSON.stringify(json_body)
      contentType: "application/json"
      dataType: "json"
      success: (data) ->
        $("#credit-spread-table-body tr.selected").toggleClass('selected')
        $("#saveSpreadsButton").prop( "disabled", true )
        if $("#saved-spreads-tab").parent().hasClass("active")
          $("#saved-spreads-tab").trigger("click")

    }

  make_tabs('selector-tab',['underlying-selection','trade-params', 'saved-spreads'])
  make_tabs('strategy-selector-tab',['credit-spread','iron-condor','debit-spread'])

  $(".edit_trade_param_set input").change ->
    name = $(@).prop('id').replace("trade_param_set_", "")
    if $(@).prop('type') == "checkbox"
      USER_TRADE_PARAMS[name] = $(@).prop('checked')
    else
      USER_TRADE_PARAMS[name] = $(@).val()

  $("#expiration-months input").change ->
    boxes = $('#expiration-months input[name="expirationMonths"]:checked')
    expiration_months = $.map boxes, (box) ->
      v = $(box).attr("value")
      [[Number(v.substring(0,4)),Number(v.substring(4)) - 1]]
    USER_TRADE_PARAMS.expiration_months = expiration_months
    exp_month_strs = $.map boxes, (box) -> $(box).attr("value")
    USER_TRADE_PARAMS.exp_month_strs = exp_month_strs


  $('#trade-params-save-button').click (event) ->
    $('#trade-params-save-text').show()
    $('#trade-params-save-text').fadeOut(600)
