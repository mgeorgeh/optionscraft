class window.IronCondor

  constructor: (call_spread, put_spread) ->
    @call_spread = call_spread
    @put_spread = put_spread
    @margin = @call_spread.margin
    @contracts = @call_spread.contracts
    @hours_to_exp = @call_spread.hours_to_exp
    @last = @call_spread.last
    @underlying_ivol = @call_spread.underlying_ivol
    fees = Utils.calc_commissions(@call_spread.underlying_symbol, 2*@contracts)
    @mark_premium = @call_spread.mark_premium + @put_spread.mark_premium
    @market_premium = @call_spread.market_premium + @put_spread.market_premium
    @profit = (@mark_premium * @contracts * 100) - fees
    prob_beyond_call_far_strike = 1 - Utils.prob_below(@last, @call_spread.far.strike_price, @underlying_ivol,@hours_to_exp)
    loss_beyond_call_far_strike = prob_beyond_call_far_strike * @margin
    prob_beyond_put_far_strike = Utils.prob_below(@last, @put_spread.far.strike_price, @underlying_ivol, @hours_to_exp)
    loss_beyond_put_far_strike = prob_beyond_put_far_strike * @margin
    intermediate_losses = @call_spread.intermediate_losses + @put_spread.intermediate_losses
    eloss = intermediate_losses + loss_beyond_put_far_strike + loss_beyond_call_far_strike
    @eprof = @profit - eloss
    @eprom = 100 * @eprof / @margin
    #these two might be flipped
    #puts: loss_at(p) = (p - near)*contracts*100
    #calls: loss_at(p) = (near - p)*contracts*100
    put_break_even_point = @put_spread.near.strike_price - (@profit/(100*@contracts))
    call_break_even_point = @call_spread.near.strike_price + (@profit/(100*@contracts))
    @pop = Utils.prob_between(put_break_even_point, call_break_even_point, @last, @underlying_ivol, @hours_to_exp)
    @net_prom = 100 * @profit / @margin
    @market_profit = (@market_premium * @contracts * 100) - fees

  to_table_row: () =>
    near_call_delta = Math.abs(@call_spread.near.delta).toFixed(2)
    near_put_delta = Math.abs(@put_spread.near.delta).toFixed(2)
    near_call_oi = @call_spread.near.open_interest
    near_put_oi = @put_spread.near.open_interest
    far_call_oi = @call_spread.far.open_interest
    far_put_oi = @put_spread.far.open_interest
    elts = [
      @call_spread.underlying_symbol,
      @eprof,
      @eprom,
      @pop,
      "C:#{@call_spread.near.strike_price}/#{@call_spread.far.strike_price}<br/>P:#{@put_spread.near.strike_price}/#{@put_spread.far.strike_price}",
      "#{@contracts}",
      @margin,
      @profit,
      @net_prom,
      "C:#{near_call_delta}<br/>P:#{near_put_delta}",
      "C:#{near_call_oi}<br/>P:#{near_put_oi}",
      "C:#{far_call_oi}<br/>P:#{far_put_oi}",
      @mark_premium,
      @market_premium,
      @market_profit
    ]
    Utils.fix_decimal x for x in elts


  @make_from_spread_lists: (put_spreads, call_spreads) ->
    unfiltered = Utils.flatten(put_spreads.map( (p) -> call_spreads.filter( (c) -> IronCondor.valid_condor(c,p) ).
                                                                    map( (c) -> new IronCondor(c,p) ) ))
    condor_filter = IronCondor.generate_condor_filter(USER_TRADE_PARAMS)
    trades = unfiltered.filter condor_filter
    if USER_TRADE_PARAMS.recommended_flag
      IronCondor.filter_recommended(trades)
    else
      trades

  @generate_condor_filter: (trade_params) =>
    (condor) ->
        (condor.mark_premium >= Number(trade_params.min_mark_premium)) &&
        (condor.net_prom >= Number(trade_params.min_percent_rom))

  @filter_recommended: (trades) ->
    if trades.length == 0
      return []
    best = Utils.maximum_by_key(trades, "eprom")
    if best.eprom > 0
      return [best]
    else
      return []


  @valid_condor: (call_spread, put_spread) ->
    call_spread.margin == put_spread.margin && call_spread.contracts == put_spread.contracts
