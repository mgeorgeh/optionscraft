class window.IronCondorTable extends AbstractDisplayTable

  constructor: () ->
    super("iron-condor")

  make_trades: (data) ->
    for s in data
      s.puts = s.puts.map (o) -> new OCOption(o)
      put_spreads = CreditSpread.get_list 'put', s.puts, s
      s.calls = s.calls.map (o) -> new OCOption(o)
      call_spreads = CreditSpread.get_list 'call', s.calls, s
      s.trades = IronCondor.make_from_spread_lists(put_spreads, call_spreads)


  # fetch_trades: () ->
  #   params = {
  #     "near_strike_max_delta": Number USER_TRADE_PARAMS.near_strike_max_delta
  #     "min_open_interest": Number USER_TRADE_PARAMS.min_open_interest
  #     "avoid_earnings": USER_TRADE_PARAMS.avoid_earnings
  #     "min_options_volume": Number USER_TRADE_PARAMS.min_options_volume
  #   }
  #   json_request = {
  #     "underlyings" : UnderlyingTable.om_symbols()
  #     "expiration_months": USER_TRADE_PARAMS.exp_month_strs
  #     "trade_params": params
  #   }
  #
  #   $.ajax {
  #     type: "POST"
  #     url: "/ghwebapi/fetch_options"
  #     data: JSON.stringify(json_request)
  #     contentType: "application/json"
  #     dataType: "json"
  #     success: (data) =>
  #       # data: list of options grouped by symbol and expiration
  #
  #       for s in data
  #         s.puts = s.puts.map (o) -> new OCOption(o)
  #         put_spreads = CreditSpread.get_list 'put', s.puts, s
  #         s.calls = s.calls.map (o) -> new OCOption(o)
  #         call_spreads = CreditSpread.get_list 'call', s.calls, s
  #         s.trades = IronCondor.make_from_spread_lists(put_spreads, call_spreads)
  #
  #         # have put spreads and call spreads
  #         # [P P P P], [C C C C C]
  #         # [P1 P2 P3 P4] -> [{P1, [C1,C2]},{P2,[C1,C4]},...]
  #         # [CON CON CON ...]
  #
  #       format = (series_list) ->
  #         s = series_list[0]
  #         {
  #           expiration_label: s.label,
  #           days_to_expiration: s.days_to_exp
  #           date: s.exp_date,
  #           trades: Utils.flatten(series_list.map (series) -> series.trades)
  #         }
  #
  #       unsorted_trades_by_expiration = (format v for k,v of Utils.group_by data, (obj) ->
  #         obj.exp_date)
  #       trades_by_expiration = Utils.sort_by_key unsorted_trades_by_expiration, "days_to_expiration"
  #       DEBUG_MODE = false
  #       if DEBUG_MODE
  #         $.ajax {
  #           type: "POST"
  #           url: "/ocraft/forwardtest"
  #           data: JSON.stringify trades_by_expiration
  #           contentType: 'application/json'
  #         }
  #
  #       if trades_by_expiration.length != 0
  #         @display_trades(@create_tab_bar(trades_by_expiration), 0)
  #         $('#wait-for-credit-spreads').hide()
  #
  #     error: (ojb, message, exeption) ->
  #       console.log(message)
  #   }
  #   console.log("fetching!")
