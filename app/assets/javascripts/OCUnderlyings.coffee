class window.OCUnderlyings
  constructor: ->
    $.getJSON '/underlyings.json', (data) ->
      @values = {}
      for u in data
        @values[u.tda_symbol] = u
      console.log @values

  get_underlying: (symbol) -> @values[symbol]
