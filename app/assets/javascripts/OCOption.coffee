class window.OCOption
  # option format: strike_price,bid,ask,open_interest,delta,volume,implied_volatility
  # "2150,47.5,48.1,1951,0.506,0,11.778"
  constructor: (opt_str) ->
    nums = opt_str.split(",").map (s) -> Number s
    @strike_price = nums[0]
    @bid = nums[1]
    @ask = nums[2]
    @open_interest = nums[3]
    @delta = nums[4]
    @volume = nums[5]
    @implied_volatility = nums[6]
