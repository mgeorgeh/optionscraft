#= require Utils
#= require KeyedUnderlyings
class window.CreditSpread
  # fees: (contracts) -> 4.95 + 0.5*2*contracts
  constructor: (near, far, spread_width, contracts, hours_to_exp, last, symbol, type) ->
    @underlying_symbol = symbol
    @type = type
    @last = Number last
    @near = near
    @far = far
    @near.strike_price = Number(@near.strike_price)
    @far.strike_price = Number(@far.strike_price)
    @contracts = Number contracts
    @hours_to_exp = Number hours_to_exp
    @margin = Math.floor(contracts * spread_width * 100)
    near_mark = 0.5 * (Number(near.bid) + Number(near.ask))
    far_mark = 0.5 * (Number(far.bid) + Number(far.ask))
    @mark_premium = near_mark - far_mark
    @reg_prom = 100 * (@mark_premium / spread_width)
    @fees = Utils.calc_commissions(@underlying_symbol, @contracts)
    @profit = @mark_premium * 100 * @contracts - @fees
    @net_prom = 100 * (@profit / @margin)
    @market_premium = Number(near.bid) - Number(far.ask)
    @market_profit = @market_premium * 100 * @contracts - 2 * @fees
    @market_prom = 100 * (@market_profit / @margin)
    @underlying_ivol = Number KeyedUnderlyings.underlyings[symbol].implied_volatility

    num_rectangles = 10
    prob_beyond_far_strike = Utils.prob_below(@last, @far.strike_price, @underlying_ivol, @hours_to_exp)

    low_bound = @far.strike_price
    up_bound = @near.strike_price
    if type == 'call'
      prob_beyond_far_strike = 1 - prob_beyond_far_strike
      low_bound = @near.strike_price
      up_bound = @far.strike_price

    d = (up_bound - low_bound) / num_rectangles

    offset = if (type == 'call') then 1 else -1
    break_even_point = (@profit /(@contracts * 100 * offset)) + @near.strike_price
    relevant_prob = Utils.prob_below(@last, break_even_point, @underlying_ivol, @hours_to_exp)
    if type == 'call'
      @prob_of_prof = relevant_prob
    else
      @prob_of_prof = 1 - relevant_prob

    loss_at = (stock_price) =>
      @contracts*100*offset*(stock_price - @near.strike_price)

    prob_b = (price1, price2) =>
      Utils.prob_between(price1, price2, @last, @underlying_ivol, @hours_to_exp)

    @intermediate_losses = 0
    l = low_bound
    u = l + d

    for i in [1..num_rectangles]
      @intermediate_losses = @intermediate_losses + prob_b(l,u)*loss_at((l + u)/2)
      l = l + d
      u = u + d

    @eprof = @profit - (@intermediate_losses + prob_beyond_far_strike * @margin)
    @eprom = 100 * (@eprof / @margin)

  to_table_row: =>

    elts = [
      @underlying_symbol
      @type
      Number(@eprof)
      @eprom
      @prob_of_prof
      "#{@near.strike_price}/#{@far.strike_price}"
      "#{@contracts}"
      "#{@margin}"
      @profit
      @net_prom
      # @reg_prom
      Math.abs(@near.delta)
      "#{@near.open_interest}"
      "#{@far.open_interest}"
      @mark_premium
      @market_premium
      @market_profit
      # @market_prom
    ]
    Utils.fix_decimal x for x in elts

  @get_list: (type, option_list, series_obj) ->
    offset = if type == 'put' then -1 else 1
    sorted_opts = option_list.sort (a,b) ->
      if a.strike_price < b.strike_price
        -1 * offset
      else if a.strike_price > b.strike_price
        offset
      else
        0

    credit_spreads = []
    for near, index in sorted_opts
      for far in sorted_opts[(index+1)..]
        spread_width = Math.abs(Number(near.strike_price) - Number(far.strike_price))
        num_contracts = Math.floor((Number(USER_TRADE_PARAMS.max_margin_per_trade) / 100) / spread_width)
        if num_contracts > 0
          #keyed underlyings are indexed by tda_symbol, have been changed to work with om symbol
          credit_spreads.push(new CreditSpread(
            near,
            far,
            spread_width,
            num_contracts,
            series_obj.hours_to_exp,
            KeyedUnderlyings.underlyings[series_obj.symbol].recent_price,
            series_obj.symbol,
            type))
    credit_spreads

  @get_filtered_list: (type, option_list, series_obj) ->
    credit_spreads = CreditSpread.get_list(type, option_list, series_obj)
    spread_filter = CreditSpread.generate_spread_filter(USER_TRADE_PARAMS)
    filtered_spreads = credit_spreads.filter spread_filter
    if USER_TRADE_PARAMS.recommended_flag
      CreditSpread.filter_recommended(filtered_spreads)
    else
      filtered_spreads

  @generate_spread_filter: (trade_params) =>
    (spread) ->
      ((spread.mark_premium >= Number(trade_params.min_mark_premium)) &&
       (spread.net_prom >= Number(trade_params.min_percent_rom)))

  @filter_recommended: (list) =>
    if list.length == 0
      return []
    best_spread = Utils.maximum_by_key(list, "eprom")
    if best_spread.eprom > 0
      return [best_spread]
    else
      return []
