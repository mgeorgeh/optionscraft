class window.AbstractDisplayTable
  @debug_flag = false
  constructor: (@name) ->
    @dom_table_id = "#" + @name + "-table"
    @dom_table_wrapper_id = "#" + @name + "-table_wrapper"
    @table_body_id = "#" + @name + "-table-body"
    @table_length_id = "#" + @name + "-table_length"
    @data_table = $(@dom_table_id)
    @display_div = $("#" + @name)
    @tab_bar_wrapper = $("#" + @name + "-tab-bar-wrapper")
    @current_tab = 0
    @tab_bar = $('#' + @name + '-tab-bar')
    num_cols = $("#" + @name + "-table-head tr th").length
    col_width = Math.floor(100 / num_cols)
    if @data_table_defined()
      @trade_table = $.fn.dataTable.tables({api: true}).table(@dom_table_id)
    else
      @trade_table = @data_table.DataTable({
        searching: false,
        dom: 'ltpB'
        columnDefs: [
          {width: "#{col_width}%", targets: [0...num_cols]}
        ],
        buttons: [
          {
            extend: 'csv'
            text: 'Download CSV'
            filename: 'trades'
            className: ''
          }
        ]
      })

    console.log(num_cols)
    $(@dom_table_wrapper_id).hide()

  data_table_defined: () =>
    $.fn.dataTable.tables({api: true}).table(@dom_table_id).context.length > 0

  get_tab: (i) -> $('#' + @name + '-tab-' + i)
  # interface:
  # series_list = [series]
  # series = {
  #   expiration_label,
  #   date,
  #   trades,
  #   days_to_expiration
  # }
  # trades = [trade]
  # trade has a .to_table_row() method

  create_tab_bar: (series_list) ->
    html_str = ""
    em_str = USER_TRADE_PARAMS.expiration_months.toString()
    series_check = (series) ->
      exp_date = [Number(series.date.slice(0,4)), Number(series.date.slice(4,6)) - 1]
      em_str.indexOf(exp_date.toString()) > -1
    current_series = series_list.filter(series_check)
    old_label = ""
    html_tab = @tab_bar.children()[@current_tab]
    if (html_tab)
      old_label = $(html_tab).text()

    exp_label = series_list[@current_tab].expiration_label
    @current_tab = 0
    for series, i in current_series
      if series.expiration_label == old_label
        @current_tab = i
      html_str += '<li role="presentation" class="scroll-row">' +
        '<a id="' + @name + '-tab-' + i + '">' +
        series.expiration_label +
        '</a><div hidden>' + series.date + '</div></li>'

    @tab_bar.children('li').remove()
    @tab_bar.append(html_str)
    @tab_bar.show()

    $.each current_series, (i) =>
      @get_tab(i).click (event) =>
      # $('#tabIndexOfSeries' + i).click (event) =>
        $("#saveSpreadsButton").prop( "disabled", true )
        @current_tab = i
        @display_trades current_series, i

    return current_series

  change_active_tab: (index) ->
    @tab_bar.children().removeClass("active")
    # $('#tabIndexOfSeries' + index).parent().addClass("active")
    @get_tab(index).parent().addClass("active")

  add_click_handlers_to_table_rows: ->
    that = @
    $(@table_body_id).off "click", "tr"
    $(@table_body_id).on "click", "tr", () ->
      $(@).toggleClass('selected')
      if $("#{that.table_body_id} tr.selected").length > 0
        $("#saveSpreadsButton").prop( "disabled", false )
      else
        $("#saveSpreadsButton").prop( "disabled", true )

  update_days_to_exp: (days) ->
    $('#days-to-exp').remove()
    $('<div class="ghw-text-center" id="days-to-exp"> ' + #personalize with strategy name
      days + (if (days == 1) then " day" else " days") +
      " to expiration</div>").insertAfter(@table_length_id)

  display_trades: (series_list, initial_index) ->
    @trade_table.clear().draw()
    @change_active_tab(initial_index)
    @change_table_data_to(series_list[initial_index])
    @add_click_handlers_to_table_rows()
    @data_table.show()
    @display_div.show()
    @tab_bar_wrapper.show()

  update_total_trade_counter: (total_trades) ->
    $('#' + @name + '-total-trade-counter').remove()
    $('<span id="' + @name + '-total-trade-counter"> (of ' + total_trades + ")</span>").insertAfter(@table_length_id) # HERE

  change_table_data_to: (series) ->
    new_data = []
    if series
      new_data = series.trades.map (trade) -> trade.to_table_row()
      @update_days_to_exp(series.days_to_expiration)
      @update_total_trade_counter(series.trades.length)
    @trade_table.rows.add(new_data).draw()
    $(@dom_table_wrapper_id).show()

  fetch_trades: () ->
    underlyings = UnderlyingTable.om_symbols()
    if window.AbstractDisplayTable.debug_flag
      underlyings = Object.keys(KeyedUnderlyings.underlyings)
    json_request = {
      "underlyings" : underlyings
      "expiration_months": USER_TRADE_PARAMS.exp_month_strs
      "trade_type": @name
      "trade_params": {
        "near_strike_max_delta": Number USER_TRADE_PARAMS.near_strike_max_delta
        "ds_near_strike_min_delta": Number USER_TRADE_PARAMS.ds_near_strike_min_delta
        "min_open_interest": Number USER_TRADE_PARAMS.min_open_interest
        "avoid_earnings": USER_TRADE_PARAMS.avoid_earnings
        "min_options_volume": Number USER_TRADE_PARAMS.min_options_volume
      }
    }

    $.ajax {
      type: "POST"
      url: "/ghwebapi/fetch_options"
      data: JSON.stringify(json_request)
      contentType: "application/json"
      dataType: "json"
      success: (data) =>
        # data: list of options grouped by symbol and expiration
        @make_trades(data) #modifies data

        format = (series_list) ->
          s = series_list[0]
          {
            expiration_label: s.label,
            days_to_expiration: s.days_to_exp
            date: s.exp_date,
            trades: Utils.flatten(series_list.map (series) -> series.trades)
          }

        unsorted_trades_by_expiration = (format v for k,v of Utils.group_by data, (obj) ->
          obj.exp_date)
        trades_by_expiration = Utils.sort_by_key unsorted_trades_by_expiration, "days_to_expiration"
        if window.AbstractDisplayTable.debug_flag
          $.ajax {
            type: "POST"
            url: "/ocraft/forwardtest"
            data: JSON.stringify trades_by_expiration
            contentType: 'application/json'
          }

        if trades_by_expiration.length != 0
          @display_trades(@create_tab_bar(trades_by_expiration), 0)
          $('#wait-for-credit-spreads').hide()

        window.AbstractDisplayTable.debug_flag = false

      error: (ojb, message, exeption) ->
        console.log(message)
    }
