class window.Utils
  @special_index_fees: {
    "SPX": 0.53
    "SPXW": 0.53
    "SPXPM": 0.40
    "RUT": 0.18
    "RUTQ": 0.18
    "VIX": 0.36
    "OEX": 0.40
    "XEO": 0.40
    "DJX": 0.18
    "NDX": 0.14
  }
  # @clone: (obj) -> jQuery.extend(true, {}, obj)
  @clone: (obj) -> JSON.parse(JSON.stringify(obj))

  @calc_commissions: (symbol, contracts) ->
    per_contract_fees = Number(USER_TRADE_PARAMS.commission_per_contract) + (Utils.special_index_fees[symbol] || 0)
    commission = Number(USER_TRADE_PARAMS.commission_base) + 2*per_contract_fees*contracts
    if commission < Number(USER_TRADE_PARAMS.commission_min)
      Number(USER_TRADE_PARAMS.commission_min)
    else
      commission

  @object_from_arrays: (keys, values) ->
    obj = {}
    for key, index in keys
      obj[key] = values[index]
    obj

  @prob_below: (stock_price, strike_price, implied_volatility, hours_to_exp) =>
    s = Number(stock_price)
    x = Number(strike_price)
    v = Number(implied_volatility) / 100
    t = Number(hours_to_exp) / (24 * 365)
    @normalcdf(Math.log(x / s) / (v*Math.sqrt(t)))

  @prob_between: (price1, price2, current_price, implied_volatility, hours_to_exp) =>
    prob1 = @prob_below(current_price, price1, implied_volatility, hours_to_exp)
    prob2 = @prob_below(current_price, price2, implied_volatility, hours_to_exp)
    if price1 > price2 then prob1 - prob2 else prob2 - prob1

  @normalcdf: (x) ->
    t=1.0 / (1.0+0.2316419*Math.abs(x))
    d=0.3989423*Math.exp(-x*x / 2)
    prob=d*t*(0.3193815+t*(-0.3565638+t*(1.781478+t*(-1.821256+t*1.330274))))
    if x > 0
      prob = 1 - prob
    prob

  @fix_decimal: (x) ->
    y = x
    if (typeof y) == 'number'
      y = y.toFixed(2)
    y

  @flatten: (arrays) -> [].concat.apply([],arrays)

  @forward_test: () ->
    window.AbstractDisplayTable.debug_flag = true
    table = new CreditSpreadTable()
    table.fetch_trades()
    # window.AbstractDisplayTable.debug_flag = false

  @sort_by_key: (array, key) ->
    array.sort (a,b) ->
      if a[key] < b[key]
        -1
      else if a[key] > b[key]
        1
      else
        0

  @group_by: (arr, fn) ->
    result = {}
    for x in arr
      key = fn x
      if result.hasOwnProperty(key)
        result[key].push(x)
      else
        result[key] = [x]
    return result

  @maximum_by: (arr, fn) ->
    result = arr[0]
    for x in arr
      if fn(x) > fn(result)
        result = x
    result

  @maximum_by_key: (arr, key) ->
    Utils.maximum_by(arr, (elt) -> elt[key])

  @next_occurring_weekday: (date, day_of_week) ->
    result_date = new Date(date.getTime())
    result_date.setDate(date.getDate() + (7 + day_of_week - date.getDay()) % 7)
    return result_date

  @pad: (number) ->
    (if number < 10 then '0' else "") + number

  @calc_eprof: (type, last, near_strike_price, far_strike_price, underlying_ivol, hours_to_exp, profit, contracts, margin) ->

    num_rectangles = 10
    prob_beyond_far_strike = Utils.prob_below(last, far_strike_price, underlying_ivol, hours_to_exp)

    low_bound = far_strike_price
    up_bound = near_strike_price
    if type == 'call'
      prob_beyond_far_strike = 1 - prob_beyond_far_strike
      low_bound = near_strike_price
      up_bound = far_strike_price

    d = (up_bound - low_bound) / num_rectangles

    offset = if (type == 'call') then 1 else -1
    break_even_point = (profit / (contracts * 100 * offset)) + near_strike_price
    relevant_prob = Utils.prob_below(last, break_even_point, underlying_ivol, hours_to_exp)
    if type == 'call'
      prob_of_prof = relevant_prob
    else
      prob_of_prof = 1 - relevant_prob

    loss_at = (stock_price) =>
      contracts*100*offset*(stock_price - near_strike_price)

    prob_b = (price1, price2) =>
      Utils.prob_between(price1, price2, last, underlying_ivol, hours_to_exp)

    intermediate_losses = 0
    l = low_bound
    u = l + d
    console.log(l,u)

    for i in [1..num_rectangles]
      intermediate_losses = intermediate_losses + prob_b(l,u)*loss_at((l + u) / 2)
      # console.log(intermediate_losses)
      l = l + d
      u = u + d

    profit - (intermediate_losses + prob_beyond_far_strike * margin)

  @object_values: (an_obj) ->
    (value for key, value of an_obj)
