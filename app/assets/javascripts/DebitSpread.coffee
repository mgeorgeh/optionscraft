#= require Utils
#= require KeyedUnderlyings
class window.DebitSpread

  constructor: (near, far, mark_cost, contracts, hours_to_exp, last, symbol, type) ->
    @underlying_symbol = symbol
    @type = type
    @last = Number last
    @near = near
    @far = far
    @near.strike_price = Number(@near.strike_price)
    @far.strike_price = Number(@far.strike_price)
    @contracts = Number contracts
    @hours_to_exp = Number hours_to_exp
    @mark_cost = mark_cost
    @reg_prom = 0
    @fees = Utils.calc_commissions(@underlying_symbol, @contracts)
    @margin = 100 * contracts * @mark_cost + @fees
    @profit = 0
    @net_prom = 0
    @market_premium = Number(near.bid) - Number(far.ask)
    # need field for market margin
    @market_profit = 0
    @market_prom = 0
    @underlying_ivol = Number KeyedUnderlyings.underlyings[symbol].implied_volatility

    num_rectangles = 10
    prob_beyond_far_strike = Utils.prob_below(@last,
                                              @far.strike_price,
                                              @underlying_ivol,
                                              @hours_to_exp)

    low_bound = @far.strike_price
    up_bound = @near.strike_price
    if type == 'call'
      prob_beyond_far_strike = 1 - prob_beyond_far_strike
      low_bound = @near.strike_price
      up_bound = @far.strike_price

    d = (up_bound - low_bound) / num_rectangles

    offset = if (type == 'call') then 1 else -1
    break_even_point = (@profit /(@contracts * 100 * offset)) + @near.strike_price
    relevant_prob = Utils.prob_below(@last, break_even_point, @underlying_ivol, @hours_to_exp)
    if type == 'call'
      @prob_of_prof = relevant_prob
    else
      @prob_of_prof = 1 - relevant_prob

    profit_at = (stock_price) =>
      @contracts*100*offset*(stock_price - @near.strike_price)

    prob_b = (price1, price2) =>
      Utils.prob_between(price1, price2, @last, @underlying_ivol, @hours_to_exp)

    intermediate_profits = 0
    l = low_bound
    u = l + d

    for i in [1..num_rectangles]
      intermediate_profits = intermediate_profits + prob_b(l,u)*profit_at((l + u)/2)
      l = l + d
      u = u + d

    @eprof = intermediate_profits - @margin
    @eprom = 100 * (@eprof / @margin)

  to_table_row: =>

    elts = [
      @underlying_symbol
      @type
      Number(@eprof)
      @eprom
      @prob_of_prof
      "#{@near.strike_price}/#{@far.strike_price}"
      "#{@contracts}"
      @margin
      Math.abs(@near.delta)
      "#{@near.open_interest}"
      "#{@far.open_interest}"
      @mark_cost
      @market_premium
    ]
    Utils.fix_decimal x for x in elts

  @get_list: (type, option_list, series_obj) ->
    offset = if type == 'put' then -1 else 1
    sorted_opts = option_list.sort (a,b) ->
      if a.strike_price < b.strike_price
        -1 * offset
      else if a.strike_price > b.strike_price
        offset
      else
        0

    debit_spreads = []
    for near, index in sorted_opts
      for far in sorted_opts[(index+1)..]
        near_mark = 0.5 * (Number(near.bid) + Number(near.ask))
        far_mark = 0.5 * (Number(far.bid) + Number(far.ask))
        mark_cost = near_mark - far_mark
        num_contracts = Math.floor(Number(USER_TRADE_PARAMS.max_margin_per_trade) / (mark_cost*100))

        if num_contracts > 0
          debit_spreads.push(new DebitSpread(
            near,
            far,
            mark_cost,
            num_contracts,
            series_obj.hours_to_exp,
            KeyedUnderlyings.underlyings[series_obj.symbol].recent_price,
            series_obj.symbol,
            type
          ))
    debit_spreads

  @get_filtered_list: (type, option_list, series_obj) ->
    credit_spreads = DebitSpread.get_list(type, option_list, series_obj)
    spread_filter = @generate_spread_filter(USER_TRADE_PARAMS)
    filtered_spreads = credit_spreads.filter spread_filter
    if USER_TRADE_PARAMS.recommended_flag
      @filter_recommended(filtered_spreads)
    else
      filtered_spreads

  @generate_spread_filter: (trade_params) =>
    (spread) -> true

  @filter_recommended: (list) =>
    if list.length == 0
      return []
    best_spread = Utils.maximum_by_key(list, "eprom")
    if best_spread.eprom > 0
      return [best_spread]
    else
      return []
