#= require KeyedUnderlyings
class window.UnderlyingTable
  @table: null
  @underlyings: []
  @display: (properties) =>

    for prop in properties
      prop.set_strainer()

    if !@table #39ms but only on page load
      @table = $('#underlying-table').DataTable({
        "scrollX": true,
        "scrollY": 300,
        paging: false,
        searching: false,
        deferRender: true
      })

    @table.clear()
    filtering_func = properties.reduce(((acc, prop) ->
      (row) -> acc(row) && prop.strainer(row)), (row) -> true)

    @underlyings = (u for u in (v for k, v of KeyedUnderlyings.underlyings) when filtering_func u) #1ms
  # ["name" "historic_volatility", "implied_volatility", "iv_rank", "trend", "rsi","recent_price"]
    new_data = ([u.om_symbol, u.historic_volatility, u.implied_volatility, u.iv_rank, u.rsi, u.trend, u.recent_price, u.name] for u in @underlyings)

    # add + drawL 115 ms
    @table.rows.add(new_data)
    @table.rows().invalidate().draw()
    $("#underlying-table-container").show()

  @om_symbols: =>
    (u.om_symbol for u in @underlyings)
