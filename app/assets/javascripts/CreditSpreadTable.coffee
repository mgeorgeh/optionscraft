class window.CreditSpreadTable extends AbstractDisplayTable

  constructor: () ->
    super("credit-spread")

  make_trades: (data) ->
    for s in data
      s.puts = s.puts.map (o) -> new OCOption(o)
      put_spreads = CreditSpread.get_filtered_list 'put', s.puts, s
      s.calls = s.calls.map (o) -> new OCOption(o)
      call_spreads = CreditSpread.get_filtered_list 'call', s.calls, s
      s.trades = put_spreads.concat call_spreads
