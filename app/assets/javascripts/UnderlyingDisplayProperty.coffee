#= require UnderlyingTable
#= require KeyedUnderlyings
class window.UnderlyingDisplayProperty
  name: ""
  input_type: "slider"
  display_suffix: ""
  display_prefix: ""
  slider_id: => "##{@name}-slider"
  slider_amount_id: => "##{@name}-amount"
  step: 1
  calc_min_max: =>
    range = @range_for()
    prop_min = Math.ceil(range.minimum) - 1
    prop_max = Math.floor(range.maximum) + 1
    [prop_min, prop_max]

  slide_func: (event, ui) =>
    $(@slider_amount_id()).val @display_prefix + ui.values[0] + @display_suffix + " - " +
                               @display_prefix + ui.values[1] + @display_suffix

  range_for: =>
    prop_list = []
    for key, underlying of KeyedUnderlyings.underlyings
      prop_list.push Number(underlying[@name])
    {minimum: Math.min(prop_list...), maximum: Math.max(prop_list...)}

  set_strainer:  =>
    vals = $(@slider_id()).slider "option", "values"
    @strainer = (row) =>
      row[@name] >= vals[0] && row[@name] <= vals[1]

  init_slider: (properties) =>
    min_max = @calc_min_max()
    @slider_obj = $(@slider_id())
    @should_fire_change = true
    # in case of emergency s = @step
    @change_fn = (event, ui) =>
      if @should_fire_change
        UnderlyingTable.display properties

    $(@slider_id()).slider({
      range: true
      min: min_max[0]
      max: min_max[1]
      values: [USER_SLIDER_PROFILE[@name].min, USER_SLIDER_PROFILE[@name].max]
      step: @step
      slide: @slide_func
      change: @change_fn
    })
    $(@slider_amount_id()).val @display_prefix +
                               $(@slider_id()).slider("values", 0) +
                               @display_suffix + " - " +
                               @display_prefix +
                               $(@slider_id()).slider("values", 1) +
                               @display_suffix

  max_out: =>
    @should_fire_change = false
    min_max = @calc_min_max()
    @slider_obj.slider("values", [min_max[0], min_max[1]])
    $(@slider_amount_id()).val @display_prefix +
                               min_max[0] +
                               @display_suffix + " - " +
                               @display_prefix +
                               min_max[1] +
                               @display_suffix
    @should_fire_change = true

class window.AssetType extends UnderlyingDisplayProperty
  name: "asset_type"
  input_type: "checkbox"
  set_strainer: =>
    asset_types = $.map($("input[name='asset_type']:checked"), (i, index) -> i.value)
    @strainer = (row) => row[@name] in asset_types

  init_slider: (properties) ->
    @should_fire_change = true
    $("#asset_type-checkbox").change (event) =>
      if @should_fire_change
        UnderlyingTable.display properties

  max_out: =>
    @should_fire_change = false
    $("input[name='asset_type']").each (index, box) ->
      if !$(box).prop("checked")
        $(box).parent().toggleClass("ghw-unchecked-box")
      $(box).prop('checked', true)
    @should_fire_change = true

class window.HistoricVolatility extends UnderlyingDisplayProperty
  name: "historic_volatility"
  display_suffix: "%"

class window.ImpliedVolatility extends UnderlyingDisplayProperty
  name: "implied_volatility"
  display_suffix: "%"

class window.IVRank extends UnderlyingDisplayProperty
  name: "iv_rank"
  display_suffix: "%"

class window.RSI extends UnderlyingDisplayProperty
  name: "rsi"
  override_min: 0
  override_max: 100
  calc_min_max: => [@override_min, @override_max]

class window.Trend extends UnderlyingDisplayProperty
  name: "trend"
  display_suffix: "%"
  step: 0.1
  calc_min_max: =>
    range = @range_for()
    prop_min = (Math.ceil(range.minimum * 10) - 1) / 10
    prop_max = (Math.floor(range.maximum* 10) + 1) / 10
    [prop_min, prop_max]

class window.RecentPrice extends UnderlyingDisplayProperty
  name: "recent_price"
  display_prefix: "$"
  hard_max_price: 200
  calc_min_max: =>
    range = @range_for()
    prop_min = Math.ceil(range.minimum) - 1
    [prop_min, @hard_max_price]

  slide_func: (event, ui) =>
    if ui.values[1] == @hard_max_price
      $(@slider_amount_id()).val "#{@display_prefix}#{ui.values[0]} - #{@display_prefix}#{ui.values[1]}+"
    else
      super event,ui

  set_strainer: =>
    vals = $("##{@name}-slider").slider "option", "values"
    if vals[1] == @hard_max_price
       @strainer = (row) => row[@name] >= vals[0]
    else
       super

  max_out: =>
    @should_fire_change = false
    min_max = @calc_min_max()
    $(@slider_id()).slider "values", [min_max[0], min_max[1]]
    $(@slider_amount_id()).val "#{@display_prefix}0 - #{@display_prefix}200+"
    @should_fire_change = true
