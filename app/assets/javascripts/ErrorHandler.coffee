class window.ErrorHandler
  constructor: () ->
    $("#errors").hide()
    @has_errors = false
    @error_html = ""

  build_error: (text) ->
    '<div class="alert alert-danger" role="alert">' + text + '</div>'

  underlying_empty: () =>
    if UnderlyingTable.om_symbols().length == 0
      @error_html += @build_error("No underlyings selected.")
      @has_errors = true

  exp_months_empty: () =>
    if USER_TRADE_PARAMS.exp_month_strs.length == 0
      @error_html += @build_error("No expiration dates selected.")
      @has_errors = true

  error_check: () =>
    @underlying_empty()
    @exp_months_empty()
    $("#errors").html(@error_html)
    if @has_errors
      $("#errors").show()
    @has_errors
