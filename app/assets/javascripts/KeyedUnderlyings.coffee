#= require UnderlyingDisplayProperty
#= require UnderlyingTable
class window.KeyedUnderlyings
  @underlyings: {}
  @build: =>
    $.getJSON "/underlyings.json", (data) =>
      for u in data
        if u.asset_type == 'ETF' || !Utils.object_values(u).includes(null)
          @underlyings[u.om_symbol] = u

      properties = [
        new AssetType(),
        new HistoricVolatility(),
        new ImpliedVolatility(),
        new IVRank(),
        new RSI(),
        new Trend(),
        new RecentPrice()
      ]

      for prop in properties
        prop.init_slider properties

      $('#max-out-button').click (event) ->
        for prop in properties
          prop.max_out()
        UnderlyingTable.display properties

      # UnderlyingTable.display properties, @underlyings
      UnderlyingTable.display properties
