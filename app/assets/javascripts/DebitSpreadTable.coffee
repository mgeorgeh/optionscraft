class window.DebitSpreadTable extends AbstractDisplayTable
  constructor: () ->
    super("debit-spread")

  make_trades: (data) ->
    for s in data
      s.puts = s.puts.map (o) -> new OCOption(o)
      put_spreads = DebitSpread.get_filtered_list 'put', s.puts, s
      s.calls = s.calls.map (o) -> new OCOption(o)
      call_spreads = DebitSpread.get_filtered_list 'call', s.calls, s
      s.trades = put_spreads.concat call_spreads
