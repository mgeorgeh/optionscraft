class window.TestClass
  @request_with_callback: (done_fn) ->
    $.ajax {
      type: "GET"
      url: "/ghwebapi/test_api"
      success: (data) =>
        processed_data = data.magic_number
        done_fn(processed_data)
    }
