require 'global_config'

# This is the GHWeb main controller
class GhwebapiController < ApplicationController
  # shuts off authentication, remove after testing
  # skip_before_action :verify_authenticity_token

  def batch_update
    respond_to do |format|
      format.html { head :no_content }
    end
  end

  def fetch_options
    # problem: for some reason all the values in params hash are strings now
    # solution: manually convert like some kind of animal
    json_body = valid_json(params)

    unless json_body
      render file: 'public/404.html'
      return
    end
    sql = generate_sql(json_body)
    opts = ActiveRecord::Base.connection.exec_query(sql)
    result = generate_result_json(opts)
    render json: result
    # {
    #   "symbol":"SPX",
    #   "exp_date":"20160831",
    #   "label":"",
    #   "days_to_exp":0,
    #   "puts": ["20,0.15,06,51",""],
    #   "calls": [...]
    # }

    # curl -H "Content-Type: application/json" -X POST -d @my_json_file.json
    # http://localhost:3000/ghwebapi/fetch_options
  end

  def valid_json(hash)
    tps = hash['trade_params']
    return nil unless tps && all_vars?(tps) && valid_trade_type(hash)

    tps['near_strike_max_delta'] = tps['near_strike_max_delta'].to_f
    tps['ds_near_strike_min_delta'] = tps['ds_near_strike_min_delta'].to_f
    tps['min_open_interest'] = tps['min_open_interest'].to_i
    tps['min_options_volume'] = tps['min_options_volume'].to_i
    if [true, 'true'].include? tps['avoid_earnings']
      tps['avoid_earnings'] = true
    elsif [false, 'false'].include? tps['avoid_earnings']
      tps['avoid_earnings'] = false
    else
      tps['avoid_earnings'] = nil
    end
    hash['underlyings'] = [] unless hash['underlyings']

    check_conditions(hash, tps) ? hash : nil
  end

  def valid_trade_type(hash)
    trade_type = hash['trade_type']
    trade_type && GlobalConfig.valid_trade_types.include?(trade_type)
  end

  def all_vars?(tps)
    [
      tps['near_strike_max_delta'],
      tps['min_open_interest'],
      tps['min_options_volume'],
      tps['ds_near_strike_min_delta']
    ].all?
  end

  def check_conditions(hash, tps)
    [
      hash['underlyings'].all? { |u| u =~ /\A[A-Z.]{1,5}\z/ },
      hash['expiration_months'].all? { |d| d =~ /\A\d\d\d\d\d\d\z/ },
      tps['near_strike_max_delta'].is_a?(Float),
      tps['ds_near_strike_min_delta'].is_a?(Float),
      tps['min_open_interest'].is_a?(Integer),
      [true, false].include?(tps['avoid_earnings']),
      tps['min_options_volume'].is_a?(Integer)
    ].all?
  end

  def generate_sql(json_body)
    # col_names = %w(strike_price bid ask open_interest
    #                delta volume implied_volatility)
    trade_params = json_body['trade_params']
    # use the absolute value of delta
    sql_conds = exp_month_sql(json_body) + 'AND ' +
                delta_filter(json_body) +
                "open_interest >= #{trade_params['min_open_interest']} AND " \
                "volume >= #{trade_params['min_options_volume']} " +
                (trade_params['avoid_earnings'] ? 'AND avoid_flag=FALSE' : '')

    sql = 'SELECT * FROM options WHERE underlying_symbol ' \
          "IN #{underlying_array_sql(json_body)} AND #{sql_conds}"
    sql
  end

  def delta_filter(json_body)
    trade_params = json_body['trade_params']
    dfilter = "@ delta <= #{trade_params['near_strike_max_delta']} AND "
    if json_body['trade_type'] == 'debit-spread'
      dfilter = "@ delta >= #{trade_params['ds_near_strike_min_delta']} AND "
    end
    dfilter
  end

  def underlying_array_sql(json_body)
    symbols = json_body['underlyings']
    symbs = symbols.empty? ? "''" : symbols.map { |s| "'#{s}'" }.join(',')
    '(' + symbs + ')'
  end

  def exp_month_sql(json_body)
    inner_sql =
      json_body['expiration_months'].map { |e| "exp_date LIKE '" + e + "%'" }
                                    .join(' OR ')
    '( ' + inner_sql + ' ) '
  end

  def generate_result_json(opts)
    t1 = Time.zone.now
    opts.group_by { |o| [o['underlying_symbol'], o['exp_date']] }
        .map do |key, options|
          an_option = options[0]
          split_type = options.partition { |o| o['opt_type'] == 'put' }
                              .map { |ol| ol.map { |o| o['compressed_form'] } }
          generate_result_hash(key, an_option, split_type, t1)
        end
  end

  def generate_result_hash(key, an_option, split_type, t1)
    days = an_option['days_to_expiration'].to_i
    {
      'symbol' => key[0],
      'exp_date' => key[1],
      'label' => generate_label(an_option),
      'days_to_exp' => days,
      'hours_to_exp' => GlobalConfig.hours_to_expiration(t1, days).to_f,
      'puts' => split_type[0],
      'calls' => split_type[1]
    }
  end

  def generate_label(an_option)
    l = an_option['description'].split(' ')[1..-3]
    l[-1].slice!(-4, 2)
    l.join(' ').sub('(Weekly)', 'W').sub('(Quarterly)', 'Q')
  end
end
