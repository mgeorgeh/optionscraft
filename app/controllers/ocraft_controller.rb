class OcraftController < ApplicationController
  before_action :authenticate_user!, only: [:saved_spreads]
  respond_to :html, :js
  def index
    render :index
  end

  def about
    render :about
  end

  def contact
    render :contact
  end

  def screener
    if user_signed_in?
      render :screener
    else
      redirect_to :root
    end
  end

  def welcome
  end

  def saved_spreads
    @user_positions = current_user.positions
  end

  def forwardtest
    # request.body.read
    @root_path = Rails.root.to_s + '/spreadsheets'
    ts = generate_timestamp
    File.write(file_path_cs(ts), request.body.read)
    File.write(file_path_un(ts), Underlying.all.to_json)
    render text: '123'
  end

  def generate_timestamp
    Time.zone.now.to_s.delete('-: ')[0...-4]
  end

  def file_path_cs(timestamp)
    @root_path + '/credit_spread_data' + timestamp + '.json'
  end

  def file_path_un(timestamp)
    @root_path + '/underlying_data' + timestamp + '.json'
  end
end
