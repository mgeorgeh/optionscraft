class PositionsController < ApplicationController
  before_action :authenticate_user!, only: [:batch_update, :show, :destroy]
  def create
    @position = Position.new
    # puts 'TEXT THAT IS VISIBLE'
    # puts params[:id]
    respond_to do |format|
      if @position.update(position_params)
        format.html { head :no_content }
      else
        format.html { redirect_to :back }
      end
    end
  end

  def index
  end

  def show
  end

  def destroy
    @position = current_user.positions.find_by id: params[:id]
    @postion&.destroy
    respond_to do |format|
      format.js
    end
  end

  def batch_update
    Position.add_positions_for_user(params['positions'], current_user.id)
    respond_to do |format|
      format.html { head :no_content }
    end
  end

  def position_params
    params.require(:position).permit(:symbol, :type)
  end
end
