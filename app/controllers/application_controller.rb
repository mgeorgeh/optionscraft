class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception
  def hello
    render text: 'Hello, world'
  end

  def after_sign_in_path_for(_user)
    '/screener'
  end
end
