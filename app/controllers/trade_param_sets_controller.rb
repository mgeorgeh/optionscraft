#
# Important class documentation
#
class TradeParamSetsController < ApplicationController
  before_action :set_trade_param_set, only: [:show, :edit, :update, :destroy]

  # GET /trade_param_sets
  # GET /trade_param_sets.json
  def index
    @trade_param_sets = TradeParamSet.all
  end

  # GET /trade_param_sets/1
  # GET /trade_param_sets/1.json
  def show
  end

  # GET /trade_param_sets/new
  def new
    @trade_param_set = TradeParamSet.new
  end

  # GET /trade_param_sets/1/edit
  def edit
  end

  # POST /trade_param_sets
  # POST /trade_param_sets.json
  def create
    @trade_param_set = TradeParamSet.new(trade_param_set_params)
    respond_to do |format|
      if @trade_param_set.save
        format.html do
          redirect_to @trade_param_set,
                      notice: 'Trade param set was successfully created.'
        end
        format.json do
          render :show, status: :created, location: @trade_param_set
        end
      else
        format.html { render :new }
        format.json do
          render json: @trade_param_set.errors, status: :unprocessable_entity
        end
      end
    end
  end

  # PATCH/PUT /trade_param_sets/1
  # PATCH/PUT /trade_param_sets/1.json
  def update
    respond_to do |format|
      if @trade_param_set.update(trade_param_set_params)
        format.html { head :no_content }
      else
        format.html { redirect_to :back }
      end
    end
  end

  # DELETE /trade_param_sets/1
  # DELETE /trade_param_sets/1.json
  def destroy
    @trade_param_set.destroy
    respond_to do |format|
      format.html do
        redirect_to trade_param_sets_url,
                    notice: 'Trade param set was successfully destroyed.'
      end
      format.json { head :no_content }
    end
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_trade_param_set
    @trade_param_set = TradeParamSet.find(params[:id])
  end

  # Never trust parameters from the internet, only allow white list through.
  def trade_param_set_params
    params.require(:trade_param_set).permit(
      :name, :near_strike_max_delta,
      :min_open_interest, :min_mark_premium,
      :max_margin_per_trade, :min_percent_rom,
      :avoid_earnings, :avoid_dividends,
      :min_options_volume, :total_margin,
      :commission_base, :commission_per_contract,
      :commission_min, :recommended_flag,
      :ds_near_strike_min_delta
    )
  end
end
