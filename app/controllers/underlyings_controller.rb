#
class UnderlyingsController < ApplicationController
  before_action :set_underlying, only: [:show, :edit, :update, :destroy]

  # GET /underlyings
  # GET /underlyings.json
  def index
    # eliminate problematic underlyings. NOT OUR PROBLEM.
    # @underlyings = Underlying.all
    @underlyings = Underlying.where(
      'implied_volatility > 0 AND recent_price IS NOT NULL'
    )
    render json: @underlyings
  end

  # GET /underlyings/1
  # GET /underlyings/1.json
  def show
  end

  # GET /underlyings/new
  def new
    @underlying = Underlying.new
  end

  # GET /underlyings/1/edit
  def edit
  end

  # POST /underlyings
  # POST /underlyings.json
  def create
    @underlying = Underlying.new(underlying_params)

    respond_to do |format|
      if @underlying.save
        format.html do
          endredirect_to @underlying, notice: 'Underlying was successfully created.'
        end
        format.json { render :show, status: :created, location: @underlying }
      else
        format.html { render :new }
        format.json { render json: @underlying.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /underlyings/1
  # PATCH/PUT /underlyings/1.json
  def update
    respond_to do |format|
      if @underlying.update(underlying_params)
        format.html do
          endredirect_to @underlying, notice: 'Underlying was successfully updated.'
        end
        format.json { render :show, status: :ok, location: @underlying }
      else
        format.html { render :edit }
        format.json { render json: @underlying.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /underlyings/1
  # DELETE /underlyings/1.json
  def destroy
    @underlying.destroy
    respond_to do |format|
      format.html do
        endredirect_to underlyings_url, notice: 'Underlying was successfully destroyed.'
      end
      format.json { head :no_content }
    end
  end

  private

  # Use callbacks to share common setup or constraints between actions
  def set_underlying
    @underlying = Underlying.find(params[:id])
  end

  # Never trust parameters from the internet, only allow the white list through.
  def underlying_params
    params.require(:underlying).permit(
      :tda_symbol, :om_symbol,
      :yh_symbol, :name, :asset_type,
      :last_earnings_date, :next_earnings_date,
      :last_dividend_date, :avg_daily_opt_volume,
      :historic_volatility, :implied_volatility,
      :iv_rank, :trend,
      :rsi, :recent_price
    )
  end
end
