class OCMailer < ApplicationMailer
  def new_symbols_alert(new_symbols)
    Rails.logger.info { "#{Time.zone.now} Attempted mailing" }
    @new_symbols = new_symbols
    mail(to: 'michael@ghweb.com',
         from: 'optionscraft@gmail.com',
         subject: 'New Symbols Found')
  end
end
