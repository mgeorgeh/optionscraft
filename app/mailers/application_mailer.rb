class ApplicationMailer < ActionMailer::Base
  default from: 'optionscraft@gmail.com'
  layout 'mailer'
end
