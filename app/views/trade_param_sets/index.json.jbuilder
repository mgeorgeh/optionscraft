json.array!(@trade_param_sets) do |trade_param_set|
  json.extract! trade_param_set, :id, :name, :near_strike_max_delta, :min_open_interest, :min_mark_premium, :max_margin_per_trade, :total_margin, :min_percent_rom, :avoid_earnings, :avoid_dividends, :min_options_volume
  json.url trade_param_set_url(trade_param_set, format: :json)
end
