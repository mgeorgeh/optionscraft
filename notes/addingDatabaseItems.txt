ul = Underlying.new(
tda_symbol: '$SPX.X',
om_symbol: 'SPX',
yh_symbol: '$SPX.X',
name: 'S&P 500 Index',
asset_type: 'Index',
last_earnings_date: 'None',
next_earnings_date: 'None',
last_dividend_date: 'None',
options_volume: 5000000,
historic_volatility: 15.37,
implied_volatility: 13.57,
iv_rank: 30.00,
trend: -1.50,
rsi: 65.00
)
ul.save

ul = Underlying.new(
tda_symbol: 'NFLX',
om_symbol: 'NFLX',
yh_symbol: 'NFLX',
name: 'Netflix',
asset_type: 'Index',
last_earnings_date: 'None',
next_earnings_date: 'None',
last_dividend_date: 'None',
options_volume: 5000000,
historic_volatility: 15.37,
implied_volatility: 13.57,
iv_rank: 30.00,
trend: -1.50,
rsi: 65.00
)
ul.save

ul = Underlying.new(
tda_symbol: 'LNKD',
om_symbol: 'LNKD',
yh_symbol: 'LNKD',
name: 'LinkedIn',
asset_type: 'Index',
last_earnings_date: 'None',
next_earnings_date: 'None',
last_dividend_date: 'None',
options_volume: 5000000,
historic_volatility: 15.37,
implied_volatility: 13.57,
iv_rank: 30.00,
trend: -1.50,
rsi: 65.00
)
ul.save

ul = Underlying.new(
tda_symbol: 'FB',
om_symbol: 'FB',
yh_symbol: 'FB',
name: 'Facebook',
asset_type: 'Index',
last_earnings_date: 'None',
next_earnings_date: 'None',
last_dividend_date: 'None',
options_volume: 5000000,
historic_volatility: 15.37,
implied_volatility: 13.57,
iv_rank: 30.00,
trend: -1.50,
rsi: 65.00
)
ul.save

ul = Underlying.new(
tda_symbol: 'GMCR',
om_symbol: 'GMCR',
yh_symbol: 'GMCR',
name: 'Green Mountain Coffee Roasters',
asset_type: 'Index',
last_earnings_date: 'None',
next_earnings_date: 'None',
last_dividend_date: 'None',
options_volume: 5000000,
historic_volatility: 15.37,
implied_volatility: 13.57,
iv_rank: 30.00,
trend: -1.50,
rsi: 65.00
)
ul.save

u = User.new(
  email:'jhallgren@ghweb.com',
  password: 'password',
  password_confirmation: 'password'
)
u.save

u = User.new(
  email:'jeff@ghweb.com',
  password: 'password',
  password_confirmation: 'password'
)
u.save

ug = UnderlyingGroup.new(
  user_id: 1,
  name: 'My First Group'
)
ug.save

ug = UnderlyingGroup.new(
  user_id: 2,
  name: 'A Second Group'
)
ug.save

ug = UnderlyingGroup.new(
  user_id: 2,
  name: 'A Third Group'
)
ug.save

jeff = User.first
jeff.underlying_groups
u = jeff.underlying_groups.first
u.underlyings # an empty array
u.underlyings << Underlying.first(3)

jh = User.last
u = jh.underlying_groups.first
u.underlyings << Underlying.first(4)

u = jh.underlying_groups.last
u.underlyings << Underlying.last(3)

# get underlyings for "My First Group" for Jeff (user id = 1)
jeff = User.first
jeff.underlying_groups.find_by(name: 'My First Group').underlyings

jeff.underlying_groups.find_by(name: 'My First Group').underlyings << Underlying.last


# removing things
UnderlyingGroup.first.destroy
Underlying.first.destroy
