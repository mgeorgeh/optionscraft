# some performance checking tools
@@starting_time = nil
def self.start_timer
  @@starting_time = Time.now
end

def self.stop_timer(name)
  puts "#{name} finished in #{(Time.now - @@starting_time) * 1000.0} ms"
end

def self.update_option_chains
  tda_api = TDApi.new
  return if tda_api.login == 'FAIL'

  # Underlying.all.each do |row|
  Underlying.first(10).each do |row|
    puts "symbol = #{row.om_symbol}"
    xml_data, price = tda_api.option_chains(row.tda_symbol)
    output_path = Rails.root.to_s + '/chains/' + row.om_symbol + '_'
    results = []
    xml_data.each do |_exp, xml|
      json_hash = make_json_strikes(row.om_symbol, xml)
      results << json_hash if json_hash
    end
    expirations = results.group_by { |h| h['date'][0..5] }
    expirations.each do |exp, arr|
      File.write(output_path + exp + '.json', arr.to_json)
    end

    row.recent_price = price
    row.save
  end
  tda_api.logout
end

def self.make_json_strikes(_om_symbol, xml_string)
  chain_hash = Hash.from_xml(xml_string)
  fields = GlobalConfig.credit_spread_fields
  strikes = chain_hash['option_date']['option_strike'].map do |obj|
    obj['put']['in_the_money'] == 'true' ?

      obj['call'].slice(*fields).merge('strike_price' => obj['strike_price'],
                                       'option_type' => 'call') :

      obj['put'].slice(*fields).merge('strike_price' => obj['strike_price'],
                                      'option_type' => 'put')
  end.select { |obj| !obj.values.include?(nil) }

  return nil if strikes.empty?

  label = strikes[0]['description'].split(' ')[1..-3]
  label[-1].slice!(-4, 2)
  label = label.join(' ').sub('(Weekly)', 'W')
  credit_spread_hash = {
    'date' => chain_hash['option_date']['date'],
    'expiration_label' => label,
    'days_to_expiration' => chain_hash['option_date']['days_to_expiration'],
    'expiration_type' => chain_hash['option_date']['expiration_type'],
    'option_strike' => strikes
  }
  credit_spread_hash
end

def option_chains(symbol)
  base_uri =  'https://apis.tdameritrade.com/apps/200/OptionChain?'
  base_uri << "source=#{TDA_SOURCE_ID}&"
  base_uri << "symbol=#{symbol}&"
  base_uri << 'quotes=true&'
  result_hash = {}
  recent_price = 0
  @expmonths.each do |exp|
    uri = base_uri + "expire=#{exp}"
    resp = @client.get(URI.parse(uri))
    doc = Nokogiri::XML(resp.body)
    if doc.at_xpath('/amtd/result').text != 'OK'
      puts "failed on symbol #{symbol}"
      Rails.logger.error { "#{Time.now} chain fetch failed on url #{uri} response body = #{resp.body}" }
      next
    end
    recent_price = doc.at_xpath('amtd/option-chain-results/last').text.to_f
    doc.xpath('/amtd/option-chain-results/option-date').each do |chain|
      result_hash[chain.at_xpath('date').text] = chain.to_s
    end
  end
  [result_hash, recent_price]
end

# for now, just write a binary file used for further testing
def volatility_history_request
  out_file_path = '/Users/jhallgre/Devel/rubyClass/work/test/binaryStuff/AAPLVolHistory.binary'
  uri = URI.parse('https://apis.tdameritrade.com/apps/100/VolatilityHistory?' \
  "source=#{TDA_SOURCE_ID}&" \
  'requestidentifiertype=SYMBOL&' \
  'requestvalue=AAPL&' \
  'intervaltype=DAILY&' \
  'intervalduration=1&' \
  'periodtype=DAY&' \
  'period=10&' \
  'enddate=20151109&' \
  'daystoexpiration=30&' \
  'volatilityhistorytype=I&' \
  'surfacetypeidentifier=DELTA_WITH_COMPOSITE&' \
  'surfacetypevalue=50%2C-50')
  req = Typhoeus::Request.new(uri, headers: @headers)
  req.on_complete do |response|
    if response.success?
      b = File.open(out_file_path, 'wb')
      b.write(response.body)
      b.close
      return 'OK'
    elsif response.timed_out?
      Rails.logger.warn { "#{Time.now} TDA vol history timed out." }
    elsif response.code != 200
      p response
      Rails.logger.warn { "#{Time.now} Unexpected response code on TDA vol history = #{response.code}" }
    else
      Rails.logger.warn { "#{Time.now} Unknown error occurred during TDA vol history." }
    end
    return 'FAIL'
  end
  req.run
end

def fetch_options_e
  # fetch took 0.508374
  # loop took 1.150232
  # render took 0.250314
  # file write took 0.216517
  # total time: 2.126348

  s1 = Time.now
  json_body = valid_json(params)

  unless json_body
    render text: '404'
    return
  end
  # col_names = ["strike_price","bid","ask","open_interest",
  #              "delta","volume","implied_volatility"]
  # col_names_with_types = col_names.map {|s| [s, Option.column_for_attribute(s).type]}

  # {
  #   "symbol":"SPX",
  #   "exp_date":"20160831",
  #   "label":"",
  #   "days_to_exp":0,
  #   "options":[
  #     ["SPXW Aug 31 2016 1950 Put","23.1","23.5","9726","-0.203","252","18.937","89"],
  #     ["SPXW Aug 31 2016 2050 Put","44.1","44.6","4873","-0.37","74","16.112","89"]
  #     ]
  #   }

  path = Rails.root.to_s + '/spreadsheets/test_opt_json.json'
  t7 = Time.now
  File.write(path, result.to_json)
  t8 = Time.now
  puts "file write took #{t8 - t7}"
  # curl -H "Content-Type: application/json" -X POST -d @my_json_file.json http://localhost:3000/ghwebapi/fetch_options
  s2 = Time.now
  puts "total time: #{s2 - s1}"
end
