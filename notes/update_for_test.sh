#!/bin/bash

bin/rails runner app/tasks/update_next_earnings_date.rb
bin/rails runner app/tasks/update_trend_data.rb
bin/rails runner app/tasks/update_volatility.rb
bin/rails runner app/tasks/update_option_chains_run.rb
