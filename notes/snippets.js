function sortTable(col, isText, direction) {
  var rows = $('#data-table-body tr').get();
  console.log(rows.length);
  rows.sort(function(a, b) {
    var A;
    var B;
    if (isText) {
      A = $(a).children('td').eq(col).text();
      B = $(b).children('td').eq(col).text();
    } else {
      A = Number($(a).children('td').eq(col).text());
      B = Number($(b).children('td').eq(col).text());
    }
    var X = A;
    var Y = B;
    if (direction === 'descending') {
      X = B;
      Y = A;
    }
    if (X < Y) {
      return -1;
    }
    if (X > Y) {
      return 1;
    }
    return 0;
  });
  $.each(rows, function(index, row) {
    $('#data-table-body').append(row);
  });
}

$('.clicky').click(function(event) {
  var index = event.target.cellIndex;
  var isText = $(event.target).hasClass('gh-isText');
  var direction = "descending";
  if ($(event.target).hasClass('gh-descending')) {
    direction = 'ascending';
    $('#data-table-head tr th').removeClass('gh-descending');
  } else {
    direction = 'descending';
    $('#data-table-head tr th').removeClass('gh-descending');
    $(event.target).addClass('gh-descending');
  }
  sortTable(index, isText, direction);
});


chains_for_underlyings2: function(underlyings, series) {
  $.each(underlyings, function(i, underlying) {
    var url = 'ghwebapi/chains_for_symbol/' + underlying.symbol + '/' + series;
    $.getJSON(url, function(data) {
      underlying.requestDone = true;
      underlying.json_strikes = data;
      var doneList = $.map(underlyings, function(u) { return u.requestDone });
      if (doneList.indexOf(false) === -1) {
        generateCreditSpreads(underlyings);
      }
    });
  });
}
