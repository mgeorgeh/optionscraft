class AddUniqueConstraintToCreditSpread < ActiveRecord::Migration
  def change
    add_index :credit_spreads, [:symbol, :type, :expiration, :strikes], unique: true, name: 'index_credit_spreads'
  end
end
