# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20161209182800) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "j_options", id: :serial, force: :cascade do |t|
    t.jsonb "data"
  end

  create_table "options", id: :serial, force: :cascade do |t|
    t.string "underlying_symbol"
    t.string "exp_date"
    t.string "opt_type"
    t.float "strike_price"
    t.integer "days_to_expiration"
    t.string "description"
    t.float "bid"
    t.float "ask"
    t.integer "open_interest"
    t.float "delta"
    t.integer "volume"
    t.float "implied_volatility"
    t.boolean "avoid_flag"
    t.string "compressed_form"
    t.index ["underlying_symbol", "exp_date"], name: "index_options_on_underlying_symbol_and_exp_date"
  end

  create_table "positions", id: :serial, force: :cascade do |t|
    t.string "symbol"
    t.string "opt_type"
    t.string "expiration"
    t.string "strikes"
    t.float "pop"
    t.float "near_delta"
    t.integer "near_vol"
    t.integer "var_vol"
    t.integer "near_oi"
    t.integer "far_oi"
    t.float "mark_premium"
    t.float "market_premium"
    t.integer "margin"
    t.integer "user_id"
    t.integer "num_contracts"
    t.index ["symbol", "expiration", "strikes", "user_id", "opt_type"], name: "credit_spread_unique_index", unique: true
  end

  create_table "trade_param_sets", id: :serial, force: :cascade do |t|
    t.string "name"
    t.decimal "near_strike_max_delta"
    t.integer "min_open_interest"
    t.decimal "min_mark_premium"
    t.integer "max_margin_per_trade"
    t.decimal "min_percent_rom"
    t.boolean "avoid_earnings"
    t.boolean "avoid_dividends"
    t.integer "min_options_volume"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "user_id"
    t.integer "total_margin"
    t.decimal "commission_base"
    t.decimal "commission_per_contract"
    t.decimal "commission_min"
    t.boolean "recommended_flag"
    t.decimal "ds_near_strike_min_delta"
  end

  create_table "underlyings", id: :serial, force: :cascade do |t|
    t.string "tda_symbol"
    t.string "om_symbol"
    t.string "yh_symbol"
    t.string "name"
    t.string "asset_type"
    t.string "next_earnings_date"
    t.integer "avg_daily_opt_volume"
    t.decimal "historic_volatility"
    t.decimal "implied_volatility"
    t.decimal "iv_rank"
    t.decimal "trend"
    t.decimal "rsi"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.decimal "recent_price"
    t.index ["om_symbol"], name: "index_underlyings_on_om_symbol", unique: true
  end

  create_table "users", id: :serial, force: :cascade do |t|
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer "sign_in_count", default: 0, null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet "current_sign_in_ip"
    t.inet "last_sign_in_ip"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "confirmation_token"
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
    t.index ["confirmation_token"], name: "index_users_on_confirmation_token", unique: true
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
  end

end
