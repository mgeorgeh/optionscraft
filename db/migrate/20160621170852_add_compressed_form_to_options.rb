class AddCompressedFormToOptions < ActiveRecord::Migration[4.2]
  def change
    add_column :options, :compressed_form, :string
  end
end
