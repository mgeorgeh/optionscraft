class RemoveColsFromUnderlying < ActiveRecord::Migration[4.2]
  def change
    remove_column :underlyings, :last_earnings_date
    remove_column :underlyings, :last_dividend_date
  end
end
