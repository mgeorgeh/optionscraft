class CreateUnderlyings < ActiveRecord::Migration[4.2]
  def change
    create_table :underlyings do |t|
      t.string :tda_symbol
      t.string :om_symbol
      t.string :yh_symbol
      t.string :name
      t.string :asset_type
      t.string :last_earnings_date
      t.string :next_earnings_date
      t.string :last_dividend_date
      t.integer :options_volume
      t.decimal :historic_volatility
      t.decimal :implied_volatility
      t.decimal :iv_rank
      t.decimal :trend
      t.decimal :rsi

      t.timestamps null: false
    end
  end
end
