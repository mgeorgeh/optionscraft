class AddMinDeltaToTradeParams < ActiveRecord::Migration[4.2]
  def change
    add_column :trade_param_sets, :ds_near_strike_min_delta, :decimal
  end
end
