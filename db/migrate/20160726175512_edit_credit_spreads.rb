class EditCreditSpreads < ActiveRecord::Migration[4.2]
  def change
    add_column :credit_spreads, :num_contracts, :integer
    rename_column :credit_spreads, :min_margin, :margin
  end
end
