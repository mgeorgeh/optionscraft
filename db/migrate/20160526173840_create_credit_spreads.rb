class CreateCreditSpreads < ActiveRecord::Migration[4.2]
  def change
    create_table :credit_spreads do |t|
      t.string :symbol
      t.string :type
      t.string :expiration
      t.string :strikes
      t.float :pop
      t.float :near_delta
      t.integer :near_vol
      t.integer :var_vol
      t.integer :near_oi
      t.integer :far_oi
      t.float :mark_premium
      t.float :market_premium
      t.integer :min_margin
    end
  end
end
