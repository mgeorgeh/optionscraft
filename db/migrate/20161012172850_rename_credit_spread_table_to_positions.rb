class RenameCreditSpreadTableToPositions < ActiveRecord::Migration[4.2]
  def change
    rename_table :credit_spreads, :positions
  end
end
