class AddUniqueKeyToUnderlyings < ActiveRecord::Migration[4.2]
  def change
    add_index :underlyings, :om_symbol, unique: true
  end
end
