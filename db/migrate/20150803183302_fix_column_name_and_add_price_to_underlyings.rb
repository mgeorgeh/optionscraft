class FixColumnNameAndAddPriceToUnderlyings < ActiveRecord::Migration[4.2]
  def change
    rename_column :underlyings, :options_volume, :avg_daily_opt_volume
    add_column :underlyings, :recent_price, :decimal
  end
end
