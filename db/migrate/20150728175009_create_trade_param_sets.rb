class CreateTradeParamSets < ActiveRecord::Migration[4.2]
  def change
    create_table :trade_param_sets do |t|
      t.string :name
      t.decimal :near_strike_max_delta
      t.integer :min_open_interest
      t.decimal :min_mark_premium
      t.integer :max_margin_per_trade
      t.decimal :min_percent_rom
      t.boolean :avoid_earnings
      t.integer :earnings_buffer_days
      t.boolean :avoid_dividends
      t.integer :min_options_volume

      t.timestamps null: false
    end
  end
end
