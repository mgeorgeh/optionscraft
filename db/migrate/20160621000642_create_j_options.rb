class CreateJOptions < ActiveRecord::Migration[4.2]
  def change
    create_table :j_options do |t|
      t.jsonb :data
    end
  end
end
