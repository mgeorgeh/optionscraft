class AddIndexToOptions < ActiveRecord::Migration[4.2]
  def change
    add_index :options, [:underlying_symbol, :exp_date]
  end
end
