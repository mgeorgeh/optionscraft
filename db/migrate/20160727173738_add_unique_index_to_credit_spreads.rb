class AddUniqueIndexToCreditSpreads < ActiveRecord::Migration[4.2]
  def change
    add_index :credit_spreads,
              [:symbol, :expiration, :strikes, :user_id, :opt_type],
              unique: true, name: 'credit_spread_unique_index'
  end
end
