class AddUserIdToCreditSpreads < ActiveRecord::Migration[4.2]
  def change
    add_column :credit_spreads, :user_id, :integer
  end
end
