class AddUserIdToTradeParamSets < ActiveRecord::Migration[4.2]
  def change
    add_column :trade_param_sets, :user_id, :integer
  end
end
