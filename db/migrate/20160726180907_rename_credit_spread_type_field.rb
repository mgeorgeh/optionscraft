class RenameCreditSpreadTypeField < ActiveRecord::Migration[4.2]
  def change
    rename_column :credit_spreads, :type, :opt_type
  end
end
