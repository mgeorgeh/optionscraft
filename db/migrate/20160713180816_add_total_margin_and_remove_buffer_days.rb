class AddTotalMarginAndRemoveBufferDays < ActiveRecord::Migration[4.2]
  def change
    add_column :trade_param_sets, :total_margin, :integer
    remove_column :trade_param_sets, :earnings_buffer_days
  end
end
