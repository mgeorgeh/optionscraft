class AddRecommendedFlagToUserTradeParams < ActiveRecord::Migration[4.2]
  def change
    add_column :trade_param_sets, :recommended_flag, :boolean
  end
end
