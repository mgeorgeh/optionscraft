class CreateOptions < ActiveRecord::Migration[4.2]
  def change
    create_table :options do |t|
      t.string :underlying_symbol
      t.string :exp_date
      t.string :opt_type
      t.float :strike_price
      t.integer :days_to_expiration
      t.string :description
      t.float :bid
      t.float :ask
      t.integer :open_interest
      t.float :delta
      t.integer :volume
      t.float :implied_volatility
    end
  end
end
