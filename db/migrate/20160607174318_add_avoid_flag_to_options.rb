class AddAvoidFlagToOptions < ActiveRecord::Migration[4.2]
  def change
    add_column :options, :avoid_flag, :boolean
  end
end
