class AddCommissionsToTradeParams < ActiveRecord::Migration[4.2]
  def change
    add_column :trade_param_sets, :commission_base, :decimal
    add_column :trade_param_sets, :commission_per_contract, :decimal
    add_column :trade_param_sets, :commission_min, :decimal
  end
end
