
# every :weekday, :at => '3am' do
#  runner "Underlyings.update_things"
# end

# Use this file to easily define all of your cron jobs.
#
# It's helpful, but not entirely necessary to understand cron before proceeding.
# http://en.wikipedia.org/wiki/Cron

# Example:
#
# set :output, "/path/to/my/cron_log.log"
#
# every 2.hours do
#   command "/usr/bin/some_great_command"
#   runner "MyModel.some_method"
#   rake "some:great:rake:task"
# end
#
# every 4.days do
#   runner "AnotherModel.prune_old_records"
# end

# Learn more: http://github.com/javan/whenever

# job_type :runnerr,  "cd :path && bin/rails runner -e 'development' ':task' :output"
# change schedule file, run whenever -i. run whenever to see active cron jobs
# this only changes local crontab. this is not rails-related at all.
# server being up has no
# bearing on whether tasks will be run
set :environment, 'development' # default is 'production'
set :output, Whenever.path + '/log/whenever.log'
# every 1.minutes do
#   runner "Underlying.test_schedule"
# end
every :day, at: '7pm' do
  runner 'Underlying.update_underlyings_from_cboe'
end

every :day, at: '7pm' do
  runner 'Underlying.update_indexes_and_etfs'
end

every :day, at: '8pm' do
  runner 'Underlying.update_next_earnings_date'
end

every :day, at: '9pm' do
  runner 'Underlying.update_trend_data'
end

every :day, at: '10am' do # must be in east coast time zone. right after market opens
  runner 'Underlying.update_volatility'
end

every 30.minutes do
  runner 'Underlying.update_option_chains'
end

every :day, at: '6am' do
  runner 'Underlying.clean_up_option_chains'
end
