# make directories we need if they aren't there already
chains_dir = Rails.root.to_s + '/chains'
Dir.mkdir(chains_dir) unless Dir.exist?(chains_dir)

Rails.logger.warn do
  "#{Time.zone.now} Done with options_craft_init"
end
