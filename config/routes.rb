Rails.application.routes.draw do
  mount JasmineRails::Engine => '/specs' if defined?(JasmineRails)

  devise_for :users, controllers: { registrations: 'registrations' }

  get '/underlyings.json' => 'underlyings#index'

  get '/welcome' => 'ocraft#welcome'

  get 'trade_param_sets/:id', to: 'trade_param_sets#show', as: 'trade_param_set'

  patch 'trade_param_sets/:id' => 'trade_param_sets#update'

  resources :positions

  post 'ghwebapi/batch_update' => 'ghwebapi#batch_update'
  post 'positions/batch_update' => 'positions#batch_update'

  get 'ghwebapi/chains_for_symbol/:symbol/:series' =>
      'ghwebapi#chains_for_symbol', :constraints => { symbol: /[^\/]+/ }

  post 'ghwebapi/fetch_options', to: 'ghwebapi#fetch_options'

  get 'ocraft/index' => 'ocraft#index'

  get '/about' => 'ocraft#about'

  get '/contact' => 'ocraft#contact'

  get '/screener' => 'ocraft#screener'

  post 'ocraft/forwardtest' => 'ocraft#forwardtest'

  get '/user_spreads' => 'ocraft#saved_spreads'

  root 'ocraft#index'
end
