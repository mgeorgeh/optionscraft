require 'rails_helper'

describe StatsGuy do
  # this runs before each test, create an object or read some data
  before :each do
    @ohlc_list = CSV.read('spec/history.csv').drop(1).map { |arr| arr.map(&:to_f) } # [[Num]]
  end

  describe '#mean' do
    it 'takes and array of float and returns a float' do
      m = StatsGuy.mean([1.1, 1.2, 1.3])
      expect(m).to be_a(Float)
      expect(m).to eql(1.2)
    end
  end

  describe '#williams_r' do
    it 'takes an ohlc array of arrays, returns a float' do
      rsi_list = @ohlc_list.take(GlobalConfig.williams_r_period)
      will_r = format '%.2f', StatsGuy.williams_r(rsi_list)
      expect(will_r).to eql('-4.03')
    end
  end

  describe '#covariance' do
    it 'takes a thing and does a thing to it' do
      data = [904.22, 906.57, 905.00, 896.57, 890.65, 892.66, 890.41, 885.51,
              881.25, 869.81, 859.66, 856.91, 865.65, 865.42, 869.33, 873.71,
              870.21, 866.39, 850.15, 848.55, 855.43, 846.90]
      xlist = (0...data.size).map(&:to_f)
      covar = StatsGuy.covariance(xlist, data)
      expect(format('%.2f', covar)).to eql('-107.99')
    end
  end

  describe '#variance' do
    it 'takes a list of floats and returns a float' do
      data = [904.22, 906.57, 905.00, 896.57, 890.65, 892.66, 890.41, 885.51,
              881.25, 869.81, 859.66, 856.91, 865.65, 865.42, 869.33, 873.71,
              870.21, 866.39, 850.15, 848.55, 855.43, 846.90]
      var = StatsGuy.variance(data)
      expect(format('%.2f', var)).to eql('343.13')
    end
  end

  describe '#linear_regression_beta' do
    it 'takes a list of floats and returns the slope of the best fit line' do
      abl = StatsGuy.linear_regression_beta(@ohlc_list.reverse.take(20).map { |x| x[6] })
      expect(format('%.2f', abl)).to eql('-2.74')
      l = (1..20).to_a
      expect(format('%.1f', StatsGuy.linear_regression_beta(l).to_s)).to eql('1.0')
    end
  end

  # relative_strength_index returns a round(2) float
  describe '#relative_strength_index' do
    it 'takes an ohlc array of arrays, returns a float' do
      rsi = StatsGuy.relative_strength_index(@ohlc_list)
      expect(rsi).to eql(71.10)
    end
    it 'takes only GlobalConfig.rsi_period days as data' do
      list = @ohlc_list.take(GlobalConfig.rsi_period)
      rsi = StatsGuy.relative_strength_index(list)
      expect(rsi).to eql(73.61)
    end
    # relative_strength_index not called with less than rsi_period data points
    # in that case, caller will return rsi as nil (yh_api)
  end
  describe '#prob_below' do
    it 'calculates normal cdf' do
      expect(StatsGuy.normal_cdf(0.5).round(4)).to eql(0.6915)
      expect(StatsGuy.normal_cdf(1.5).round(4)).to eql(0.9332)
      expect(StatsGuy.normal_cdf(-1.5).round(4)).to eql(0.0668)
      expect(StatsGuy.normal_cdf(0).round(4)).to eql(0.5)
    end
    it 'calculates probabilities' do
      expect(StatsGuy.prob_below(100, 100, 20, 100).round(1)).to eql(0.5)
      expect(StatsGuy.prob_below(100, 110, 20, 100).round(2)).to eql(1.00)
    end
  end
end
