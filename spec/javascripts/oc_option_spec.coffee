describe "OCOption", ->
  it "parses options properly", ->
    sample_str = "85.0,0.01,0.03,1550,-0.011,86,37.115"
    opt = new OCOption(sample_str)
    expect(opt.strike_price).toEqual(85)
    expect(opt.bid).toEqual(0.01)
    expect(opt.ask).toEqual(0.03)
    expect(opt.open_interest).toEqual(1550)
    expect(opt.delta).toEqual(-0.011)
    expect(opt.volume).toEqual(86)
    expect(opt.implied_volatility).toEqual(37.115)
