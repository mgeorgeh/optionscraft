describe "Utils", ->
  it "fixes decimals", ->
    expect(Utils.fix_decimal(1.222)).toEqual('1.22')
    expect(Utils.fix_decimal(1.2)).toEqual('1.20')
    expect(Utils.fix_decimal(1.249)).toEqual('1.25')
    expect(Utils.fix_decimal('1.111')).toEqual('1.111')

  it "flattens array of arrays", ->
    ary = [[1,2],[3,4],[5,6,7],[8,9],[10]]
    flattened = Utils.flatten(ary)
    expect(flattened).toEqual([1,2,3,4,5,6,7,8,9,10])

    ary = [[1,2,[3,4]],[5,6,7],[8,9],[10]]
    flattened = Utils.flatten(ary)
    expect(flattened).toEqual([1,2,[3,4],5,6,7,8,9,10])

    ary = [[1,2],[3,4],[5,6,7],[],[10]]
    flattened = Utils.flatten(ary)
    expect(flattened).toEqual([1,2,3,4,5,6,7,10])

  it "sorts array by hash key", ->
    ary = [
      {date: "20160513"},
      {date: "20160506"},
      {date: "20160527"},
      {date: "20160520"},
    ]
    Utils.sort_by_key(ary, "date")
    expect(ary.length).toEqual(4)
    expect(ary[0].date).toEqual("20160506")
    expect(ary[1].date).toEqual("20160513")
    expect(ary[2].date).toEqual("20160520")
    expect(ary[3].date).toEqual("20160527")

    # is date ever null or empty?
    ary = [
      {date: "20160513"},
      {date: ""},
      {date: "20160527"},
      {date: "20160520"},
    ]
    Utils.sort_by_key(ary, "date")
    expect(ary.length).toEqual(4)
    expect(ary[0].date).toEqual("")
    expect(ary[1].date).toEqual("20160513")
    expect(ary[2].date).toEqual("20160520")
    expect(ary[3].date).toEqual("20160527")

  it "groups by properly", ->
    arr = [[1,2],[1,3],[2,2],[2,3]]
    result = Utils.group_by arr, (elt) -> elt[0]
    expect(result).toEqual({1: [[1,2],[1,3]], 2: [[2,2],[2,3]]})
    expect(Utils.group_by({key1: 2, key2: 2, key3: 3}, (elt) -> elt)).toEqual({})

  it "calculates normal cdf properly", ->
    expect(Utils.normalcdf(0.5).toFixed(4)).toEqual('0.6915')
    expect(Utils.normalcdf(1.5).toFixed(4)).toEqual('0.9332')
    expect(Utils.normalcdf(-(1.5)).toFixed(4)).toEqual('0.0668')
    expect(Utils.normalcdf(0).toFixed(4)).toEqual('0.5000')

  it "calculates probabilities", ->
    expect(Utils.prob_below(100,100,20,100).toFixed(1)).toEqual('0.5')
    expect(Utils.prob_below(100,110,20,100).toFixed(2)).toEqual('1.00')

  it "calculates prob between", ->
    #0.003065895510874578, 33.0, 33.05, 35.375, 35.66, 98.64
    expect(Utils.prob_between(33.0,33.05,35.375,35.66,98.64).toFixed(4)).toEqual('0.0031')
    # 0.011832146878038338, 169.75, 170.0, 174.24, 16.62, 98.64
    expect(Utils.prob_between(169.75,170.0,174.24,16.62,98.64).toFixed(4)).toEqual('0.0118')

  it "calculates fees properly", ->
    expect(Utils.calc_commissions("AAPL",8).toFixed(2)).toEqual('12.95')
    expect(Utils.calc_commissions("AAPL",2).toFixed(2)).toEqual('8.00')
    expect(Utils.calc_commissions("SPX",8).toFixed(2)).toEqual('21.43')
