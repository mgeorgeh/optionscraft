describe "CreditSpread", ->
  beforeEach ->
    KeyedUnderlyings.underlyings = {
      "AAPL": {
        "recent_price": 97
        "trend": 0.32
        "iv_rank": 55
      }
    }

  afterEach ->
    KeyedUnderlyings.underlyings = null

  it "generates the right number of spreads", ->
    s = option_data[0]
    s.puts = s.puts.map (o) -> new OCOption(o)
    expect(s.puts.length).toEqual(7)
    put_spreads = CreditSpread.get_filtered_list 'put', s.puts, s
    expect(put_spreads.length).toEqual(21)
    s.calls = s.calls.map (o) -> new OCOption(o)
    expect(s.calls.length).toEqual(11)
    call_spreads = CreditSpread.get_filtered_list 'call', s.calls, s
    expect(call_spreads.length).toEqual(34)
