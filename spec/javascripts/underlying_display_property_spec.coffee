describe "UnderlyingDisplayProperty", ->
  slider = null
  beforeEach ->
    KeyedUnderlyings.underlyings = {
      "AAPL": {
        "recent_price": 97
        "trend": 0.32
        "iv_rank": 55
      }
    }
    slider = $('<label for="historic_volatility-amount">Historic volatility:</label>
    <input class="ghw-grey-background ghw-slider-label" id="historic_volatility-amount" readonly type="text">
    <div id="historic_volatility-slider"></div><br/>')
    $(document.body).append(slider)

  afterEach ->
    KeyedUnderlyings.underlyings = null
    slider.remove()
    slider = null

  it "does make stuff", ->
    at = new AssetType()
    expect(at).not.toBe(null)
    expect(at.name).toBe("asset_type")
    hv = new HistoricVolatility()
    expect(hv).not.toBe(null)
    expect(hv.name).toBe("historic_volatility")
    hv.init_slider([hv])
    vals = $(hv.slider_id()).slider "option", "values"
    expect(vals).toEqual([8,50])
    iv = new ImpliedVolatility()
    expect(iv).not.toBe(null)
    expect(iv.name).toBe("implied_volatility")
    ivr = new IVRank()
    expect(ivr).not.toBe(null)
    expect(ivr.name).toBe("iv_rank")
    expect(ivr.range_for()).toEqual({minimum: 55, maximum: 55})
    expect(ivr.calc_min_max()).toEqual([54,56])
    ivr.set_strainer()
    rsi = new RSI()
    expect(rsi).not.toBe(null)
    expect(rsi.name).toBe("rsi")
    expect(rsi.calc_min_max()).toEqual([0,100])
    tr = new Trend()
    expect(tr).not.toBe(null)
    expect(tr.name).toBe("trend")
    expect(tr.calc_min_max()).toEqual([0.3,0.4])
    rp = new RecentPrice()
    expect(rp).not.toBe(null)
    expect(rp.name).toBe("recent_price")
    expect(rp.range_for()).toEqual({minimum: 97, maximum: 97})
    expect(rp.calc_min_max()).toEqual([96,200])
