require 'rails_helper'

describe Option do
  describe 'init' do
    om_symbol = 'AAPL'
    # last = '100'
    date = '20160708'
    days_to_expiration = 7
    should_avoid = false
    it 'rejects invalid data' do
    end

    it 'accepts valid data' do
      test_strike = { 'strike_price' => '85.00', 'standard_option' => 'true', 'put' => { 'option_symbol' => 'AAPL_080516P85', 'description' => 'AAPL (Weekly) Aug 5 2016 85 Put', 'bid' => '0.33', 'ask' => '0.36', 'bid_ask_size' => '20X66', 'last' => '0.41', 'last_trade_date' => '2016-07-01 09:40:17 EDT', 'volume' => '45', 'open_interest' => '86', 'real_time' => 'true', 'underlying_symbol' => 'AAPL', 'delta' => '-0.084', 'gamma' => '0.018', 'theta' => '-0.019', 'vega' => '0.047', 'rho' => '-0.008', 'implied_volatility' => '28.636', 'time_value_index' => '0.41', 'multiplier' => '100.00', 'change' => '0.00', 'change_percent' => nil, 'in_the_money' => 'false', 'near_the_money' => 'false', 'theoretical_value' => '0.35', 'deliverable_list' => { 'cash_in_lieu_dollar_amount' => nil, 'cash_dollar_amount' => nil, 'index_option' => 'false', 'row' => { 'symbol' => 'AAPL', 'shares' => '100' } } }, 'call' => { 'option_symbol' => 'AAPL_080516C85', 'description' => 'AAPL (Weekly) Aug 5 2016 85 Call', 'bid' => '11.10', 'ask' => '11.40', 'bid_ask_size' => '55X114', 'last' => '11.00', 'last_trade_date' => '2016-06-30 15:34:06 EDT', 'volume' => '0', 'open_interest' => '5', 'real_time' => 'true', 'underlying_symbol' => 'AAPL', 'delta' => '0.905', 'gamma' => '0.018', 'theta' => '-0.018', 'vega' => '0.049', 'rho' => '0.041', 'implied_volatility' => '30.326', 'time_value_index' => '0.04', 'multiplier' => '100.00', 'change' => '0.00', 'change_percent' => nil, 'in_the_money' => 'true', 'near_the_money' => 'false', 'theoretical_value' => '11.25', 'deliverable_list' => { 'cash_in_lieu_dollar_amount' => nil, 'cash_dollar_amount' => nil, 'index_option' => 'false', 'row' => { 'symbol' => 'AAPL', 'shares' => '100' } } } }
      opt_type = test_strike['put']['in_the_money'] == 'true' ? 'call' : 'put'
      opt_data = {
        strike_price: test_strike['strike_price'],
        opt_type: opt_type,
        data: test_strike[opt_type]
      }
      o = Option.init(should_avoid, days_to_expiration, date, opt_data, om_symbol)
      compressed = '85.0,0.33,0.36,86,-0.084,45,28.636'
      assert !o.nil?
      assert o.compressed_form == compressed
    end
    it 'behaves the same as Option.new' do
      test_strike = { 'strike_price' => '85.00', 'standard_option' => 'true', 'put' => { 'option_symbol' => 'AAPL_080516P85', 'description' => 'AAPL (Weekly) Aug 5 2016 85 Put', 'bid' => '0.33', 'ask' => '0.36', 'bid_ask_size' => '20X66', 'last' => '0.41', 'last_trade_date' => '2016-07-01 09:40:17 EDT', 'volume' => '45', 'open_interest' => '86', 'real_time' => 'true', 'underlying_symbol' => 'AAPL', 'delta' => '-0.084', 'gamma' => '0.018', 'theta' => '-0.019', 'vega' => '0.047', 'rho' => '-0.008', 'implied_volatility' => '28.636', 'time_value_index' => '0.41', 'multiplier' => '100.00', 'change' => '0.00', 'change_percent' => nil, 'in_the_money' => 'false', 'near_the_money' => 'false', 'theoretical_value' => '0.35', 'deliverable_list' => { 'cash_in_lieu_dollar_amount' => nil, 'cash_dollar_amount' => nil, 'index_option' => 'false', 'row' => { 'symbol' => 'AAPL', 'shares' => '100' } } }, 'call' => { 'option_symbol' => 'AAPL_080516C85', 'description' => 'AAPL (Weekly) Aug 5 2016 85 Call', 'bid' => '11.10', 'ask' => '11.40', 'bid_ask_size' => '55X114', 'last' => '11.00', 'last_trade_date' => '2016-06-30 15:34:06 EDT', 'volume' => '0', 'open_interest' => '5', 'real_time' => 'true', 'underlying_symbol' => 'AAPL', 'delta' => '0.905', 'gamma' => '0.018', 'theta' => '-0.018', 'vega' => '0.049', 'rho' => '0.041', 'implied_volatility' => '30.326', 'time_value_index' => '0.04', 'multiplier' => '100.00', 'change' => '0.00', 'change_percent' => nil, 'in_the_money' => 'true', 'near_the_money' => 'false', 'theoretical_value' => '11.25', 'deliverable_list' => { 'cash_in_lieu_dollar_amount' => nil, 'cash_dollar_amount' => nil, 'index_option' => 'false', 'row' => { 'symbol' => 'AAPL', 'shares' => '100' } } } }
      opt_type = test_strike['put']['in_the_money'] == 'true' ? 'call' : 'put'
      opt_data = {
        strike_price: test_strike['strike_price'],
        opt_type: opt_type,
        data: test_strike[opt_type]
      }
      o = Option.init(should_avoid, days_to_expiration, date, opt_data, om_symbol)
      strike = test_strike[opt_type]
      compressed = '85.0,0.33,0.36,86,-0.084,45,28.636'
      o2 = Option.new(underlying_symbol: om_symbol,
                      exp_date: date,
                      opt_type: opt_type,
                      strike_price: test_strike['strike_price'].to_f,
                      days_to_expiration: days_to_expiration,
                      description: strike['description'],
                      bid: strike['bid'].to_f,
                      ask: strike['ask'].to_f,
                      open_interest: strike['open_interest'].to_i,
                      delta: strike['delta'].to_f,
                      volume: strike['volume'].to_i,
                      implied_volatility: strike['implied_volatility'].to_f,
                      avoid_flag: should_avoid,
                      compressed_form: compressed)
      assert o.attributes == o2.attributes
    end
    it 'detects nonsensible options' do
      test_obj1 = {
        'standard_option' => 'false',
        'strike_price' => ''
      }
      test_obj2 = {
        'standard_option' => 'true',
        'put' => {},
        'call' => {},
        'strike_price' => '100'
      }
      assert !Option.sensible_option('100', test_obj1)
      assert !Option.sensible_option('100', test_obj2)
      assert Option.sensible_option('99', test_obj2)
    end
  end
  describe 'same_option' do
    it 'detects that two options are the same' do
      o1 = Option.new(underlying_symbol: 'AAPL', exp_date: '20160923', opt_type: 'put', strike_price: 100)
      o2 = Option.new(underlying_symbol: 'AAPL', exp_date: '20160923', opt_type: 'put', strike_price: 100)
      o3 = Option.new(underlying_symbol: 'GE', exp_date: '20160923', opt_type: 'put', strike_price: 100)
      assert o1.same_option?(o2)
      assert !o1.same_option?(o3)
    end
  end
end
