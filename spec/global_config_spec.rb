require 'rails_helper'

describe GlobalConfig do
  # this runs before each test, create an object or read some data
  before :each do
    # time zone is where ever this is run, might need to fix that
    @market_open_date_list = [
      [Time.zone.parse('2016-04-19 13:30:00'), true], # 04/19/2016 13:30 open
      [Time.zone.parse('2016-04-19 09:29:59'), false], # before market open by a second
      [Time.zone.parse('2016-04-19 09:30:00'), true], # exact market open
      [Time.zone.parse('2016-04-19 09:30:01'), true], # after market open by a second
      [Time.zone.parse('2016-04-19 09:31:00'), true], # after market open
      [Time.zone.parse('2016-04-19 16:00:00'), true], # ?? exact market close
      [Time.zone.parse('2016-04-19 16:00:01'), false], # after market close by a second
      [Time.zone.parse('2016-04-19 16:30:00'), false], # after market close
      [Time.zone.parse('2016-05-30 12:30:00'), false], # holiday
      [Time.zone.parse('2016-11-24 10:00:00'), true], # before early close, it's DST now
      [Time.zone.parse('2016-11-24 13:01:00'), false], # minute after early close
      [Time.zone.parse('2017-07-04 13:30:00'), false], # another holiday
      [Time.zone.parse('2016-09-17 13:30:00'), false], # weekend
      [Time.zone.parse('2016-08-07 13:30:00'), false], # ^
    ]

    # exp on the 29th = 3 days
    # 1pm to midnight = 11 hours +
    # the 27th = 24 hours +
    # the 28th = 24 hours +
    # midnight to 3pm = 15 hours
    # total = 74 hours
    @expiration_hours_list = [
      [Time.zone.parse('2016-04-26 13:00:00'), 3, '75.0'],
      [Time.zone.parse('2016-04-26 13:15:00'), 3, '74.75'],
      [Time.zone.parse('2016-04-26 14:00:00'), 3, '74.0'],
      [Time.zone.parse('2016-04-28 08:30:00'), 1, '31.5'],
      [Time.zone.parse('2016-04-28 15:00:00'), 1, '25.0'],
      [Time.zone.parse('2016-04-29 14:00:00'), 0, '2.0'],
      [Time.zone.parse('2016-04-29 14:30:00'), 0, '1.5'],
      [Time.zone.parse('2016-04-29 14:45:00'), 0, '1.25']
    ]
  end

  describe '#market_open' do
    it 'takes a date and determines if market is open' do
      @market_open_date_list.each do |d|
        # puts d[0]
        expect(GlobalConfig.market_open?(d[0])).to eq d[1]
      end
    end
  end

  describe '#hours_to_expiration' do
    it 'takes a time and number of days and calcs hours to expiration' do
      @expiration_hours_list.each do |d|
        # puts d[0]
        expect(GlobalConfig.hours_to_expiration(d[0], d[1])).to eq d[2]
      end
    end
  end

  describe '#expiration_months' do
    it 'returns array of YYYYMM strings' do
      months = GlobalConfig.expiration_months
      expect(months).to be_a(Array)
      expect(months.length).to eql(GlobalConfig.months_beyond_front_month + 1)
      months.each do |date|
        expect(date).to be_a(String)
        expect(date.length).to eql(6)
      end
    end
  end
  describe '#monthly_expiration_date?' do
    it '' do
      expect(GlobalConfig.monthly_expiration_date?(Date.new(2016, 8, 26))).to eql(false)
      expect(GlobalConfig.monthly_expiration_date?(Date.new(2016, 8, 19))).to eql(true)
      expect(GlobalConfig.monthly_expiration_date?(Date.new(2016, 8, 12))).to eql(false)
      expect(GlobalConfig.monthly_expiration_date?(Date.new(2016, 8, 5))).to eql(false)
      expect(GlobalConfig.monthly_expiration_date?(Date.new(2017, 1, 20))).to eql(true)
      expect(GlobalConfig.monthly_expiration_date?(Date.new(2017, 2, 17))).to eql(true)
      expect(GlobalConfig.monthly_expiration_date?(Date.new(2017, 3, 17))).to eql(true)
    end
  end
end
