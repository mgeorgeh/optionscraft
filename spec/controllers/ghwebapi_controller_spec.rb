require 'rails_helper'
require 'test_helper'

describe GhwebapiController, type: :controller do
  test_params = {}
  before :all do
    updater = UpdateOptionChainsTest.new
    updater.run_from_xml('AAPL', '201605')
    test_params = {
      'underlyings' => ['AAPL'],
      'expiration_months' => ['201605'],
      'trade_type' => 'credit-spread',
      'trade_params' => {
        'ds_near_strike_min_delta' => 0.35,
        'near_strike_max_delta' => 0.3,
        'min_open_interest' => 100,
        'min_mark_premium' => 0.0,
        'max_margin_per_trade' => 1000,
        'total_margin' => 5000,
        'min_percent_rom' => 0.0,
        'avoid_earnings' => true,
        'avoid_dividends' => true,
        'min_options_volume' => 50
      }
    }
  end
  after :all do
    Option.destroy_all
  end

  describe 'fetch options' do
    it 'fetches options properly' do
      puts 'fetches options properly'
      post :fetch_options, params: test_params
      # post(ghwebapi_fetch_options_path, params: test_params)

      expected_response_json = JSON.parse(File.read(Rails.root.to_s + '/spec/data/option_response.json'))
      assert_response 200
      # puts "RESPONSE:"
      # p response
      resp_data = JSON.parse(response.body)

      # has correct number and values of expirations
      expect(resp_data.length).to eql(4)
      expirations = %w(20160506 20160513 20160520 20160527)
      resp_expirations = resp_data.map { |c| c['exp_date'] }
      expect(expirations).to eql(resp_expirations)

      # has correct total number of options
      # num of puts
      num_puts = resp_data.flat_map { |h| h['puts'] }.length
      expect(num_puts).to eql(22)
      # num of calls
      num_calls = resp_data.flat_map { |h| h['calls'] }.length
      expect(num_calls).to eql(43)

      # make labels correctly
      expected_labels = expected_response_json.map { |c| c['label'] }
      resp_labels = resp_data.map { |c| c['label'] }
      good_labels = ['W May 6 16', 'W May 13 16', 'May 20 16', 'W May 27 16']
      expect(expected_labels).to eql(resp_labels)
      expect(good_labels).to eql(resp_labels)
    end
    it 'renders 404 properly' do
      # post(ghwebapi_fetch_options_path, params: {})
      post :fetch_options, {}
      assert_response 200
      assert_template file: "#{Rails.root}/public/404.html"
      # expect(response).to render_template file: "#{Rails.root.to_s}/public/404.html"
    end
  end
  describe 'test controller methods' do
    test_controller = GhwebapiController.new
    it 'rejects invalid json' do
      puts 'rejects invalid json'
      json_hash = {
        'underlyings' => [],
        'expiration_months' => [],
        'trade_params' => {}
      }
      expect(test_controller.valid_json(json_hash)).to eql(nil)
    end
    it 'accepts valid json' do
      expect(test_controller.valid_json(test_params)).to eql(test_params)
    end
    it 'generates sql properly' do
      sql = "SELECT * FROM options WHERE underlying_symbol IN ('AAPL') AND ( exp_date LIKE '201605%' ) AND @ delta <= 0.3 AND open_interest >= 100 AND volume >= 50 AND avoid_flag=FALSE"
      expect(test_controller.generate_sql(test_params)).to eql(sql)
    end
  end
end
