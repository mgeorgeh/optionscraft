require 'rails_helper'
require 'test_helper'
# require 'update_option_chains'

describe UpdateOptionChainsTest do
  # before :all do
  #   # u = Underlying.new()
  # end
  after :all do
    Option.destroy_all
  end
  describe 'attach_year' do
    it 'adds a year when is appropriate' do
      t = UpdateOptionChainsTest.new
      date_str = 'Jan 6'
      expect(t.attach_year(date_str)).to eql('Jan 6 2018')
    end
    it 'attaches no year when appropriate' do
      t = UpdateOptionChainsTest.new
      date_str = 'Dec 6'
      expect(t.attach_year(date_str)).to eql('Dec 6 2017')
    end
  end
  describe 'exps_to_avoid' do
    it 'does the right thing when an earnings date exists' do
      t = UpdateOptionChainsTest.new
      u = Underlying.new(next_earnings_date: '31-Jan-17', om_symbol: 'AAPL')
      exp_dates = [Date.new(2017, 1, 20), Date.new(2017, 2, 10)]
      expect(t.exps_to_avoid(u, exp_dates)).to eql([['AAPL', '20170210']])
    end
    it 'does the right thing when no earnings date exists' do
      t = UpdateOptionChainsTest.new
      u = Underlying.new(next_earnings_date: 'None', om_symbol: 'AAPL')
      exp_dates = [Date.new(2017, 1, 20), Date.new(2017, 2, 10)]
      expect(t.exps_to_avoid(u, exp_dates)).to eql([])
    end
    it 'does the right thing with an estimated earnings date' do
      t = UpdateOptionChainsTest.new
      u = Underlying.new(next_earnings_date: 'Feb 4 - Mar 3 (Est.)', om_symbol: 'AAPL')
      exp_dates = [Date.new(2017, 1, 20), Date.new(2017, 2, 10)]
      expect(t.exps_to_avoid(u, exp_dates)).to eql([['AAPL', '20170210']])
    end
  end
  describe 'AAPL test' do
    it 'check xml and options' do
      updater = UpdateOptionChainsTest.new
      updater.run_from_xml('AAPL', '201605')
      hash = Hash.from_xml(updater.xml_file)['amtd']['option_chain_results']
      last = hash['last']
      expect(last).to eql('97.011')
      options = Option.all.to_a
      expect(options.length).to eql(283)
      num_puts = options.count { |o| o.opt_type == 'put' }
      expect(num_puts).to eql(130)
      num_calls = options.count { |o| o.opt_type == 'call' }
      expect(num_calls).to eql(153)
      exp_dates = options.map(&:exp_date).uniq.length
      expect(exp_dates).to eql(4)
      any_nils = options.any? { |o| o.attributes.values.include?(nil) }
      expect(any_nils).to eql(false)
    end
  end
  describe 'BTU test' do
    it 'performs properly on invalid tda symbols' do
      updater = UpdateOptionChainsTest.new
      updater.run_from_xml('BTU', '201605')
      expect(updater.error).to eql('invalid symbol')
    end
  end
  describe 'MYL test' do
    it 'proper number of options and no nils for single expiration' do
      updater = UpdateOptionChainsTest.new
      updater.run_from_xml('MYL', '201607')
      options = Option.all.to_a
      expect(options.length).to eql(19)
      num_puts = options.count { |o| o.opt_type == 'put' }
      expect(num_puts).to eql(7)
      num_calls = options.count { |o| o.opt_type == 'call' }
      expect(num_calls).to eql(12)
      exp_dates = options.map(&:exp_date).uniq.length
      expect(exp_dates).to eql(1)
      any_nils = options.any? { |o| o.attributes.values.include?(nil) }
      expect(any_nils).to eql(false)
    end
  end
end
