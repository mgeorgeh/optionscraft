# require 'rails_helper'
require 'spec_helper'
require 'test_helper'

describe ForwardTestTest do
  describe 'desirable' do
    t = ForwardTestTest.new
    it 'rejects undesirable spreads' do
      s1 = { 'eprof' => '-1.0',
             'profit' => '110',
             'near' => { 'delta' => '0.05' } }
      s2 = { 'eprof' => '1.0',
             'profit' => '9',
             'near' => { 'delta' => '0.05' } }
      s3 = { 'eprof' => '1.0',
             'profit' => '110',
             'near' => { 'delta' => '0.25' } }
      expect(t.desirable(s1)).to eql(false)
      expect(t.desirable(s2)).to eql(false)
      expect(t.desirable(s3)).to eql(false)
    end

    it 'accepts desirable spreads' do
      s = { 'eprof' => '1.0',
            'profit' => '110',
            'near' => { 'delta' => '0.05' } }
      expect(t.desirable(s)).to eql(true)
    end
  end

  describe 'right_expiration' do
    it 'detects the correct exp date' do
      t = ForwardTestTest.new
      t.expiration = '20161202'
      s = { 'date' => '20161202' }
      expect(t.right_expiration(s)).to eql(true)
      s1 = { 'date' => '2016232' }
      expect(t.right_expiration(s1)).to eql(false)
    end
  end

  describe 'init_dates' do
    it 'performs monday actions correctly' do
      t = ForwardTestTest.new
      t.test_today = Date.new(2016, 11, 28)
      t.test_exp_date = Date.new # ignored
      t.init_dates(false, false)
      expect(t.have_fetched_json).to eql(true)
    end

    it 'overrides json fetch properly' do
      t = ForwardTestTest.new
      t.test_today = Date.new(2016, 11, 28)
      t.test_exp_date = Date.new # ignored
      t.init_dates(false, true)
      expect(t.have_fetched_json).to eql(false)
    end

    it 'performs saturday actions correctly' do
      t = ForwardTestTest.new
      t.test_today = Date.new(2016, 11, 26)
      t.test_exp_date = Date.new(2016, 12, 2)
      t.init_dates(false, false)
      expect(t.date).to eql(Date.new(2016, 11, 25))
    end

    it 'performs sunday actions correctly' do
      t = ForwardTestTest.new
      t.test_today = Date.new(2016, 11, 27)
      t.test_exp_date = Date.new(2016, 12, 2)
      t.init_dates(false, false)
      expect(t.date).to eql(Date.new(2016, 11, 25))
    end

    it 'sets expiration variable properly' do
      t = ForwardTestTest.new
      t.test_today = Date.new(2016, 11, 28)
      t.test_exp_date = Date.new(2016, 12, 2)
      t.init_dates(false, false)
      expect(t.expiration).to eql('20161202')
    end
  end

  describe 'select_spreads' do
    it 'selects the right spreads' do
      t = ForwardTestTest.new
      path = "#{Rails.root}/spec/data/credit_spread_data20161107181229.json"
      t.cs_json = JSON.parse(File.read(path))
      under_path = "#{Rails.root}/spec/data/underlying_data20161107181229.json"
      t.underlyings = JSON.parse(File.read(under_path))
      t.date = Date.new(2016, 11, 11)
      t.expiration = '20161111'
      t.now = Date.new(2016, 11, 11)
      t.select_spreads
      expect(t.selected_spreads.length).to eql(59)
    end
  end
end
