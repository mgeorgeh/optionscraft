require 'spec_helper'
require 'test_helper'

describe QuoteDataTest do
  describe 'quote data' do
    path = "#{Rails.root}/spec/data/credit_spread_data20161107181229.json"
    cs_json = JSON.parse(File.read(path))[0]['trades']
    q = QuoteDataTest.new(cs_json, '20161111')
    it 'has the right exp date' do
      expect(q.exp_date).to eql(Date.new(2016, 11, 11))
      expect(q.symbol_date_str).to eql('111116')
    end

    it 'has the right number of symbols' do
      symbols = [
        'SPXW_111116P2115',
        'SPXW_111116P2110',
        'SPXW_111116C2135',
        'SPXW_111116C2140'
      ]
      expect(q.initial_symbols).to eql(symbols)
    end

    it 'formats prices' do
      expect(q.format_price(20.000)).to eql('20')
      expect(q.format_price(200.to_f)).to eql('200')
    end
  end
end
