require 'update_option_chains'
require_relative '../app/tasks/forward_test.rb'
require_relative '../app/tasks/quote_data.rb'

# altering some functionality so class is testable
class UpdateOptionChainsTest < UpdateOptionChains
  cattr_accessor :xml_file
  cattr_accessor :error
  attr_accessor :current_day
  def initialize
    @om_symbols_to_skip = []
    @current_day = Date.new(2017, 1, 31)
  end

  def run_from_xml(om_symbol, expmonth)
    @@xml_file = read_xml_chain(om_symbol, expmonth)
    @cs_options = []
    write_options_to_db(@@xml_file, om_symbol, expmonth, []) if @@xml_file
  end

  def read_xml_chain(om_symbol, exp_month)
    chain_file = Rails.root.to_s + "/spec/data/#{om_symbol}_#{exp_month}.xml"
    File.read(chain_file)
  end

  def update_underlying_recent_price(om_symbol, last)
  end

  def delete_underlying_from_table(_om_symbol, reason)
    @@error = reason
  end
end

class ForwardTestTest < ForwardTest
  attr_accessor :expiration, :today, :test_today,
                :test_exp_date, :date, :have_fetched_json,
                :expiration, :cs_json, :underlyings,
                :selected_spreads, :now

  @spreadsheets_dir = "#{Rails.root}/spec/data"

  def initialize
    @have_fetched_json = false
  end

  def fetch_json
    @have_fetched_json = true
  end

  def fetch_today
    @test_today
  end

  def fetch_next_exp_date
    @test_exp_date
  end

  def quote_data_source
    QuoteDataTest
  end
end

class QuoteDataTest < QuoteData
  def fetch
    []
  end
end
