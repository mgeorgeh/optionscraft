require 'rails_helper'

RSpec.describe OCMailer do
  describe 'new_symbols_alert' do
    let(:mail) { OCMailer.new_symbols_alert(['abc', 'def']) }

    it 'renders the headers' do
      expect(mail.subject).to eq('New Symbols Found')
      expect(mail.to).to eq(['michael@ghweb.com'])
      expect(mail.from).to eq(['optionscraft@gmail.com'])
    end

    it 'renders the body' do
      expect(mail.body.encoded).to include('Found new symbols', 'abc', 'def')
    end
  end
end
