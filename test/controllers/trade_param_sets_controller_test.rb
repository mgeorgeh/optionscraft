require 'test_helper'

class TradeParamSetsControllerTest < ActionController::TestCase
  setup do
    @trade_param_set = trade_param_sets(:one)
  end

  test 'should get index' do
    get :index
    assert_response :success
    assert_not_nil assigns(:trade_param_sets)
  end

  test 'should get new' do
    get :new
    assert_response :success
  end

  test 'should create trade_param_set' do
    assert_difference('TradeParamSet.count') do
      post :create, trade_param_set: { avoid_dividends: @trade_param_set.avoid_dividends, avoid_earnings: @trade_param_set.avoid_earnings, earnings_buffer_days: @trade_param_set.earnings_buffer_days, max_margin_per_trade: @trade_param_set.max_margin_per_trade, min_mark_premium: @trade_param_set.min_mark_premium, min_open_interest: @trade_param_set.min_open_interest, min_options_volume: @trade_param_set.min_options_volume, min_percent_rom: @trade_param_set.min_percent_rom, name: @trade_param_set.name, near_strike_max_delta: @trade_param_set.near_strike_max_delta }
    end

    assert_redirected_to trade_param_set_path(assigns(:trade_param_set))
  end

  test 'should show trade_param_set' do
    get :show, id: @trade_param_set
    assert_response :success
  end

  test 'should get edit' do
    get :edit, id: @trade_param_set
    assert_response :success
  end

  test 'should update trade_param_set' do
    patch :update, id: @trade_param_set, trade_param_set: { avoid_dividends: @trade_param_set.avoid_dividends, avoid_earnings: @trade_param_set.avoid_earnings, earnings_buffer_days: @trade_param_set.earnings_buffer_days, max_margin_per_trade: @trade_param_set.max_margin_per_trade, min_mark_premium: @trade_param_set.min_mark_premium, min_open_interest: @trade_param_set.min_open_interest, min_options_volume: @trade_param_set.min_options_volume, min_percent_rom: @trade_param_set.min_percent_rom, name: @trade_param_set.name, near_strike_max_delta: @trade_param_set.near_strike_max_delta }
    assert_redirected_to trade_param_set_path(assigns(:trade_param_set))
  end

  test 'should destroy trade_param_set' do
    assert_difference('TradeParamSet.count', -1) do
      delete :destroy, id: @trade_param_set
    end

    assert_redirected_to trade_param_sets_path
  end
end
