require 'test_helper'

class UnderlyingsControllerTest < ActionController::TestCase
  setup do
    @underlying = underlyings(:one)
  end

  test 'should get index' do
    get :index
    assert_response :success
    assert_not_nil assigns(:underlyings)
  end

  test 'should get new' do
    get :new
    assert_response :success
  end

  test 'should create underlying' do
    assert_difference('Underlying.count') do
      post :create, underlying: { asset_type: @underlying.asset_type, historic_volatility: @underlying.historic_volatility, implied_volatility: @underlying.implied_volatility, iv_rank: @underlying.iv_rank, last_dividend_date: @underlying.last_dividend_date, last_earnings_date: @underlying.last_earnings_date, name: @underlying.name, next_earnings_date: @underlying.next_earnings_date, om_symbol: @underlying.om_symbol, options_volume: @underlying.avg_daily_opt_volume, rsi: @underlying.rsi, tda_symbol: @underlying.tda_symbol, trend: @underlying.trend, yh_symbol: @underlying.yh_symbol, recent_price: @underlying.recent_price }
    end

    assert_redirected_to underlying_path(assigns(:underlying))
  end

  test 'should show underlying' do
    get :show, id: @underlying
    assert_response :success
  end

  test 'should get edit' do
    get :edit, id: @underlying
    assert_response :success
  end

  test 'should update underlying' do
    patch :update, id: @underlying, underlying: { asset_type: @underlying.asset_type, historic_volatility: @underlying.historic_volatility, implied_volatility: @underlying.implied_volatility, iv_rank: @underlying.iv_rank, last_dividend_date: @underlying.last_dividend_date, last_earnings_date: @underlying.last_earnings_date, name: @underlying.name, next_earnings_date: @underlying.next_earnings_date, om_symbol: @underlying.om_symbol, options_volume: @underlying.avg_daily_opt_volume, rsi: @underlying.rsi, tda_symbol: @underlying.tda_symbol, trend: @underlying.trend, yh_symbol: @underlying.yh_symbol, recent_price: @underlying.recent_price }
    assert_redirected_to underlying_path(assigns(:underlying))
  end

  test 'should destroy underlying' do
    assert_difference('Underlying.count', -1) do
      delete :destroy, params: { id: @underlying }
    end

    assert_redirected_to underlyings_path
  end
end
